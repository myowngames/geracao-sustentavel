from django.http import JsonResponse
from django.contrib.auth.models import Permission

from url_filter.integrations.drf import DjangoFilterBackend

from rest_framework.authentication import SessionAuthentication, BasicAuthentication
from rest_framework.permissions import IsAuthenticated, AllowAny
from rest_framework import viewsets
from rest_framework.decorators import detail_route, list_route
from rest_framework.filters import OrderingFilter

from rest_framework_jwt.authentication import JSONWebTokenAuthentication

from mod_user.serializers.PermissionSerializer import PermissionSerializer

from mod_shared.paginations.DefaultPagination import DefaultPagination
from mod_shared.mixins.CounterMixin import CounterMixin


class PermissionViewSet(viewsets.ModelViewSet):
	queryset 				= Permission.objects.all();
	serializer_class 		= PermissionSerializer;
	authentication_classes 	= (SessionAuthentication, BasicAuthentication, JSONWebTokenAuthentication);
	permission_classes 		= (IsAuthenticated, );
	pagination_class 		= DefaultPagination;
	filter_backends 		= [DjangoFilterBackend, OrderingFilter];
	ordering 				= ('-id')


