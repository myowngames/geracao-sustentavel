from django.http import JsonResponse
from django.contrib.auth.models import Group
from django.contrib.auth.models import Permission

from mod_user.serializers.GroupSerializer import GroupSerializer

from url_filter.integrations.drf import DjangoFilterBackend

from rest_framework.authentication import SessionAuthentication, BasicAuthentication
from rest_framework.permissions import IsAuthenticated, AllowAny
from rest_framework import viewsets
from rest_framework.decorators import detail_route, list_route
from rest_framework.filters import OrderingFilter

from rest_framework_jwt.authentication import JSONWebTokenAuthentication

from mod_user.models.User import User
from mod_user.serializers.UserSerializer import UserSerializer

from mod_shared.paginations.DefaultPagination import DefaultPagination
from mod_shared.mixins.CounterMixin import CounterMixin


class GroupViewSet(viewsets.ModelViewSet):
    queryset = Group.objects.all();
    serializer_class = GroupSerializer;
    authentication_classes = (SessionAuthentication, BasicAuthentication, JSONWebTokenAuthentication);
    permission_classes = (IsAuthenticated,);
    pagination_class = DefaultPagination;
    filter_backends = [DjangoFilterBackend, ];

    @detail_route(methods=['put'])
    def set_permissions(self, request, pk=None, *args, **kwargs):
        group = self.get_object();

        if group:
            # remove all groups from the user
            for permission in group.permissions.all():
                group.permissions.remove(permission);

            ##attach to the posted groups
            for permissionId in request.data['permissions']:
                permission = Permission.objects.get(pk=permissionId);

                if permission:
                    group.permissions.add(permission);

        serializer = self.get_serializer(self.get_object());
        return JsonResponse(serializer.data);
