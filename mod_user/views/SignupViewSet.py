# -*- coding: utf-8 -*-
import uuid, random, sys

from django.http import JsonResponse
from django.contrib.auth import get_user_model

from rest_framework import viewsets
from rest_framework.permissions import AllowAny
from rest_framework.decorators import list_route
from mod_user.models.User import User
from mod_user.models.UserResetPasswordToken import UserResetPasswordToken
from mod_user.serializers.UserSerializer import UserSerializer
from mod_application.serializers.GangSerializer import GangSerializer
from mod_shared.filters.EmptyQueryset import EmptyQueryset
from rest_framework.response import Response
from rest_framework.exceptions import ValidationError
from rest_framework import status
from django.utils.crypto import get_random_string
from django.contrib.auth.models import Group
import base64
from django.db import IntegrityError
from django.core.files.base import ContentFile


class SignupViewSet(viewsets.GenericViewSet):
    queryset = get_user_model().objects.all()
    serializer_class = UserSerializer
    permission_classes = (AllowAny,)
    filter_backends = (EmptyQueryset,)

    @list_route(methods=['post'])
    def remember_pass(self, request, *args, **kwargs):
        query = User.objects.filter(email=request.data['email'])
        if query.count() == 1:
            user = query.get()

            if hasattr(user, 'user_reset_password_token'):
                user.user_reset_password_token.delete()

            token = str(uuid.uuid4()) + str(random.random())
            ptoken = UserResetPasswordToken.objects.create(user=user, token=token)
            ptoken.sendEmail()
        else:
            raise EntryNotFound('Email não encontrado')

        return JsonResponse({})


    @list_route(methods=['post'])
    def check_reset_pass_token(self, request, *args, **kwargs):
        if 'id' not in request.data.keys():
            raise ValidationError('Missing Id(token) parameter')

        query = UserResetPasswordToken.objects.filter(token=request.data['id'])
        response = {}
        response['status'] = False
        response['message'] = ""

        if query.count() == 1:
            response['status'] = True
            response['message'] = "Reset Password Token valid"
        else:
            raise ValidationError("Token de troca de senha invalido")
        return JsonResponse(response)

    @list_route(methods=['post'])
    def reset_pass(self, request, *args, **kwargs):
        if 'id' not in request.data.keys():
            raise ValidationError('Faltando parametro token parameter')

        query = UserResetPasswordToken.objects.filter(token=request.data['id'])

        if request.data['password'] != request.data['confirmPassword']:
            raise ValidationError(['Senhas não combinam'])

        if query.count() == 1:
            instance = query.get()
            user = instance.user
            user.set_password(str(request.data['password']))
            user.save()
            instance.delete()
        else:
            raise ValidationError(['Token de troca de senha invalido'])

        return JsonResponse({})

    @list_route(methods=['post'], serializer_class=GangSerializer)
    def register(self, request, *arg, **kwargs):
        data = request.data
        users = data.pop('users')
        gang_serializer = GangSerializer(data=data)
        gang_leader = None
        gang_leader_password = None
        final_users = []


        if users and len(users) > 0:
            if len(users) != 5:
                raise ValidationError("Grupo precisa ter no minimo 5 integrantes")
            if gang_serializer.is_valid():
                for i, user in enumerate(users):
                    # Upload in Base64
                    users[i]['term'] = ContentFile(
                        base64.b64decode(user['term']['value']),
                        name=user['term']['filename']
                    )

                    unique_username = False
                    query = User.objects.filter(username=user['username'])

                    if query.count() > 0:
                        while not unique_username:
                            username = users[i]['username'] + get_random_string(length=4,
                                                                            allowed_chars='0123456789' + user['name'])
                            query = User.objects.filter(username=username)
                            if query.count() <= 0:
                                users[i]['username'] = username
                                unique_username = True

                    user_serializer = UserSerializer(data=users[i])
                    if not user_serializer.is_valid():
                        return Response(user_serializer.errors,
                                        status=status.HTTP_400_BAD_REQUEST)
                gang = gang_serializer.save()
                for user in users:
                    # user.gang = gang_serializer
                    user['gang'] = gang.id
                    user['city'] = gang.city.id
                    password = user['password']

                    user_serializer = UserSerializer(data=user)
                    try:
                        if not user_serializer.is_valid():
                            gang.delete()
                            return Response(user_serializer.errors,
                                        status=status.HTTP_400_BAD_REQUEST)
                        # user_serializer.is_valid()
                        user = user_serializer.save()
                        # Send email to the gang leader
                        if user.gang_leader:
                            gang_leader = user
                            gang_leader_password = password
                        else:
                            user.is_active = False
                            user.save()

                        final_users.append(user)
                    except IntegrityError as error:
                        gang.delete()
                        raise error
                    except KeyError as error2:
                        gang.delete()
                        raise error2
                    except:
                        gang.delete()
                        raise

                group = Group.objects.get(pk=2)
                group.user_set.add(gang_leader)
                gang_leader.sendGangLeaderEmail(gang_leader_password)

                return Response(gang_serializer.data)
            else:
                return Response(gang_serializer.errors,
                                status=status.HTTP_400_BAD_REQUEST)

        else:
            raise ValidationError('Missing Users')
