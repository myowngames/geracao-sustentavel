from django.contrib import admin

# Register your models here.
from django.contrib import admin
from django.contrib.auth.admin import UserAdmin
from mod_user.models.User import User

admin.site.register(User, UserAdmin)
