from rest_framework import serializers

from mod_user.models.User import User
# from mod_application.serializers.GangSerializer import GangSerializer


class UserSerializer(serializers.ModelSerializer):
    # gang = GangSerializer()

    class Meta:
        fields = ('last_login','term','username', 'is_staff', 'id', 'name', 'email', 'password', 'rg',
                  'cpf', 'phone', 'birthday', 'gang_leader', 'created', 'updated', 'gang', 'groups', 'city','is_active')
        model = User
