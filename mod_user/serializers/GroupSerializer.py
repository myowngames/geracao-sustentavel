from rest_framework import serializers

from django.contrib.auth.models import Group

from mod_user.serializers.PermissionSerializer import PermissionSerializer

class GroupSerializer(serializers.ModelSerializer):
	permissions = PermissionSerializer(many=True, read_only=False, required=False);
	
	class Meta:
		fields = ('id','name', 'permissions');
		model = Group;