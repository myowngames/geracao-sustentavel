from rest_framework import serializers

from django.contrib.auth.models import Permission
from mod_application.serializers.ContentTypeSerializer import ContentTypeSerializer


class PermissionSerializer(serializers.ModelSerializer):
    content_type = ContentTypeSerializer()

    class Meta:
        fields = ('id', 'content_type', 'codename', 'name')
        model = Permission
