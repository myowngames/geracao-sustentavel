from django.apps import AppConfig


class ModUserConfig(AppConfig):
    name = 'mod_user'
