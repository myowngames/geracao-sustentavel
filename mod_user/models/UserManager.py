from django.contrib.auth.models import BaseUserManager


class UserManager(BaseUserManager):
    def create_user(self, name, password=None, **extrafields):
        user = self.model(
            name=name,
            username=extrafields['username']
        )

        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_superuser(self, name, password, **extrafields):
        user = self.create_user(
            password=password,
            name=name,
            username=extrafields['username']
        )
        user.is_staff = True
        user.save(using=self._db)
        return user
