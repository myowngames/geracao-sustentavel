#  -*- coding: utf-8 -*-
from django.db import models
from django.contrib.auth.models import AbstractUser

from mod_user.models.UserManager import UserManager
from mod_application.models.Gang import Gang
from mod_application.models.City import City
from django.core.mail import send_mail
import sys  

def user_directory_path(instance, filename):
    # file will be uploaded to MEDIA_ROOT/user_<id>/<filename>
    return 'user_term/{0}_{1}'.format(instance.id, filename.encode(sys.getdefaultencoding()))

class User(AbstractUser):
    id = models.AutoField(primary_key=True, max_length=10)
    gang = models.ForeignKey(Gang, on_delete=models.CASCADE, related_name='user', null=True)
    term = models.FileField(upload_to=user_directory_path, null=True, blank=True)
    city = models.ForeignKey(City, on_delete=models.CASCADE, related_name='user', null=True, blank=True)
    name = models.CharField(max_length=255, null=False)
    email = models.EmailField(
        max_length=255,
        null=True,
        unique=True,
        error_messages={
            'invalid': "Email inválido",
            'unique': "Email já cadastrado!",
        }
    )
    password = models.CharField(max_length=255)
    rg = models.CharField(max_length=255, null=True)
    cpf = models.CharField(max_length=255, null=True)
    phone = models.CharField(max_length=255, null=True)
    birthday = models.DateField(null=True)
    gang_leader = models.BooleanField(default=False)

    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)

    # Remove django fields since we use email as username
    first_name = None
    last_name = None
    is_superuser = None
    date_joined = None

    # REQUIRED_FIELDS array
    REQUIRED_FIELDS = ['name']

    objects = UserManager()

    def user_directory_path(self, filename):
        user_directory_path(self, filename);

    def save(self, force_insert=False, force_update=False, using=None, update_fields=None):
        if force_insert:
            if not self.password:
                self.set_unusable_password()
            else:
                self.set_password(self.password)
        else:
            if self.pk:
                previousValues = User.objects.get(pk=self.pk)
                if previousValues:
                    if previousValues.password != self.password and self.password and self.password != "":
                        search = str(self.password).find('pbkdf2_sha256')
                        if search == -1:
                            self.set_password(self.password)

        super(User, self).save(force_insert, force_update, using, update_fields)

    def sendGangLeaderEmail(self, password):
    
        message = ""
        message += "Olá, Você criou um grupo com sucesso na Geração Sustentavel.<br />"
        message += "Para acessar use o username: <b>" + self.username + "</b><br/>"
        message += "e sua senha: <b>" + password + "</b> <br />"
        message += "Obrigado"
    
        send_mail(
            subject='Geração Sustentavel - Grupo',
            message=message,
            html_message=message,
            from_email='naoresponda@geracaosustentavel.org',
            recipient_list=[self.email]
        )