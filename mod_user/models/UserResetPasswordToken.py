from django.db import models

from mod_user.models.User import User

from django.conf import settings
from django.core.mail import send_mail

class UserResetPasswordToken(models.Model):
    user = models.OneToOneField(
        User,
        on_delete=models.CASCADE,
        primary_key=True,
        related_name='user_reset_password_token'
    )
    token = models.CharField(max_length=255, null=False)
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)

    def sendEmail(self, *args, **kwargs):
        if (self.user.email):

            resetLink = settings.ANGULAR_FRONTEND_BASE_URL + '#/auth/reset-password/' + str(self.token)

            message = ""
            message += "Olá, esta é uma solicitação de senha de troca em sua conta do Geração Sustentavel.<br />"
            message += "Seu usuario é: <b>" + self.user.username + "</b><br/>"
            message += "Seu link para troca de senha é: <a href='" + resetLink + "'>" + resetLink + "</a>. <br />"
            message += "Este link irá expirar em 30 minutos. <br />"
            message += "Obrigado"

            send_mail(
                subject='Geração Sustentavel - Troca de Senha do serializer',
                message=message,
                html_message=message,
                from_email='naoresponda@geracaosustentavel.org',
                recipient_list=[self.user.email]
            )
        else:
            raise ValidationError(
                "Usuário não contem endereço de email ligado a sua conta, por favor entre em contato com o administrador de seu grupo")