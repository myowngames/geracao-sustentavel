import { NgModule } from '@angular/core';
import { NgxEchartsModule } from 'ngx-echarts';

import { ThemeModule } from '../../@theme/theme.module';
import { VideosComponent } from './videos.component';
import { VideosRoutingModule } from './videos-routing.module';



@NgModule({
	imports: [
		ThemeModule,
		NgxEchartsModule,
		VideosRoutingModule,
	],
	declarations: [
		VideosComponent,
	],
	exports: [
		VideosComponent,
	],
})

export class VideosModule { }
