import { Component, Pipe, PipeTransform, OnInit } from '@angular/core';
import { SubscriptionComponent } from '../../@core/abstract_components/subscription.component';
import { DomSanitizer } from '@angular/platform-browser';


import { FlashService } from '../../@core/utils/flash.service';
import { GangPhaseDeliverableService } from '../../@core/data/gang-phase-deliverable.service';
import { GangPhaseDeliverableVoteService } from '../../@core/data/gang-phase-deliverable-vote.service';
import { UserService } from '../../@core/data/user.service';

import 'rxjs/add/operator/take'

@Component({
	selector: 'ngx-videos',
	styleUrls: ['./videos.component.scss'],
	templateUrl: './videos.component.html',
})

export class VideosComponent extends SubscriptionComponent implements OnInit {
	videos = [];
	cityGrouping = [];
	baseIframeLink = "https://www.youtube.com/embed/{{video_identifier}}/?rel=0";
	user: any = {};

	currentTab = 'firstTab';

	currentVoterVoteOn = null;

	//https://www.youtube.com/embed/0Nv2Q9u2PbE?rel=0

	constructor(
		private gangPhaseDeliverableService: GangPhaseDeliverableService,
		private gangPhaseDeliverableVoteService: GangPhaseDeliverableVoteService,
		private userService: UserService,
		private flashService: FlashService,
		private sanitizer: DomSanitizer,
	) {
		super();
	}

	ngOnInit() {
		this.getAllVideos();
		this.pushSub(this.userService.getCurrentUser().subscribe(
			(result) => {
				this.user = result;
			}
		));
	}
	youtube_parser(url) {
		var regExp = /^.*((youtu.be\/)|(v\/)|(\/u\/\w\/)|(embed\/)|(watch\?))\??v?=?([^#\&\?]*).*/;
		var match = url.match(regExp);
		return (match && match[7].length == 11) ? match[7] : false;
	}
	getAllVideos() {
		this.videos = [];
		this.cityGrouping = [];
		this.pushSub(this.gangPhaseDeliverableService.getAllVideos().subscribe(
			(result) => {
				this.videos = result;

				for (let i = 0; i < this.videos.length; i++) {

					//generate the YouTube Embed URL
					if (this.videos[i].video_link != null) {

						const video_identifier = this.youtube_parser(this.videos[i].video_link);
						if (video_identifier) {
							let link = this.baseIframeLink.replace("{{video_identifier}}", video_identifier);
							this.videos[i]['video_iframe_link'] = this.sanitizer.bypassSecurityTrustResourceUrl(link);
						}
						else {
							this.videos[i]['video_iframe_link'] = null;
						}
					}
					else {
						this.videos[i]['video_iframe_link'] = null;
					}

					//generate an array of cities so we can display by city
					if (this.cityGrouping.map(function (item) { return item.city_name }).indexOf(this.videos[i].gang_phase.gang.city.name) == -1) {
						let current = {
							city_name: this.videos[i].gang_phase.gang.city.name,
							videos_indexes: []
						}
						this.cityGrouping.push(current);
					}

					this.cityGrouping[this.cityGrouping.map(function (item) { return item.city_name }).indexOf(this.videos[i].gang_phase.gang.city.name)].videos_indexes.push(i);

					//get the votes for the current city user (is we are the city admin user)
					if (this.displayCityAdminVote == true) {
						this.pushSub(this.gangPhaseDeliverableVoteService.getPhaseDeliverableVotesAdminCity({ gang_phase_deliverable: this.videos[i].id }).subscribe(
							(result) => {
								if (result['votes'] == 0) {
									this.videos[i]['current_city_admin_user_total_votes'] = null;
								}
								else {
									this.videos[i]['current_city_admin_user_total_votes'] = result['votes']
								}
							}
						));
					}

					//get the vote of the current logged user
					if (this.displayVoterVote === true) {
						this.pushSub(this.gangPhaseDeliverableVoteService.getVoterUserVote().subscribe(
							(result) => {
								this.currentVoterVoteOn = result;
							}
						));
					}

					this.setVideoTotalVotes(i);

				}//for videos ends here
			}
		));
	}

	setVideoTotalVotes(videos_index) {
		this.pushSub(this.gangPhaseDeliverableVoteService.getVideoTotalVotes({ gang_phase_deliverable: this.videos[videos_index].id }).subscribe(
			(result) => {
				this.videos[videos_index]['total_votes'] = result['votes'];
			}
		));
	}

	saveCityGroupVote(video_index) {
		if (typeof this.videos[video_index]['current_city_admin_user_total_votes'] == 'undefined') {
			return;
		}

		if ((this.videos[video_index]['current_city_admin_user_total_votes'] == null || this.videos[video_index]['current_city_admin_user_total_votes'] == '') && this.videos[video_index]['current_city_admin_user_total_votes'] != 0) {
			this.flashService.push('error', 'O número de votos é inválido');
			this.getAllVideos();
		} else {
			this.pushSub(this.gangPhaseDeliverableVoteService.saveCityAdminVote({ total_votes: this.videos[video_index]['current_city_admin_user_total_votes'], gang_phase_deliverable: this.videos[video_index].id }).subscribe(
				(result) => {
					this.flashService.push('success', 'Os votos foram atualizados.');
					this.getAllVideos();
				}
			))
		}
	}


	removeVideoVotes(video_index) {
		this.videos[video_index]['current_city_admin_user_total_votes'] = 0;
		this.saveCityGroupVote(video_index);
		this.videos[video_index]['current_city_admin_user_total_votes'] = null;
	}


	changeTab(tab) {
		this.currentTab = tab;
	}


	nationalVotingIsVideoWithMoreVotes(video) {
		for (let i = 0; i < this.videosWithMoreVotesByCity.length; i++) {
			if (video.id != this.videosWithMoreVotesByCity[i].id) {
				if (video['total_votes'] < this.videosWithMoreVotesByCity[i]['total_votes']) {
					return false;
				}
			}
		}

		return true;
	}

	voterVoteOn(video) {
		this.currentVoterVoteOn = video.id;
	}


	sendVoterVote() {
		if (this.currentVoterVoteOn == null) {
			this.flashService.push('error', 'Favor, escolha o vídeo que deseja votar primeiro');
		} else {
			let data = {
				gang_phase_deliverable: this.currentVoterVoteOn
			}

			this.gangPhaseDeliverableVoteService.createEntry(data).subscribe(
				(result) => {
					this.flashService.push('success', 'Seu voto foi contabilizado.');
					this.getAllVideos();
				}
			);
		}
	}


	get displayCityAdminVote() {
		if (typeof this.user['groups'] != 'undefined') {
			if (this.user['groups'].length > 0) {
				//3 is the city group ID in the database
				return this.user.groups.indexOf(3) != -1;
			}
		}
		return false;
	}


	get displayIntercementAdminVote() {
		if (typeof this.user['groups'] != 'undefined') {
			if (this.user['groups'].length > 0) {
				return this.user.groups.indexOf(1) != -1;
			}
		}
		return false;
	}

	get displayVoterVote() {
		if (typeof this.user['groups'] != 'undefined') {
			if (this.user['groups'].length > 0) {
				return this.user.groups.indexOf(4) != -1;
			}
		}
		return false;
	}

	//used by city admin, tab vencedor da cidade
	get videoWithMoreVotes() {
		let video = {};

		for (let i = 0; i < this.videos.length; i++) {
			if (typeof video['id'] == 'undefined') {
				video = this.videos[0];
				continue;
			}

			if (video['total_votes'] < this.videos[i]['total_votes']) {
				video = this.videos[i];
			}
		}

		return video;
	}


	get videosWithMoreVotesByCity() {
		let videos = [];

		for (let i = 0; i < this.cityGrouping.length; i++) {
			let topVideo = {};

			for (let j = 0; j < this.cityGrouping[i]['videos_indexes'].length; j++) {
				if (typeof topVideo['id'] == 'undefined') {
					topVideo = this.videos[this.cityGrouping[i]['videos_indexes'][j]];
					continue;
				}

				if (topVideo['total_votes'] < this.videos[this.cityGrouping[i]['videos_indexes'][j]]['total_votes']) {
					topVideo = this.videos[this.cityGrouping[i]['videos_indexes'][j]];
				}
			}

			videos.push(topVideo);
		}
		return videos;
	}



	get isUserVoteThere() {
		if (typeof this.currentVoterVoteOn == 'object') {
			return false;
		}
		else if (this.currentVoterVoteOn == null) {
			return false;
		}

		return true;
	}


}
