import { RouterModule, Routes, } from '@angular/router';
import { NgModule } from '@angular/core';

import { PhasesComponent } from './phases.component';
import { NewPhaseComponent } from './new/new-phase.component';

const routes: Routes = [
    {
        path: '',
        component: PhasesComponent,
    },
    {
        path: 'edit/:id',
        component: NewPhaseComponent,
    },
    {
        path: 'new',
        component: NewPhaseComponent,
    },
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule],
})
export class PhasesRoutingModule {

}
