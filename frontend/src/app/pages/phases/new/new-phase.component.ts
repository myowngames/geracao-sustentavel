import { Component, OnInit } from '@angular/core';
import { CanActivate, Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, Validators, FormControl, FormArray } from '@angular/forms';

import { SubscriptionComponent } from '../../../@core/abstract_components/subscription.component';
import { FlashService } from '../../../@core/utils/flash.service';
import { PhaseService } from '../../../@core/data/phase.service';
import { DeliverableService } from '../../../@core/data/deliverable.service';

import { NgbDatepickerConfig, NgbDateParserFormatter } from '@ng-bootstrap/ng-bootstrap';
import { NgbDateBRParserFormatter } from '../../../@core/utils/ngb-date-br-parser-formatter';

import * as moment from 'moment';
import { DeliverableFileService } from '../../../@core/data/deliverable-file.service';

import '../../../@core/utils/ckeditor.loader';
import 'ckeditor';

@Component({
	selector: 'ngx-new-phase',
	styleUrls: ['./new-phase.component.scss'],
	templateUrl: './new-phase.component.html',
	providers: [{ provide: NgbDateParserFormatter, useClass: NgbDateBRParserFormatter }],
})


export class NewPhaseComponent extends SubscriptionComponent implements OnInit {
	public form;
	public phase: any = {};
	public submitted: boolean = false;
	public html: string;
	public allPhases: any = [];
	public deliverableAllowedContentTypes = [];
	public deliverableSampleFiles = [];
	public instructions_html: string = '';
	public tinymceConfig: any = {
		width: '100%',
	}

	constructor(private phaseService: PhaseService,
		private flashService: FlashService,
		private deliverableService: DeliverableService,
		private deliverableFileService: DeliverableFileService,
		private fb: FormBuilder,
		private router: Router,
		private activatedRoute: ActivatedRoute) {
		super();
		this.phase.id = this.activatedRoute.params['value'].id;
	}

	ngOnInit() {
		this.form = this.fb.group({
			id: [
				this.phase.id,
				Validators.compose([]),
			],
			name: [
				this.phase.name,
				Validators.compose([Validators.required, Validators.minLength(2)]),
			],
			start_at: [
				this.phase.start_at,
				Validators.compose([Validators.required, Validators.minLength(2)]),
			],
			end_at: [
				this.phase.end_at,
				Validators.compose([]),
			],
			instructions: [
				this.phase.instructions,
				Validators.compose([]),
			],
			phase_depends: [
				this.phase.phase_depends,
				Validators.compose([]),
			],
			deliverable: this.fb.array([]),
			created: [
				this.phase.created,
				Validators.compose([]),
			],
			updated: [
				this.phase.updated,
				Validators.compose([]),
			],
		});
		if (this.phase.id) {
			this.getServiceInfor();
		}
		this.getAllPhases();
		this.getDeliverableAllowedContentTypes();
	}

	getDeliverableAllowedContentTypes() {
		this.deliverableService.getAllowedContentTypes().subscribe(
			(result) => {
				this.deliverableAllowedContentTypes = result;
			},
		);
	}

	getAllPhases() {
		this.pushSub(
			this.phaseService.getAllEntries().subscribe(
				(result) => {
					this.allPhases = result;
					if (this.phase.id) {
						for (let i = this.allPhases.length - 1; i >= 0; i--) {
							if (this.allPhases[i].id === this.phase.id) {
								this.allPhases.splice(i, 1);
								break;
							}
						}
					}
				},
			),
		)
	}

	getServiceInfor(): void {
		this.pushSub(
			this.phaseService.getSingleEntry(this.phase.id, {}).subscribe(
				result => {
					this.phase = result;
					console.log(this.phase)
					this.instructions_html = this.phase.instructions;
					this.pushSub(this.phaseService.getDeliverable(this.phase.id).subscribe(
						(dresult) => {
							for (let i = 0; i < dresult.length; i++) {
								if (!dresult[i]['files']) {
									dresult[i]['files'] = [];
								}
								this.createNewDeliverable({
									id: dresult[i]['id'],
									name: dresult[i]['name'],
									description: dresult[i]['description'],
									files: dresult[i]['files'],
								});
							}
						},
					));
					const start = moment(this.phase.start_at, 'YYYY-MM-DD');
					this.phase.start_at = {
						'year': start.get('year'),
						'month': start.get('month') + 1,
						'day': start.get('date'),
					};
					const end = moment(this.phase.end_at, 'YYYY-MM-DD');
					this.phase.end_at = {
						'year': end.get('year'),
						'month': end.get('month') + 1,
						'day': end.get('date'),
					};
					this.initFormValues();
				},
			),
		);
	}

	initFormValues(): void {
		this.form.patchValue(this.phase);
	}

	createNewFile(index: number, contents: Object = {}): void {
		const defaultObj = {
			id: null,
			name: null,
			sample: null,
			allowed_content_types: null,
			votable: false,
		};
		const usableObj = Object.assign({}, defaultObj, contents);
		const filesFormObject: FormGroup = new FormGroup({
			id: new FormControl(usableObj['id']),
			name: new FormControl(usableObj['name'], Validators.required),
			sample: new FormControl(usableObj['sample']),
			allowed_content_types: new FormControl(usableObj['allowed_content_types'], Validators.required),
			votable: new FormControl(usableObj['votable'], Validators.required),
		});
		this.form.get('deliverable').controls[index].get('files').push(filesFormObject);
	}

	createNewDeliverable(contents = {}): void {
		const defaultObj = {
			id: null,
			name: null,
			description: null,
			files: [],
		};
		const usableObj = Object.assign({}, defaultObj, contents);
		const deliverableFormObject: FormGroup = new FormGroup({
			id: new FormControl(usableObj['id']),
			name: new FormControl(usableObj['name'], Validators.required),
			description: new FormControl(usableObj['description']),
			files: this.fb.array([]),
		});

		this.form.get('deliverable').push(deliverableFormObject);
		if (usableObj.files && usableObj.files.length > 0) {
			for (let i = 0; i < usableObj.files.length; i++) {
				this.createNewFile(this.form.get('deliverable').controls.length - 1, usableObj.files[i]);
			}
		}
	}

	removeFile(index = null, fileIndex = null) {
		const files: FormArray = this.form.get('deliverable').controls[index].get('files');
		const file = files.controls[fileIndex];
		if (file.get('id').value) {

			this.pushSub(
				this.deliverableFileService.deleteEntry(file.get('id').value).subscribe(
					result => {
						files.removeAt(fileIndex);
						if (typeof this.deliverableSampleFiles[index][fileIndex] !== 'undefined') {
							this.deliverableSampleFiles[index].splice(fileIndex, 1);
						}
						this.flashService.push('success', 'Arquivo removido com sucesso');
					},
				),
			)

		} else {
			files.removeAt(fileIndex);
			if (this.deliverableSampleFiles[index] && typeof this.deliverableSampleFiles[index][fileIndex] !== 'undefined') {
				this.deliverableSampleFiles[index].splice(fileIndex, 1);
			}
		}

	}

	removeDeliverable(index = null) {
		if (index != null) {
			if (this.form.get('deliverable').at(index).get('id').value != null) {
				this.pushSub(this.deliverableService.deleteEntry(this.form.get('deliverable').at(index).get('id').value).subscribe(
					(result) => {

						this.form.get('deliverable').removeAt(index);
						if (typeof this.deliverableSampleFiles[index] !== 'undefined') {
							this.deliverableSampleFiles.splice(index, 1);
						}

						this.flashService.push('success', 'Item removido com sucesso');
					},
				));
			}
			else {
				this.form.get('deliverable').removeAt(index);
				if (typeof this.deliverableSampleFiles[index] !== 'undefined') {
					this.deliverableSampleFiles.splice(index, 1);
				}
			}
		}

	}

	onFileChange($event, deliverableIndex, fileIndex) {
		if (!this.deliverableSampleFiles[deliverableIndex]) {
			this.deliverableSampleFiles[deliverableIndex] = [];
		}
		this.deliverableSampleFiles[deliverableIndex][fileIndex] = $event.target.files[0]; // <--- File Object for future use.
		this.form.get('deliverable').at(deliverableIndex).get('files').at(fileIndex).get('sample').setValue(
			this.deliverableSampleFiles[deliverableIndex][fileIndex].name ? this.deliverableSampleFiles[deliverableIndex][fileIndex].name : null,
		); // <-- Set Value for Validation
	}

	removeFileSample(di, fi): void {
		// Not working the way i want
		if (this.deliverableSampleFiles[di]) {
			if (this.deliverableSampleFiles[di][fi]) {
				this.deliverableSampleFiles[di][fi].sample = ' ';
			}
		}
		if (this.phase.deliverable[di]) {
			if (this.phase.deliverable[di].files[fi]) {
				this.phase.deliverable[di].files[fi].sample = ' ';
			}
		}
		this.form.get('deliverable').at(di).get('files').at(fi).get('sample').setValue(' ');
	}


	onSubmit() {
		this.submitted = true;
		const data = this.form.value;

		if (data.start_at != null) {
			if (data.start_at.year) {
				data.start_at = moment().set({
					'year': data.start_at.year,
					'month': data.start_at.month - 1,
					'date': data.start_at.day,
				})
					.format('YYYY-MM-DD');
			}
		}

		if (data.end_at != null) {
			if (data.end_at.year) {
				data.end_at = moment().set({
					'year': data.end_at.year,
					'month': data.end_at.month - 1,
					'date': data.end_at.day,
				})
					.format('YYYY-MM-DD');
			}
		}
		if (this.phase.id != null && typeof this.phase.id != 'undefined') {
			this.pushSub(
				this.phaseService.updateEntry(this.phase.id, data).subscribe(
					(result) => {
						this.phase = result;
						this.sendDeliverables(data.deliverable).then(
							(result) => {
								this.submitted = false;
								this.flashService.push('success', 'Fase atualizada com sucesso!');
								this.router.navigate(['admin', 'phases']);
							},
							(error) => {
								this.submitted = false;
							},
						)
					},
					(error) => {
						this.submitted = false;
					},
				),
			)
		} else {
			this.pushSub(
				this.phaseService.createEntry(data).subscribe(
					(result) => {
						this.phase = result;
						this.sendDeliverables(data.deliverable).then(
							(result) => {
								this.flashService.push('success', 'Fase criada com sucesso!');
								this.submitted = false;
								this.router.navigate(['admin', 'phases']);
							},
							(error) => {
								this.submitted = false;
							},
						)
					},
					(error) => {
						this.submitted = false;
					},
				),
			)
		}
	}

	sendFiles(files = [], deliverable_index: number, deliverable_id: number): Promise<any> {
		const options = {
			headers: { 'Content-Type': 'multipart/form-data' },
		};
		return new Promise((resolve, reject) => {
			if (files.length <= 0) {
				resolve();
			}

			for (let i = 0; i < files.length; i++) {
				files[i]['deliverable'] = deliverable_id.toString();
				const data = files[i];
				if (!data.sample || typeof data.sample.size === 'undefined') {
					delete (data.sample);
				}
				if (this.deliverableSampleFiles[deliverable_index] && typeof this.deliverableSampleFiles[deliverable_index][i] !== 'undefined') {
					data['sample'] = this.deliverableSampleFiles[deliverable_index][i];
				}
				if (!files[i].id) {
					this.pushSub(
						this.deliverableFileService.createEntryWithFile(data, options).subscribe(
							(result) => {
								if (i === (files.length - 1)) {
									resolve(result)
								}
							},
							(error) => {
								reject(error);
							},
						),
					)
				} else {
					this.pushSub(
						this.deliverableFileService.UpdateEntryWithFile(files[i].id, data, options, 'PUT').subscribe(
							(result) => {
								if (i === (files.length - 1)) {
									resolve(result)
								}
							},
							(error) => {
								reject(error);
							},
						),
					)
				}
			}
		})
	}

	sendDeliverables(deliverables = [], i = 0): Promise<any> {
		return new Promise((resolve, reject) => {
			if (deliverables.length <= 0) {
				resolve();
			}
			const deliverable: any = deliverables[i];
			deliverable.phase = this.phase.id;

			if (deliverable.id) {
				this.pushSub(
					this.deliverableService.updateEntry(deliverable.id, deliverable).subscribe(
						(result) => {
							this.sendFiles(deliverable.files, i, result.id).then(
								(fr) => {
									if (i === deliverables.length - 1) {
										resolve(fr);
									} else {
										i++;
										this.sendDeliverables(deliverables, i).then(
											(re) => {
												resolve(re);
											},
											(rj) => {
												reject(rj);
											},
										)
									}
								},
								(fj) => {
									reject(fj);
								},
							)
						},
						(err) => {
							reject(err);
						},
					),
				)
			} else {
				this.pushSub(
					this.deliverableService.createEntry(deliverable).subscribe(
						(result) => {
							this.sendFiles(deliverable.files, i, result.id).then(
								(fr) => {
									if (i === deliverables.length - 1) {
										resolve(fr);
									} else {
										i++;
										this.sendDeliverables(deliverables, i).then(
											(re) => {
												resolve(re);
											},
											(rj) => {
												reject(rj);
											},
										)
									}
								},
								(fj) => {
									reject(fj);
								},
							)
						},
						(err) => {
							reject(err);
						},
					),
				)
			}
		})
	}
}
