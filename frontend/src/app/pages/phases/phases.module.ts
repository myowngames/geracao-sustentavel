import { NgModule } from '@angular/core';
import { NgxEchartsModule } from 'ngx-echarts';
import { DirectivesModule } from '../../@core/directives/directives.module';
import { ThemeModule } from '../../@theme/theme.module';
import { PhasesComponent } from './phases.component';
import { NewPhaseComponent } from './new/new-phase.component';
import { PhasesRoutingModule } from './phases-routing.module'
import { Ng2SmartTableModule } from 'ng2-smart-table';
import { FormsModule } from '@angular/forms';
import { TextMaskModule } from 'angular2-text-mask';
import { CKEditorModule } from 'ng2-ckeditor';
@NgModule({
    imports: [
      ThemeModule,
      NgxEchartsModule,
      PhasesRoutingModule,
      Ng2SmartTableModule,
      DirectivesModule,
      FormsModule,
      TextMaskModule,
      CKEditorModule,

    ],
    declarations: [
      PhasesComponent,
      NewPhaseComponent,
    ],
})
export class PhasesModule { }
