import { Component, Injector, OnDestroy, OnInit } from '@angular/core';
import { NgbDateStruct } from '@ng-bootstrap/ng-bootstrap';

import { CanActivate, Router, ActivatedRoute } from '@angular/router';

import { LocalDataSource } from 'ng2-smart-table';
import { SmartTableRemoteDataSource } from '../../@core/data/smart-table_remote-data-source';

import { SubscriptionComponent } from '../../@core/abstract_components/subscription.component';
import { PhaseService } from '../../@core/data/phase.service';
import { GangPhaseService } from '../../@core/data/gang-phase.service';
import { GangService } from '../../@core/data/gang.service';
import { FlashService } from '../../@core/utils/flash.service';

import { Moment } from 'moment';
import * as moment from 'moment';
import { GangPhaseFilteredService } from '../../@core/data/gang-phase-filtered.service';
import { HttpClient } from '@angular/common/http';
import { NbAuthService } from '@nebular/auth';

@Component({
	selector: 'ngx-dashboard',
	styleUrls: ['./phases.component.scss'],
	templateUrl: './phases.component.html',
})
export class PhasesComponent extends SubscriptionComponent implements OnDestroy, OnInit {
	model: NgbDateStruct;
	date: { year: number, month: number };
	phases: any;
	allGangs = [];

	settings = {
		noDataMessage: 'Nenhuma Informação Encontrada',
		editable: false,
		mode: 'external',
		actions: false,
		hideSubHeader: true,
		filter: false,
		pager: {
			perPage: 20,
			display: true,
		},
		add: {
			addButtonContent: false,
			createButtonContent: false,
			cancelButtonContent: false,
		},
		edit: {
			editButtonContent: false,
			saveButtonContent: false,
			cancelButtonContent: false,
		},
		delete: {
			deleteButtonContent: false,
			confirmDelete: true,
		},
		columns: {
			gang__city__name: {
				title: 'Cidade',
				type: 'html',
				//filter: false,
				filterFunction: function(){
					return 'city_name';
				},
				valuePrepareFunction: function (cell, row) {
					const gang = this.getGroupFromId(row.gang);
					if (gang && gang.city && gang.city.name) {
						return gang.city.name;
					} else {
						return '';
					}
				}.bind(this),
			},
			completed: {
				title: 'Finalizado',
				type: 'html',
				sear: false,
				valuePrepareFunction: function (cell, row) {
					return row.completed === true ? 'Sim' : 'Não';
				}.bind(this),
			},
			gang__school: {
				title: 'Escola',
				type: 'html',
				valuePrepareFunction: function (cell, row) {
					return this.getGroupFromId(row.gang).school;
				}.bind(this),
			},
			gang__name: {
				title: 'Nome',
				type: 'html',
				valuePrepareFunction: function (cell, row) {
					return this.getGroupFromId(row.gang).name;
				}.bind(this),
			},
		},
	};

	sources: any = [];
	constructor(private gangService: GangService,
		private phaseService: PhaseService,
		private gangPhaseService: GangPhaseFilteredService,
		private router: Router,
		private activeRoute: ActivatedRoute,
		private flashService: FlashService) {
		super();
	}
	ngOnDestroy() {
	}
	ngOnInit() {
		this.pushSub(
			this.phaseService.getAllEntries().subscribe(
				result => {
					this.phases = result;
					for (let i = 0; i < this.phases.length; i++) {
						this.gangPhaseService.setLink(this.phases[i].id);
						const service = Object.assign(
							Object.create(
								Object.getPrototypeOf(this.gangPhaseService),
							),
							this.gangPhaseService,
						);
						this.phases[i].source = new SmartTableRemoteDataSource(service);
					}
				},
			),
		);

		this.pushSub(this.gangService.getAllEntries().subscribe(
			(result) => {
				this.allGangs = result;
			},
		));
	}

	getDateMoment(date): Moment {
		return moment(date, 'YYYY-MM-DD', true);
	}

	printDate(date): string {
		return this.getDateMoment(date).format('DD/MM/YYYY');
	}

	createNewEntryButtonClickListener(): void {
		this.router.navigate(['admin', 'phases', 'new']);
	}

	editEntryButtonClickListener(id): void {
		this.router.navigate(['admin', 'phases', 'edit', id]);
	}
	onUserRowSelect(event): void {
		this.router.navigate(['admin', 'groups', 'view', event.data.gang]);
	}

	getGroupFromId(id) {
		for (let i = 0; i < this.allGangs.length; i++) {
			if (this.allGangs[i].id === id) {
				return this.allGangs[i];
			}
		}
	}
}
