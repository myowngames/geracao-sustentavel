import { Component, OnDestroy, OnInit } from '@angular/core';

import { CanActivate, Router, ActivatedRoute } from '@angular/router';

import { LocalDataSource } from 'ng2-smart-table';
import { SmartTableRemoteDataSource } from '../../@core/data/smart-table_remote-data-source';

import { SubscriptionComponent } from '../../@core/abstract_components/subscription.component';
import { FrontEndFileService } from '../../@core/data/frontend-file.service';
import { UserService } from '../../@core/data/user.service';
import { FlashService } from '../../@core/utils/flash.service';
import { ImageRenderComponent, DateRenderComponent } from '../../@theme/components/smart-table-render';

@Component({
	selector: 'ngx-dashboard',
	styleUrls: ['./frontend-files.component.scss'],
	templateUrl: './frontend-files.component.html',
})
export class FrontendFilesComponent extends SubscriptionComponent implements OnDestroy, OnInit {
	settings = {
		noDataMessage: 'Nenhuma Informação Encontrada',
		editable: false,
		mode: 'external',
		pager: {
			perPage: 20,
			display: true,
		},
		actions: {
			columnTitle: 'Ações',
		},
		add: {
			addButtonContent: '<i class="nb-plus"></i>',
			createButtonContent: '<i class="nb-checkmark"></i>',
			cancelButtonContent: '<i class="nb-close"></i>',
		},
		edit: {
			editButtonContent: '<i class="fa fa-edit"></i>',
			saveButtonContent: '<i class="nb-checkmark"></i>',
			cancelButtonContent: '<i class="nb-close"></i>',
		},
		delete: {
			deleteButtonContent: '<i class="nb-trash"></i>',
			confirmDelete: true,
		},
		columns: {
			name: {
				title: 'Nome',
				type: 'string',
			},
			file: {
				title: 'Arquivo',
				type: 'custom',
				renderComponent: ImageRenderComponent,
			},
			updated: {
				filter: false,
				title: 'Alterado',
				type: 'custom',
				renderComponent: DateRenderComponent,
			},
			created: {
				filter: false,
				title: 'Criado',
				type: 'custom',
				renderComponent: DateRenderComponent,
			},

		},
	};
	settings_public = {
		noDataMessage: 'Nenhuma Informação Encontrada',
		editable: false,
		mode: 'external',
		pager: {
			perPage: 20,
			display: true,
		},
		actions: false,
		add: {
			addButtonContent: '<i class="nb-plus"></i>',
			createButtonContent: '<i class="nb-checkmark"></i>',
			cancelButtonContent: '<i class="nb-close"></i>',
		},
		edit: {
			editButtonContent: '<i class="fa fa-edit"></i>',
			saveButtonContent: '<i class="nb-checkmark"></i>',
			cancelButtonContent: '<i class="nb-close"></i>',
		},
		delete: {
			deleteButtonContent: '<i class="nb-trash"></i>',
			confirmDelete: true,
		},
		columns: {
			name: {
				title: 'Nome',
				type: 'string',
			},
			file: {
				title: 'Arquivo',
				type: 'custom',
				renderComponent: ImageRenderComponent,
			},
			updated: {
				title: 'Alterado',
				type: 'custom',
				renderComponent: DateRenderComponent,
			},
			created: {
				title: 'Criado',
				type: 'custom',
				renderComponent: DateRenderComponent,
			},

		},
	};
	source: SmartTableRemoteDataSource = new SmartTableRemoteDataSource(this.frontEndFileService);
	public user: any = [];
	constructor(
		private frontEndFileService: FrontEndFileService,
		private router: Router,
		private activeRoute: ActivatedRoute,
		private flashService: FlashService,
		private userService: UserService,
	) {
		super();
		this.getUser();
	}
	getUser(): void {
		this.pushSub(
			this.userService.getCurrentUser().subscribe(
				(result) => {
					this.user = result;

				},
				(error) => {

				},
			),
		)
	}
	ngOnDestroy() {

	}
	ngOnInit() {

	}
	createNewEntryButtonClickListener(): void {
		this.router.navigate(['admin', 'frontend-files', 'new']);
	}

	editEntryButtonClickListener(event): void {
		this.router.navigate(['admin', 'frontend-files', 'edit', event.data.id]);
	}

	deleteEntryButtonListener(event): void {
		this.frontEndFileService.deleteEntry(event.data.id).subscribe(
			(result) => {
				if (result != false) {
					this.source.remove(event.data);
					this.flashService.push('success', 'Arquivo Removido com Sucesso!');
				}
			},
		);
	}
}
