import { NgModule } from '@angular/core';
import { NgxEchartsModule } from 'ngx-echarts';

import { ThemeModule } from '../../@theme/theme.module';
import { FrontEndFilesRoutingModule } from './frontend-files-routing.module';
import { FrontendFilesComponent } from './frontend-files.component';
import { NewFrontendFileComponent } from './new/new-frontend-file.component';

import { Ng2SmartTableModule } from 'ng2-smart-table';
import { DirectivesModule } from '../../@core/directives/directives.module';

@NgModule({
    imports: [
        ThemeModule,
        NgxEchartsModule,
        FrontEndFilesRoutingModule,
        Ng2SmartTableModule,
        DirectivesModule
    ],
    declarations: [
        FrontendFilesComponent,
        NewFrontendFileComponent
    ],
})
export class FrontendFilesModule { }