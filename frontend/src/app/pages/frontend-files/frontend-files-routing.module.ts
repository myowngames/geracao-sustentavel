import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';

import { FrontendFilesComponent } from './frontend-files.component';
import { NewFrontendFileComponent } from './new/new-frontend-file.component';


const routes: Routes = [
    {
        path: '',
        component: FrontendFilesComponent,
    },
    {
        path: 'edit/:id',
        component: NewFrontendFileComponent,
    },
    {
        path: 'new',
        component: NewFrontendFileComponent,
    },
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule],
})
export class FrontEndFilesRoutingModule {

}
