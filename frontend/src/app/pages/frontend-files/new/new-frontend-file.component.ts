import { Component, OnDestroy } from '@angular/core';
import { CanActivate, Router, ActivatedRoute  } from '@angular/router';
import { FormBuilder, FormGroup, Validators, FormControl, FormArray } from '@angular/forms';

import { SubscriptionComponent } from '../../../@core/abstract_components/subscription.component';
import { FlashService } from '../../../@core/utils/flash.service';
import { FrontEndFileService } from '../../../@core/data/frontend-file.service';
import { UserService } from '../../../@core/data/user.service';
@Component({
	selector: 'new-frontend-file',
	styleUrls: ['./new-frontend-file.component.scss'],
	templateUrl: './new-frontend-file.component.html',
})
export class NewFrontendFileComponent extends SubscriptionComponent implements OnDestroy {
    public form;
    public user;
    public file;
	  public frontendFile: any = {};
    public objectKeys = Object.keys;
    public submitted: boolean = false;


    public fileName = '';

    constructor(
    	private frontEndFileService: FrontEndFileService,
        private flashService: FlashService,
    	private fb: FormBuilder,
    	private router: Router,
    	private activatedRoute: ActivatedRoute,
        private userService: UserService,
    ) {
        super();
    	this.frontendFile.id = this.activatedRoute.params['value'].id;

    }
    ngOnDestroy() {

    }
    getUser(): void {
        this.pushSub(
            this.userService.getCurrentUser().subscribe(
                (result) => {
                    this.user = result;
                    if (!this.user.is_staff) {
                        this.flashService.push('error', 'Você não tem permissão para ver essa pagina!');
                        this.router.navigate(['admin', 'frontend-files']);
                    }
                    if (this.frontendFile.id) {
                        this.getServiceInfor();
                    }
                },
                (error) => {

                },
            ),
        )
    }
    ngOnInit() {
        this.form = this.fb.group({
            id: [
                this.frontendFile.id,
                Validators.compose([]),
            ],
            name: [
                this.frontendFile.name,
                Validators.compose ([Validators.required, Validators.minLength(2)]),
            ],
            file: [
                this.frontendFile.file,
                Validators.compose ([]),
            ],
            created: [
                this.frontendFile.created,
                Validators.compose([]),
            ],
            updated: [
                this.frontendFile.updated,
                Validators.compose([]),
            ],
        });
        this.getUser()
    }


    getServiceInfor(): void {
        this.pushSub(
            this.frontEndFileService.getSingleEntry(this.frontendFile.id, {}).subscribe(
                result => {
                    this.frontendFile = result;
                    this.initFormValues();
                },
            ),
        );
    }

    onFileChange($event) {
        this.file = $event.target.files[0]; // <--- File Object for future use.
        this.form.controls['file'].setValue(this.file ? this.file.name : ''); // <-- Set Value for Validation
    }

    initFormValues(): void {
    	this.form.patchValue(this.frontendFile);
    }

  onSubmit() {
    this.submitted = true;
    const data = this.form.value;
    if (!this.file) {
      delete data.file;
    } else {
      data.file = this.file;
    }

    if (this.frontendFile.id != null && typeof this.frontendFile.id !== 'undefined') {
        this.pushSub(
            this.frontEndFileService.UpdateEntryWithFile(this.frontendFile.id, data).subscribe(
                (result) => {
                    this.flashService.push('success', 'Arquivo atualizado com sucesso!');
                    this.frontendFile = result;
                    this.router.navigate(['admin', 'frontend-files']);
                    this.submitted = false;
                },
                (error) => {
                    this.submitted = false;
                },
            ),
        );
    } else {
        this.pushSub(
            this.frontEndFileService.createEntryWithFile(data).subscribe(
                (result) => {
                    this.flashService.push('success', 'Arquivo criada com sucesso!');
                    this.frontendFile = result;
                    this.router.navigate(['admin', 'frontend-files']);
                    this.submitted = false;
                },
                (error) => {
                    this.submitted = false;
                },
            ),
        )

    }

    }

}
