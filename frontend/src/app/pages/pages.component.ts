import {Component, OnDestroy, OnInit} from '@angular/core';

import {MENU_ITEMS} from './pages-menu';

import {SubscriptionComponent} from '../@core/abstract_components/subscription.component';
import {UserService} from '../@core/data/user.service';
import {NavigationEnd, Router} from "@angular/router";

@Component({
  selector: 'ngx-pages',
  template: `
    <ngx-sample-layout>
      <nb-menu [items]="menu"></nb-menu>
      <router-outlet>
      
      </router-outlet>
    </ngx-sample-layout>
  `,
})


export class PagesComponent extends SubscriptionComponent {
  menu = MENU_ITEMS;

  constructor(private userService: UserService, private router: Router) {
    super();
  }

  ngOnInit() {
    for (let i = 0; i < this.menu.length; i++) {
      this.menu[i].hidden = true;
    }

    this.defineUserMenu();

    this.router.events.subscribe((evt) => {
      if (!(evt instanceof NavigationEnd)) {
        return;
      }
      let id = evt.url.split('#')[1];
      if(id){
        setTimeout(function(){
          let target = document.getElementById(id);
          if(target){
            target.scrollIntoView();
          }
        }, 1000)
      }
    });

  }


  defineUserMenu(): void {
    this.pushSub(this.userService.getCurrentUser().subscribe(
      (user) => {
        if(user.is_staff){
          user.groups.push(0);
        }
        for (let i = 0; i < this.menu.length; i++) {
          if (typeof this.menu[i]['data'] !== 'undefined') {
            if (typeof this.menu[i].data['allowed_groups'] !== 'undefined') {
              for (let j = 0; j < user.groups.length; j++) {
                if (this.menu[i].data['allowed_groups'].indexOf(user.groups[j]) != -1) {
                  this.menu[i].hidden = false;
                }
              }
            }
          }

        }
      }
    ));
  }


}
