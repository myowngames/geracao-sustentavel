import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

import { SubscriptionComponent } from '../../../@core/abstract_components/subscription.component';
import { FlashService } from '../../../@core/utils/flash.service';
import { GangService } from '../../../@core/data/gang.service';
import { UserService } from '../../../@core/data/user.service';
import { PhaseService } from '../../../@core/data/phase.service';
import * as moment from 'moment';
import { GangPhaseService } from '../../../@core/data/gang-phase.service';
import { environment } from '../../../../environments/environment';
import { DomSanitizer } from '@angular/platform-browser';
@Component({
	selector: 'ngx-view-gang',
	styleUrls: ['./view-gang.component.scss'],
	templateUrl: './view-gang.component.html',
})
export class ViewGangComponent extends SubscriptionComponent implements OnInit {

	public form;
	public gang: any = {};
	public users: any = [];
	public submitted: boolean = false;
	public phases: any = [];
	public user: any = [];
	public currentGroup: number = 0;
	private baseIframeLink = 'https://www.youtube.com/embed/{{video_identifier}}/?rel=0';
	constructor(private gangService: GangService,
		private flashService: FlashService,
		private userService: UserService,
		private phaseService: PhaseService,
		private gangPhaseSerive: GangPhaseService,
		private router: Router,
		private activatedRoute: ActivatedRoute,
		private sanitizer: DomSanitizer,
	) {
		super();
		this.gang.id = this.activatedRoute.params['value'].id;
	}

	ngOnInit() {
		if (!this.gang.id) {
			this.flashService.push('error', 'Precisamos saber qual grupo você quer ver');
			this.router.navigate(['admin', 'dashboard']);
		} else {
			this.userService.getCurrentUser().subscribe(
				(result) => {
					this.user = result;
					this.currentGroup = this.user.groups[0];
					if (this.user.is_staff || this.user.groups[0] === 1 ||
						this.user.groups[0] === 3 || this.gang.id === this.user.gang) {

					} else {
						this.gang.id = this.user.gang;
					}
					this.getServiceInfo();

					this.getGangPhases();
				},
			);
		}
	}

	printDate(date): string {
		if (date) {
			return moment(date, 'YYYY-MM-DD', true).format('DD/MM/YYYY');
		} else {
			return '';
		}
	}
	printCreatedDate(date): string {
		if (date) {
			return moment(new Date(date.toString())).format('DD/MM/YYYY HH:mm:ss');
		} else {
			return '';
		}
	}
	youtube_parser(url) {
		var regExp = /^.*((youtu.be\/)|(v\/)|(\/u\/\w\/)|(embed\/)|(watch\?))\??v?=?([^#\&\?]*).*/;
		var match = url.match(regExp);
		return (match && match[7].length == 11) ? match[7] : false;
	}
	getIframeVideoLink(video_link: string) {
		if (video_link != null) {
			const video_identifier = this.youtube_parser(video_link);
			if (video_identifier) {
				const link = this.baseIframeLink.replace('{{video_identifier}}', video_identifier);
				return this.sanitizer.bypassSecurityTrustResourceUrl(link);
			} else {
				return null;
			}
		} else {
			return null;
		}
	}
	getGangPhases(): void {
		this.pushSub(
			this.gangService.getGangPhases(this.gang.id).subscribe(
				(result) => {
					this.phases = result;
					for (let i = 0; i < this.phases.length; i++) {
						for (let j = 0; j < this.phases[i].phase.deliverable.length; j++) {
							for (let h = 0; h < this.phases[i].phase.deliverable[j].files.length; h++) {
								for (let g = 0; g < this.phases[i].gang_phase_deliverable.length; g++) {
									// this.phases[i].phase.deliverable[j].files[h].uploaded = false;
									if (this.phases[i].phase.deliverable[j].files[h].id === this.phases[i].gang_phase_deliverable[g].deliverable_file) {
										this.phases[i].phase.deliverable[j].files[h].uploaded = this.phases[i].gang_phase_deliverable[g];
										this.phases[i].phase.deliverable[j].files[h].uploaded.file = this.getUrl(this.phases[i].phase.deliverable[j].files[h].uploaded.file);
										if (this.phases[i].phase.deliverable[j].files[h].uploaded.video_link) {
											this.phases[i].phase.deliverable[j].files[h].uploaded.video_iframe_link = this.getIframeVideoLink(this.phases[i].phase.deliverable[j].files[h].uploaded.video_link);
										}
										break;
									}
								}
							}
						}
					}
				},
			),
		);
	}
	public getUrl(endpoint_url) {
		// define url
		if (endpoint_url) {
			let url = environment.api_endpoint;
			const suffix = endpoint_url;

			if (suffix.search('http://') === -1 && suffix.search('https://') === -1) {
				const parts = suffix.split('');
				if (parts[0] === '/') {
					parts.shift();
				}

				const imploded = parts.join('');

				url += imploded;
			}
			return url;
		}

	}
	getServiceInfo(): void {
		this.pushSub(
			this.gangService.getSingleEntry(this.gang.id, {}).subscribe(
				result => {
					this.gang = result;
					this.getGangUsers();
				},
			),
		);
	}
	editGang(event): void {
		this.router.navigate(['admin', 'groups', 'edit', this.gang.id]);
	}
	editUser(event): void {
		this.router.navigate(['admin', 'user', 'edit', event.id]);
	}
	addUser(): void {
		this.router.navigate(['admin', 'user', 'new'], { queryParams: { gang_id: this.gang.id } });
	}

	deleteUser(event): void {
		this.pushSub(
			this.userService.deleteEntry(event.id).subscribe(
				(result) => {
					if (result !== false) {
						this.flashService.push('success', 'Integrante removido com sucesso!');
						this.getGangUsers();
					}
				},
			),
		);
	}

	getGangUsers(): void {
		this.pushSub(
			this.gangService.getGangUsers(this.gang.id, {}).subscribe(
				result => {
					this.users = result;
				},
			),
		);
	}
}
