import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';

import { GangsComponent } from './gangs.component';
import { NewGangComponent } from './new/new-gang.component';
import {ViewGangComponent} from "./view/view-gang.component";


const routes: Routes = [
    {
        path: '',
        component: GangsComponent,
    },
    {
      path: 'view/:id',
      component: ViewGangComponent,
    },
    {
        path: 'edit/:id',
        component: NewGangComponent,
    },
    {
        path: 'new',
        component: NewGangComponent,
    },
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule],
})
export class GangsRoutingModule {

}
