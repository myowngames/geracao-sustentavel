import { NgModule } from '@angular/core';
import { NgxEchartsModule } from 'ngx-echarts';

import { ThemeModule } from '../../@theme/theme.module';
import { DirectivesModule } from '../../@core/directives/directives.module';
import { GangsComponent } from './gangs.component';
import { NewGangComponent } from './new/new-gang.component';

import { GangsRoutingModule} from './gangs-routing.module';

import { Ng2SmartTableModule } from 'ng2-smart-table';
import {ViewGangComponent} from "./view/view-gang.component";
@NgModule({
    imports: [
        ThemeModule,
        NgxEchartsModule,
        Ng2SmartTableModule,
        GangsRoutingModule,
        DirectivesModule
    ],
    declarations: [
        GangsComponent,
        NewGangComponent,
        ViewGangComponent
    ],
})
export class GangsModule { }
