import { Component } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

import { SmartTableRemoteDataSource } from '../../@core/data/smart-table_remote-data-source';

import { SubscriptionComponent } from '../../@core/abstract_components/subscription.component';
import { GangService } from '../../@core/data/gang.service';
import { FlashService } from '../../@core/utils/flash.service';
import { DateRenderComponent } from '../../@theme/components/smart-table-render';
import { environment } from '../../../environments/environment';

@Component({
	selector: 'ngx-dashboard',
	styleUrls: ['./gangs.component.scss'],
	templateUrl: './gangs.component.html',
})
export class GangsComponent extends SubscriptionComponent {
	settings = {
		editable: false,
		noDataMessage: 'Nenhuma Informação Encontrada',
		mode: 'external',
		actions: {
			columnTitle: 'Ações',
		},
		pager: {
			perPage: 20,
			display: true,
		},
		add: {
			addButtonContent: '<i class="nb-plus"></i>',
			createButtonContent: '<i class="nb-checkmark"></i>',
			cancelButtonContent: '<i class="nb-close"></i>',
		},
		edit: {
			editButtonContent: '<i class="fa fa-edit"></i>',
			saveButtonContent: '<i class="nb-checkmark"></i>',
			cancelButtonContent: '<i class="nb-close"></i>',
		},
		delete: {
			deleteButtonContent: '<i class="nb-trash"></i>',
			confirmDelete: true,
		},
		columns: {
			name: {
				title: 'Nome',
				type: 'string',
			},
			city__name: {
				title: 'Cidade',
				type: 'html',
				valuePrepareFunction: function (cell, row) {
					return row.city.name + ' - ' + row.city.state;
				},

			},
			school: {
				title: 'Escola',
				type: 'string',
			},
			is_focal_point: {
				title: 'Ponto Focal?',
				type: 'html',
				valuePrepareFunction: function (cell, row) {
					if (cell) {
						return 'Sim';
					}else {
						return 'Não';
					}
				},

			},
			created: {
				filter: false,
				title: 'Criado',
				type: 'custom',
				renderComponent: DateRenderComponent,
			},
		},
	};
	public downloadReportLink = environment.api_endpoint + 'report/generate/';
	source: SmartTableRemoteDataSource = new SmartTableRemoteDataSource(this.gangService);
	constructor(
		private gangService: GangService,
		private router: Router,
		private activeRoute: ActivatedRoute,
		private flashService: FlashService,
	) {
		super();
	}

	onUserRowSelect(event): void {
		this.router.navigate(['admin', 'groups', 'view', event.data.id]);
	}

	createNewEntryButtonClickListener(): void {
		this.router.navigate(['admin', 'groups', 'new']);
	}

	editEntryButtonClickListener(event): void {
		this.router.navigate(['admin', 'groups', 'edit', event.data.id]);
	}

	deleteEntryButtonListener(event): void {
		this.pushSub(
			this.gangService.deleteEntry(event.data.id).subscribe(
				(result) => {
					if (result !== false) {
						this.source.remove(event.data);
						this.flashService.push('success', 'Grupo removido com sucesso!');
					}
				},
			),
		);
	}
}
