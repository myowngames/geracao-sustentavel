import { Component, OnDestroy } from '@angular/core';
import { CanActivate, Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, Validators, FormControl, FormArray } from '@angular/forms';

import { SubscriptionComponent } from '../../../@core/abstract_components/subscription.component';
import { FlashService } from '../../../@core/utils/flash.service';
import { GangService } from '../../../@core/data/gang.service';
import { CityService } from '../../../@core/data/city.service';
import { UserService } from '../../../@core/data/user.service';
import { GangPhaseService } from '../../../@core/data/gang-phase.service';

@Component({
	selector: 'new-gang',
	styleUrls: ['./new-gang.component.scss'],
	templateUrl: './new-gang.component.html',
})
export class NewGangComponent extends SubscriptionComponent {
	public form;
	public user;
	public gang: any = {};
	public cities: any = [];
	public users: any = [];
	public objectKeys = Object.keys;
	public submitted: boolean = false;
	public currentUser: any = [];
	public currentGroup: number = 0;

	constructor(
		private gangService: GangService,
		private userService: UserService,
		private cityService: CityService,
		private flashService: FlashService,
		private gangPhaseService: GangPhaseService,
		private fb: FormBuilder,
		private router: Router,
		private activatedRoute: ActivatedRoute,
	) {
		super();
		this.gang.id = this.activatedRoute.params['value'].id;
	}
	ngOnInit() {
		this.form = this.fb.group({
			id: [
				this.gang.id,
				Validators.compose([]),
			],
			name: [
				this.gang.name,
				Validators.compose([Validators.required, Validators.minLength(2)]),
			],
			school: [
				this.gang.school,
				Validators.compose([Validators.required, Validators.minLength(2)]),
			],
			city: [
				this.gang.city,
				Validators.compose([Validators.required]),
			],
			is_focal_point: [
				this.gang.is_focal_point,
				Validators.compose([]),
			],
			created: [
				this.gang.created,
				Validators.compose([]),
			],
			updated: [
				this.gang.updated,
				Validators.compose([]),
			],
		});
		this.getCurrentUser().then(
			(result) => {
				if (this.currentGroup === 1 || this.currentGroup === 3 || this.currentUser.is_staff
					|| (this.currentUser.gang === this.gang.id && this.currentUser.gang_leader)) {

				} else {
					this.flashService.push('error', 'Você não tem permição para acessar essa pagina');
					this.router.navigate(['admin', 'dashboard']);
				}
				if (this.gang.id) {
					this.getServiceInfor();
				}
				this.getCities();
				this.getUsers();
			},


		);

	}
	getCurrentUser(): Promise<any> {
		return new Promise((resolve, reject) => {
			this.userService.getCurrentUser().subscribe(
				(result) => {
					this.currentUser = result;
					this.currentGroup = this.currentUser.groups[0];
					resolve();
				},
			);
		});
	}
	getCities(): void {
		this.pushSub(
			this.cityService.getAllEntries().subscribe(
				result => {
					this.cities = result;
				},
			),
		);
	}
	getUsers(): void {
		this.pushSub(
			this.userService.getAllEntries().subscribe(
				result => {
					this.users = result;
				},
			),
		);
	}

	getServiceInfor(): void {
		this.pushSub(
			this.gangService.getSingleEntry(this.gang.id, {}).subscribe(
				result => {
					this.gang = result;
					this.gang.city = this.gang.city.id;
					this.initFormValues();
				},
			),
		);
	}


	initFormValues(): void {
		this.form.patchValue(this.gang);
	}
	getCityById(id): any {
		for (let i = 0; i < this.cities.length; i++) {
			if (this.cities[i].id === id) {
				return this.cities[i];
			}
		}
		return [];
	}
	onSubmit() {
		this.submitted = true;
		const data = this.form.value;

		if (this.gang.id != null && typeof this.gang.id !== 'undefined') {
			this.pushSub(
				this.gangService.updateEntry(this.gang.id, data).subscribe(
					(result) => {
						this.flashService.push('success', 'Grupo atualizado com sucesso!');
						this.gang = result;
						this.router.navigate(['admin', 'groups']);
						this.submitted = false;
					},
					(error) => {
						this.submitted = false;
					},
				),
			);
		} else {
			this.pushSub(
				this.gangService.createEntry(data).subscribe(
					(result) => {
						this.flashService.push('success', 'Grupo criado com sucesso!');
						this.gang = result;
						this.router.navigate(['admin', 'groups']);
						this.submitted = false;
					},
					(error) => {
						this.submitted = false;
					},
				),
			);

		}

	}

}
