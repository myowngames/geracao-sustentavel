import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';

import { PagesComponent } from './pages.component';
import { DashboardComponent } from './dashboard/dashboard.component';


const routes: Routes = [
    {
      path: '',
        component: PagesComponent,
        children: [
            {
                path: 'dashboard',
                component: DashboardComponent,
            },
            {
                path: 'cities',
                loadChildren: './cities/cities.module#CitiesModule'
            },
            {
                path: 'groups',
                loadChildren: './gangs/gangs.module#GangsModule'
            },
            {
                path: 'group',
                loadChildren: './group/group.module#GroupModule'
            },
            {
                path: 'user',
                loadChildren: './user/user.module#UserModule'
            },
            {
                path: 'notifications',
                loadChildren: './notifications/notifications.module#NotificationsModule'
            },
            {
                path: 'phases',
                loadChildren: './phases/phases.module#PhasesModule'
            },
            {
                path: 'phaseview',
                loadChildren: './phaseview/phaseview.module#PhaseViewModule'
            },
            {
                path: 'videos',
                loadChildren: './videos/videos.module#VideosModule'
            },
            {
                path: 'frontend-files',
                loadChildren: './frontend-files/frontend-files.module#FrontendFilesModule'
            }
            //{
            //    path: '',
            //    redirectTo: 'dashboard',
            //    pathMatch: 'full',
            //},
        ],
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule],
})
export class PagesRoutingModule {
}
