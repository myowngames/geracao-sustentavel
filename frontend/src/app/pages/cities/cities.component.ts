import { Component, OnDestroy, OnInit } from '@angular/core';
import { CanActivate, Router, ActivatedRoute  } from '@angular/router';

import { LocalDataSource } from 'ng2-smart-table';
import { SmartTableRemoteDataSource } from '../../@core/data/smart-table_remote-data-source';

import { SubscriptionComponent } from '../../@core/abstract_components/subscription.component';
import { CityService } from '../../@core/data/city.service';
import { FlashService } from '../../@core/utils/flash.service';

@Component({
  selector: 'ngx-dashboard',
  styleUrls: ['./cities.component.scss'],
  templateUrl: './cities.component.html',
})
export class CitiesComponent extends SubscriptionComponent implements OnDestroy, OnInit {
	settings = {
		editable: false,
		mode: 'external',
		actions: {
			columnTitle : 'Ações'
		},
		noDataMessage: 'Nenhuma Informação Encontrada',
		pager: {
			perPage: 20,
			display: true,
		},
		add: {
			addButtonContent: '<i class="nb-plus"></i>',
			createButtonContent: '<i class="nb-checkmark"></i>',
			cancelButtonContent: '<i class="nb-close"></i>',
		},
		edit: {
			editButtonContent: '<i class="fa fa-edit"></i>',
			saveButtonContent: '<i class="nb-checkmark"></i>',
			cancelButtonContent: '<i class="nb-close"></i>',
		},
		delete: {
			deleteButtonContent: '<i class="nb-trash"></i>',
			confirmDelete: true,
		},
		columns: {
			name: {
				title: 'Nome',
				type: 'string',
			},
			state: {
				title: 'Estado',
				type: 'string',
			},
		},
	};

	source: SmartTableRemoteDataSource = new SmartTableRemoteDataSource(this.cityService);
	constructor(
		private cityService: CityService,
		private router: Router,
		private activeRoute: ActivatedRoute,
		private flashService: FlashService
   	){
		super();
    }
	ngOnDestroy(){

	}
	ngOnInit(){
	
	}
	createNewEntryButtonClickListener(): void{
    	this.router.navigate(['admin', 'cities', 'new']);
    }

    editEntryButtonClickListener(event): void{
    	this.router.navigate(['admin', 'cities', 'edit', event.data.id]);
    }
	
    deleteEntryButtonListener(event): void{
    	this.cityService.deleteEntry(event.data.id).subscribe(
    		(result) => {
    			if (result != false){
    				this.source.remove(event.data);
    				this.flashService.push("success", "Cidade removida com sucesso!");
    			}
    		}
   		);
    }
}
