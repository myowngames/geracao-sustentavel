import { Component, OnDestroy } from '@angular/core';
import { CanActivate, Router, ActivatedRoute  } from '@angular/router';
import { FormBuilder, FormGroup, Validators, FormControl, FormArray } from '@angular/forms';

import { SubscriptionComponent } from '../../../@core/abstract_components/subscription.component';
import { FlashService } from '../../../@core/utils/flash.service';
import { CityService } from '../../../@core/data/city.service';

@Component({
	selector: 'new-city',
	styleUrls: ['./new-city.component.scss'],
	templateUrl: './new-city.component.html',
})
export class NewCityComponent extends SubscriptionComponent implements OnDestroy {


	public form;
    public user;
	public city: any = {};
	public states: any = {};
  
    public objectKeys = Object.keys;
    public submitted: boolean = false;
    constructor(
    	private cityService: CityService,
        private flashService: FlashService,
    	private fb: FormBuilder,
    	private router: Router,
    	private activatedRoute: ActivatedRoute,
    ){
        super();
    	this.city.id = this.activatedRoute.params['value'].id;
        this.states  = cityService.states;
    }
    ngOnDestroy(){
    
    }
    ngOnInit(){
    	this.form = this.fb.group({
            id: [
                this.city.id,
                Validators.compose([])
            ],
            name: [
                this.city.name,
                Validators.compose ([Validators.required, Validators.minLength(2)])
            ],
            state: [
                this.city.state,
                Validators.compose ([Validators.required, Validators.minLength(2)])
            ],
            created: [
                this.city.created,
                Validators.compose([])
            ],
            updated: [
                this.city.updated,
                Validators.compose([])
            ],
        });

        if(this.city.id){
            this.getServiceInfor();
        }
    }

    getServiceInfor(): void{
        this.pushSub(
            this.cityService.getSingleEntry(this.city.id, {}).subscribe(
                result => {
                    this.city = result;
                    this.initFormValues();
                }
            )
        );
    }


    initFormValues(): void{
    	this.form.patchValue(this.city);
    }

    onSubmit(){
        this.submitted = true;
        var data = this.form.value;
        if (this.city.id != null && typeof this.city.id != 'undefined'){
            this.pushSub(
                this.cityService.updateEntry(this.city.id, data).subscribe(
                    (result) => {
                        this.flashService.push('success', 'Cidade atualizada com sucesso!');
                        this.city = result;
                        this.router.navigate(['admin', 'cities']);
                        this.submitted = false;
                    },
                    (error) => {
                        this.submitted = false;
                    }
                )
            );
        }
        else{
            this.pushSub(
                this.cityService.createEntry(data).subscribe(
                    (result) => {
                        this.flashService.push('success', 'Cidade criada com sucesso!');
                        this.city = result;
                        this.router.navigate(['admin', 'cities']);
                        this.submitted = false;
                    },
                    (error) => {
                        this.submitted = false;
                    }
                )
            )

        }

    }

}
