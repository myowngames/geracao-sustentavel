import { NgModule } from '@angular/core';
import { NgxEchartsModule } from 'ngx-echarts';

import { Ng2SmartTableModule } from 'ng2-smart-table';
import { ThemeModule } from '../../@theme/theme.module';
import { DirectivesModule } from '../../@core/directives/directives.module';

import { CitiesRoutingModule } from './cities-routing.module';
import { CitiesComponent } from './cities.component';
import { NewCityComponent } from './new/new-city.component';

@NgModule({
    imports: [
        ThemeModule,
        NgxEchartsModule,
        CitiesRoutingModule,
        Ng2SmartTableModule,
        DirectivesModule
    ],
    declarations: [
        CitiesComponent,
        NewCityComponent
    ],
})
export class CitiesModule { }