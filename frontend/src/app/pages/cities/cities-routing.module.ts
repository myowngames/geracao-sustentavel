import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';

import { CitiesComponent } from './cities.component';
import { NewCityComponent } from './new/new-city.component';

const routes: Routes = [
    {
        path: '',
        component: CitiesComponent,
    },
    {
        path: 'edit/:id',
        component: NewCityComponent,
    },
    {
        path: 'new',
        component: NewCityComponent,
    },
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule],
})
export class CitiesRoutingModule {

}
