import { NgModule } from '@angular/core';
import { NgxEchartsModule } from 'ngx-echarts';

import { ThemeModule } from '../../@theme/theme.module';
import { UserComponent } from './user.component';
import { UserEditComponent } from './edit/user-edit.component';
import { UserRoutingModule} from './user-routing.module';

import { Ng2SmartTableModule } from 'ng2-smart-table';

@NgModule({
    imports: [
        ThemeModule,
        NgxEchartsModule,
        Ng2SmartTableModule,
        UserRoutingModule
    ],
    declarations: [
        UserComponent,
        UserEditComponent
    ],
})
export class UserModule { }