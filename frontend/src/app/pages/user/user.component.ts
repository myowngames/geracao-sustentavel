import { Component, OnDestroy, OnInit } from '@angular/core';
import { CanActivate, Router, ActivatedRoute } from '@angular/router';

import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { LocalDataSource } from 'ng2-smart-table';
import { SmartTableRemoteDataSource } from '../../@core/data/smart-table_remote-data-source';

import { SubscriptionComponent } from '../../@core/abstract_components/subscription.component';
import { UserService } from '../../@core/data/user.service';
import { FlashService } from '../../@core/utils/flash.service';
import { DateRenderComponent } from '../../@theme/components/smart-table-render';
import { environment } from '../../../environments/environment';

@Component({
	selector: 'ngx-user',
	styleUrls: ['./user.component.scss'],
	templateUrl: './user.component.html',
})

export class UserComponent extends SubscriptionComponent implements OnInit {
	settings = {
		noDataMessage: 'Nenhuma Informação Encontrada',
		editable: false,
		mode: 'external',
		actions: {
			columnTitle: 'Ações',
		},
		pager: {
			perPage: 20,
			display: true,
		},
		add: {
			addButtonContent: '<i class="nb-plus"></i>',
			createButtonContent: '<i class="nb-checkmark"></i>',
			cancelButtonContent: '<i class="nb-close"></i>',
		},
		edit: {
			editButtonContent: '<i class="fa fa-edit"></i>',
			saveButtonContent: '<i class="nb-checkmark"></i>',
			cancelButtonContent: '<i class="nb-close"></i>',
		},
		delete: {
			deleteButtonContent: '<i class="nb-trash"></i>',
			confirmDelete: true,
		},
		columns: {
			id: {
				title: 'ID',
				type: 'number',
			},
			name: {
				title: 'Nome',
				type: 'string',
			},
			username: {
				title: 'Username',
				type: 'string',
			},
			email: {
				title: 'Email',
				type: 'string',
			},
			created: {
				filter: false,
				title: 'Criado',
				type: 'custom',
				renderComponent: DateRenderComponent,
			},
		},
	};
	public downloadReportLink = environment.api_endpoint + 'report/user_list/';
	source: SmartTableRemoteDataSource = new SmartTableRemoteDataSource(this.userService);
	private currentUser;
	private currentGroup;

	constructor(
		private userService: UserService,
		private router: Router,
		private activeRoute: ActivatedRoute,
		private flashService: FlashService,
	) {
		super();
	}
	ngOnInit() {
		this.getCurrentUser();
	}
	getCurrentUser(): void {
		this.userService.getCurrentUser().subscribe(
			(result) => {
				this.currentUser = result;
				this.currentGroup = this.currentUser.groups[0];
				if (this.currentUser.is_staff || this.currentGroup === 3 || this.currentGroup === 1 ){

				} else {
					this.router.navigate(['admin', 'dashboard']);
				}
			},
		);
	}

	createNewEntryButtonClickListener(): void {
		this.router.navigate(['admin', 'user', 'new']);
	}

	editEntryButtonClickListener(event): void {
		this.router.navigate(['admin', 'user', 'edit', event.data.id]);
	}

	deleteEntryButtonListener(event): void {
		this.userService.deleteEntry(event.data.id).subscribe(
			(result) => {
				if (result !== false) {
					this.source.remove(event.data);
					this.flashService.push('success', 'Grupo removido com sucesso!');
				}
			},
		);
	}
}
