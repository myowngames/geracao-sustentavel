import { Component, OnInit } from '@angular/core';
import { CanActivate, Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, Validators, FormControl, FormArray } from '@angular/forms';

import { SubscriptionComponent } from '../../../@core/abstract_components/subscription.component';
import { GroupService } from '../../../@core/data/group.service';
import { GangService } from '../../../@core/data/gang.service';
import { UserService } from '../../../@core/data/user.service';
import { FlashService } from '../../../@core/utils/flash.service';
import { CityService } from '../../../@core/data/city.service';

import { NgbDatepickerConfig, NgbDateParserFormatter } from '@ng-bootstrap/ng-bootstrap';
import { NgbDateBRParserFormatter } from '../../../@core/utils/ngb-date-br-parser-formatter';


import * as moment from 'moment';


@Component({
	selector: 'user-edit',
	styleUrls: ['./user-edit.component.scss'],
	templateUrl: './user-edit.component.html',
	providers: [{ provide: NgbDateParserFormatter, useClass: NgbDateBRParserFormatter }],
})

export class UserEditComponent extends SubscriptionComponent implements OnInit {
	public form;
	public currentUser: any = {};
	public currentGroup: number = 0;
	public user: any = {
		is_staff: false,
		is_active: true,
		gang_leader: false,
	};
	public cities = [];
	public allGroups = [];
	public allGangs = [];
	public submitted = false;
	private existing_group = null;

	public userTermGotSelected = false;

	constructor(private userService: UserService,
		private flashService: FlashService,
		private cityService: CityService,
		private groupService: GroupService,
		private gangService: GangService,
		private fb: FormBuilder,
		private router: Router,
		private activatedRoute: ActivatedRoute) {
		super();
		this.user.id = this.activatedRoute.params['value'].id;
		if (this.user.id === '0') {
			this.user = {
				is_staff: false,
				is_active: true,
				gang_leader: false,
			};
		}
		if (this.activatedRoute.queryParams['value'].gang_id) {
			this.existing_group = this.activatedRoute.queryParams['value'].gang_id;
			this.user.gang = this.existing_group;
		}
	}
	ngOnInit() {
		this.form = this.fb.group({
			id: [
				this.user.id,
				Validators.compose([]),
			],
			groups: this.fb.array([]),
			name: [
				this.user.name,
				Validators.compose([Validators.required, Validators.minLength(2)]),
			],
			username: [
				this.user.username,
				Validators.compose([Validators.required, Validators.minLength(4)]),
			],
			email: [
				this.user.email,
				Validators.compose([]),
			],
			is_staff: [
				this.user.is_staff,
				Validators.compose([]),
			],
			is_active: [
				this.user.is_active,
				Validators.compose([]),
			],
			password: [
				this.user.password,
				Validators.compose([]),
			],
			rg: [
				this.user.rg,
				Validators.compose([]),
			],
			cpf: [
				this.user.cpf,
				Validators.compose([]),
			],
			phone: [
				this.user.phone,
				Validators.compose([]),
			],
			birthday: [
				this.user.birthday,
				Validators.compose([]),
			],
			city: [
				this.user.city,
				Validators.compose([]),
			],
			gang: [
				this.user.gang,
				Validators.compose([]),
			],
			gang_leader: [
				this.user.gang_leader,
				Validators.compose([]),
			],
			term: [
				this.user.term,
				Validators.compose([])
			]
		});
		this.getCurrentUser().then(
			(result) => {
				this.getEditableUser();
			},
		);
		this.getAllGangs();
		this.getCidades();
	}
	getCidades(): void {
		this.pushSub(
			this.cityService.getAllEntries().subscribe(
				(response) => {
					this.cities = response;
				},
			),
		);
	}
	getCurrentUser(): Promise<any> {
		return new Promise((resolve, reject) => {
			this.userService.getCurrentUser().subscribe(
				(result) => {
					this.currentUser = result;
					this.currentGroup = this.currentUser.groups[0];
					resolve();
				},
			);
		});
	}
	getEditableUser() {
		if (typeof this.user.id !== 'undefined') {
			this.pushSub(
				this.userService.getSingleEntry(this.user.id).subscribe(
					(result) => {
						this.user = result;

						const birthday = moment(this.user.birthday, 'YYYY-MM-DD');
						if (this.user.birthday != null) {
							this.user.birthday = {
								'year': birthday.get('year'),
								'month': birthday.get('month') + 1,
								'day': birthday.get('date'),
							};
						}

						if (this.currentUser.is_staff || this.currentGroup === 3 || this.currentGroup === 1
							|| (this.user.gang === this.currentUser.gang && this.currentUser.gang_leader)) {
							this.user.password = null;
							this.form.patchValue(this.user);
							this.getAllGroups();
						} else {
							this.flashService.push('error', 'Você não tem permição para acessar essa pagina');
							this.router.navigate(['admin', 'dashboard']);
						}
					},
				),
			);
		} else {
			this.getAllGroups();
		}
	}
	getAllGroups() {
		this.pushSub(
			this.groupService.getAllEntries().subscribe(
				(result) => {
					this.allGroups = result;
					this.parseUserGroupsToForm();
				},
			),
		);
	}

	getAllGangs() {
		this.pushSub(
			this.gangService.getAllEntries().subscribe(
				(result) => {
					this.allGangs = result;
				},
			),
		);
	}

	parseUserGroupsToForm() {
		for (let i = 0; i < this.allGroups.length; i++) {
			let userIsIn = false;
			if (this.user.groups && this.user.groups.length > 0) {
				if (this.user.groups.indexOf(this.allGroups[i].id) !== -1) {
					userIsIn = true;
				}
			}
			this.form.get('groups').push(new FormControl(userIsIn));
		}
	}

	setUserTermFile($event){
		this.userTermGotSelected = true;
		this.form.get('term').patchValue($event.target.files[0]);
	}

	saveUserTermFile(){
		return new Promise((resolve, reject) => {
			if (this.userTermGotSelected == true){				
				let data = {
					term: this.form.get('term').value
				}
				this.pushSub(
					this.userService.UpdateEntryWithFile(this.user.id, data).subscribe(
						(result) => {
							this.userTermGotSelected = false;
							resolve(true);
						}
					)
				);
			}
			else{
				resolve(true);
			}
		});
	}

	removeUserTerm(){
		let data = {
			term: null,
		}

		this.pushSub(
			this.userService.updateEntry(this.user.id, data).subscribe(
				(result) => {
					this.flashService.push('success', 'Termo removido com sucesso');
					this.form.get('term').patchValue(null);
					this.user.term = null;
				},
				(err) => {
					this.flashService.push('error' , 'Erro removendo termo do usuário');
				}
			)
		);
	}

	onSubmit() {
		this.submitted = true;
		const data = this.form.value;
		const groupChecks = data.groups;
		data.groups = [];
		for (let i = 0; i < this.allGroups.length; i++) {
			if (groupChecks[i]) {
				data.groups.push(this.allGroups[i].id);
			}
		}

		//we deal with the term after the user is created
		delete data.term;

		if (!data.password) {
			delete data.password;
		}
		if (!data.gang || data.gang === 'null') {
			data.gang = null;
		}
		if (!data.city || data.city === 'null') {
			data.city = null;
		}
		if (data.birthday != null) {
			if (data.birthday.year) {
				data.birthday = moment().set({
					'year': data.birthday.year,
					'month': data.birthday.month - 1,
					'date': data.birthday.day,
				})
					.format('YYYY-MM-DD');
			}
		}

		if (this.user.id != null && typeof this.user.id !== 'undefined') {
			this.pushSub(
				this.userService.updateEntry(this.user.id, data).subscribe(
					(result) => {
						this.flashService.push('success', 'Usuario atualizado com sucesso!');
						this.user = result;

						this.saveUserTermFile().then(function(val){
							this.submitted = false;	
							if (this.existing_group) {
								this.router.navigate(['admin', 'groups', 'view', this.existing_group]);
							} 
							else {
								if (this.currentUser.is_staff || this.currentGroup === 3 || this.currentGroup === 1) {
									this.router.navigate(['admin', 'user']);
								} else {
									this.router.navigate(['admin', 'groups', 'view', 'meugrupo']);
								}
							}
						}.bind(this))						
					},
					(error) => {
						this.submitted = false;
					},
				),
			);
		} else {
			this.pushSub(
				this.userService.createEntry(data).subscribe(
					(result) => {
						this.flashService.push('success', 'Usuario atualizado com sucesso!');
						this.user = result;

						this.saveUserTermFile().then(function(val){
							if (this.existing_group) {
								this.router.navigate(['admin', 'groups', 'view', this.existing_group]);
							} 
							else {
								if (this.currentUser.is_staff || this.currentGroup === 3 || this.currentGroup === 1) {
									this.router.navigate(['admin', 'user']);
								} else {
									this.router.navigate(['admin', 'groups', 'view', 'meugrupo']);
								}
							}

							this.submitted = false;
						}.bind(this))						
					},
					(error) => {
						this.submitted = false;
					},
				),
			);

		}
	}

}
