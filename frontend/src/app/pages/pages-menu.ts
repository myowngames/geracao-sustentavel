import { NbMenuItem } from '@nebular/theme';

export const MENU_ITEMS: NbMenuItem[] = [
	{
		title: 'Início',
		icon: 'fa fa-home',
		link: '/admin/dashboard',
		home: true,
		data: {
			allowed_groups: [0, 1, 2, 3, 4],
		},
	},
	{
		title: 'Geral',
		icon: 'fa fa-users',
		link: '/admin/groups/view/meugrupo',
		data: {
			allowed_groups: [2],
		},
	},
	{
		title: 'Notificações',
		icon: 'fa fa-comments',
		link: '/admin/notifications', // Mudei o Geral para Notificações
		data: {
			allowed_groups: [0, 1, 3],
		},
	},
	{
		title: 'Grupos',
		icon: 'fa fa-users',
		link: '/admin/groups',
		data: {
			allowed_groups: [0, 1, 3],
		},
	},
	{
		title: 'Cidades',
		icon: 'fa fa-map-signs',
		link: '/admin/cities',
		data: {
			allowed_groups: [0, 1],
		},
	},
	{
		title: 'Fases',
		icon: 'fa fa-road',
		link: '/admin/phases',
		data: {
			allowed_groups: [0, 1, 3],
		},
	},
	{
		title: 'Fases',
		icon: 'fa fa-road',
		link: '/admin/phaseview',
		data: {
			allowed_groups: [2],
		},
	},
	{
		title: 'Videos',
		icon: 'fa fa-play',
		link: '/admin/videos',
		data: {
			allowed_groups: [0, 1, 3],
		},
	},

	{
		title: 'Arquivos',
		icon: 'fa fa-file',
		link: '/admin/frontend-files',
		data: {
			allowed_groups: [0, 1, 2, 3, 4],
		},
	},

	{
		title: 'Usuários',
		icon: 'fa fa-user',
		link: '/admin/user',
		data: {
			allowed_groups: [0, 1],
		},
	},
	{
		title: 'Tipos de Usuários',
		icon: 'fa fa-users',
		link: '/admin/group',
		data: {
			allowed_groups: [0],
		},
	},
];
