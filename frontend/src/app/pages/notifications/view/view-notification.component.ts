import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, NavigationEnd, NavigationStart} from '@angular/router';

import {SubscriptionComponent} from '../../../@core/abstract_components/subscription.component';
import {FlashService} from '../../../@core/utils/flash.service';
import * as moment from 'moment';
import {NotificationService} from "../../../@core/data/notification.service";

@Component({
  selector: 'ngx-view-notification',
  styleUrls: ['./view-notification.component.scss'],
  templateUrl: './view-notification.component.html',
})
export class ViewNotificationComponent extends SubscriptionComponent implements OnInit {
  public notification: any = {};
  public submitted: boolean = false;
  
  constructor(private notificationService: NotificationService,
              private flashService: FlashService,
              private router: Router,
              private activatedRoute: ActivatedRoute,
              ) {
    super();
    this.notification.id = this.activatedRoute.params['value'].id;
    router.events.subscribe((event) => {
      if (event instanceof NavigationEnd) {
        if (this.activatedRoute.params['value'].id !== this.notification.id) {
          this.notification.id = this.activatedRoute.params['value'].id;
          this.getNotification();
        }
      }
    });
  }
  back() {
    this.router.navigate(['admin', 'dashboard']);
  }
  ngOnInit() {
    if (!this.notification.id) {
      this.flashService.push('error', 'Precisamos saber qual notificacao você quer ver')
      this.router.navigate(['admin', 'dashboard']);
    } else {
      this.getNotification();
    }
  }
  
  printDate(date): string {
    if (date) {
      return moment(date).format('DD/MM/YYYY HH:MM:SS');
    } else {
      return '';
    }
  }
  
  getNotification(): void {
    this.pushSub(
      this.notificationService.getSingleEntry(this.notification.id, {}).subscribe(
        result => {
          this.notification = result;
          this.readNotification()
        }
      )
    )
  }
  
  readNotification(): void {
    
    this.pushSub(
      this.notificationService.updateEntry(this.notification.id, {state: 'read'}).subscribe(
        result => {
          this.notification = result;
        }
      )
    )
  }
}
