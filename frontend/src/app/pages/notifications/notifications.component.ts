import { Component, OnDestroy, OnInit } from '@angular/core';

import { FlashService } from '../../@core/utils/flash.service';
import { SubscriptionComponent } from '../../@core/abstract_components/subscription.component';
import { FormBuilder, FormGroup, Validators, FormControl, FormArray } from '@angular/forms';
import { GangService } from '../../@core/data/gang.service';
import { CityService } from '../../@core/data/city.service';
import { NotificationService } from '../../@core/data/notification.service';
import { NotificationContentService } from '../../@core/data/notification-content.service';
import { SmartTableRemoteDataSource } from '../../@core/data/smart-table_remote-data-source';
import { Router } from '@angular/router';
import '../../@core/utils/ckeditor.loader';
import 'ckeditor';

@Component({
	selector: 'ngx-dashboard',
	styleUrls: ['./notifications.component.scss'],
	templateUrl: './notifications.component.html',
})
export class NotificationsComponent extends SubscriptionComponent implements OnDestroy, OnInit {
	settings = {
		noDataMessage: 'Nenhuma Informação Encontrada',
		editable: false,
		mode: 'external',
		pager: {
			perPage: 20,
			display: true,
		},
		actions: false,
		add: '',
		edit: {
			editButtonContent: '<i class="fa fa-edit"></i>',
			saveButtonContent: '<i class="nb-checkmark"></i>',
			cancelButtonContent: '<i class="nb-close"></i>',
		},
		delete: false,
		columns: {
			subject: {
				title: 'Título de mensagem',
				type: 'string',
			},
			created: {
				title: 'Data do envio',
				type: 'string',
			},
		},
	};
	private notification_type_index = {
		everyone: 'everyone',
		all: 'all',
		one: 'one',
		city: 'city',
		admin: 'admin',
	};
	public notification_type = [
		{ index: this.notification_type_index.everyone, value: 'Todos os grupos (participantes + ponto focal)' },
		{ index: this.notification_type_index.all, value: 'Todos os grupos (participantes)'},
		{ index: this.notification_type_index.one, value: 'Um grupo específico'},
		{ index: this.notification_type_index.city, value: 'Todos os grupos de cidades específicas'},
		{ index: this.notification_type_index.admin, value: 'Todos os grupos focais (administradores)'},
	];
	public selected_notification_type: string;

	source: SmartTableRemoteDataSource = new SmartTableRemoteDataSource(this.notificationContentService);

	public form: FormGroup;
	public notification: any = {
		cities: null,
		type: 'system',
		email_notification: false,
		internal_notification: true,
	};
	public submitted: boolean = false;
	public allGangs: any = [];
	public allCities: any = [];
	public allSelected: boolean = false;
	public gangCount: number = 0;
	constructor(private flashService: FlashService,
		private fb: FormBuilder,
		private gangService: GangService,
		private cityService: CityService,
		private router: Router,
		private notificationService: NotificationService,
		private notificationContentService: NotificationContentService) {
		super();
	}

	ngOnDestroy() {

	}

	ngOnInit() {
		this.form = this.fb.group({
			type: [
				this.notification.type,
				Validators.compose([Validators.required]),
			],
			cities: this.fb.array([]),
			select_gang: [
				this.notification.select_gang,
				Validators.compose([]),
			],
			selected_notification_type: [
				this.selected_notification_type,
				Validators.compose([Validators.required]),
			],
			content_subject: [
				this.notification.subject,
				Validators.compose([Validators.required]),
			],
			content: [
				this.notification.content,
				Validators.compose([Validators.required]),
			],
		});
		this.getAllGangs();
		this.getAllCities();
		this.pushSub(
			this.form.get('selected_notification_type').valueChanges.subscribe(
				(value) => {
					this.selected_notification_type = value;
					this.getGangNotificationCount();
				},
			),
		);
		this.pushSub(
			this.form.get('select_gang').valueChanges.subscribe(
				(value) => {
					this.getGangNotificationCount();
				},
			),
		);
		this.pushSub(
			this.form.get('cities').valueChanges.subscribe(
				(value) => {
					this.getGangNotificationCount();
				},
			),
		);
	}

	getGangNotificationCount() {
		if (this.selected_notification_type && this.selected_notification_type !== 'null') {
			const params: any = { };
			if (this.selected_notification_type === this.notification_type_index.admin) {
				params.is_focal_point = true;
			}else if ( this.selected_notification_type === this.notification_type_index.city) {
				const cities = this.form.get('cities').value;
				let gang_list: any = [];
				for (const k in cities) {
					if (cities.hasOwnProperty(k)) {
						if (cities[k]) {
							gang_list = this.getGangBasedOnGroup(k, gang_list, false);
						}
					}
				}
				this.gangCount = gang_list.length;
				return;
			}else if ( this.selected_notification_type === this.notification_type_index.one) {
				if (this.form.get('select_gang').value && this.form.get('select_gang').value !== 'null' ) {
					this.gangCount = 1;
				} else {
					this.gangCount = 0;
				}
				return;
			}else if (this.selected_notification_type === this.notification_type_index.all ) {
				params.is_focal_point = false;
			}else {
				this.gangCount = this.allGangs.length;
			}
			this.pushSub(
				this.gangService.getCount({params: params }).subscribe(
					(result) => {
						this.gangCount = result.count;
					},
				),
			);
		} else {
			this.gangCount = 0;
		}
	}

	getAllGangs() {
		this.pushSub(
			this.gangService.getAllEntries().subscribe(
				(result) => {
					this.allGangs = result;
				},
			),
		);
	}

	getAllCities() {
		this.pushSub(
			this.cityService.getAllEntries().subscribe(
				(result) => {
					const cities = this.form.get('cities') as FormArray;
					for (let i = 0; i < result.length; ++i) {
						cities.insert(i, new FormControl());
					}
					this.allCities = result;
				},
			),
		);
	}

	selectAllCities() {
		const cities = this.form.get('cities') as FormArray;
		this.allSelected = !this.allSelected;
		for (let i = 0; i < cities.controls.length; i++) {
			cities.controls[i].patchValue(this.allSelected);
		}
	}

	cleanForm() {
		this.form.reset();
	}
	getGangsWithFilter(is_focal_point: boolean = null): object {
		const gangs = [];
		for (let i = this.allGangs.length - 1; i >= 0; i--) {
			if (is_focal_point !== null) {
				if (this.allGangs[i].is_focal_point === is_focal_point) {
					gangs.push({ id: this.allGangs[i].id });
				}
			}else {
				gangs.push({ id: this.allGangs[i].id });
			}
		}
		return gangs;
	}
	getGangBasedOnGroup(index, gangs, is_focal_point: boolean = null): object {
		for (let i = this.allGangs.length - 1; i >= 0; i--) {
			if (this.allGangs[i].city) {
				if (this.allGangs[i].city.id === this.allCities[index].id) {
					if (is_focal_point !== null) {
						if (this.allGangs[i].is_focal_point === is_focal_point) {
							gangs.push({ id: this.allGangs[i].id });
						}
					}else {
						gangs.push({ id: this.allGangs[i].id });
					}
				}
			}
		}
		return gangs;
	}

	onSubmit() {
		const data = this.form.value;
		data.content = {
			content: data.content,
			subject: data.content_subject,
		};
		// Set Gangs
		if (data.type === 'email') {
			data.email_notification = true;
		}
		data.gang_list = [];
		if (this.selected_notification_type === this.notification_type_index.admin) {
			data.gang_list = this.getGangsWithFilter(true);
		}else if ( this.selected_notification_type === this.notification_type_index.city) {
			for (const k in data.cities) {
				if (data.cities.hasOwnProperty(k)) {
					if (data.cities[k]) {
						data.gang_list = this.getGangBasedOnGroup(k, data.gang_list, false);
					}
				}
			}
		}else if ( this.selected_notification_type === this.notification_type_index.one) {
			const gang_id = data.select_gang;
			if (gang_id && gang_id !== 'null') {
				data.gang_list.push({ id: gang_id });
			}
		}else if (this.selected_notification_type === this.notification_type_index.all) {
			// All Groups
			data.gang_list = this.getGangsWithFilter(false);
		}else {
			data.gang_list = this.allGangs;
		}
		if (data.gang_list.length <= 0) {
			this.flashService.push('warning', 'Por favor, selecione uma cidade ou groupo');
			return;
		}
		this.submitted = true;
		this.pushSub(
			this.notificationService.createEntry(data).subscribe(
				(result) => {
					this.flashService.push('success', 'Notificação criada e enviada com sucesso');
					this.submitted = false;
					this.source.refresh();
					this.cleanForm();
				},
				(error) => {
					this.submitted = false;
				},
			),
		);
	}
	onUserRowSelect(event): void {
		this.router.navigate(['admin', 'notifications', 'content', event.data.id]);
	}
	editEntryButtonClickListener(event): void {
		// this.router.navigate(['admin', 'notifications', 'view', event.data.id]);
	}

}
