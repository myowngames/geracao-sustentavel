import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';

import { NotificationsComponent } from './notifications.component';
import { ViewNotificationComponent } from './view/view-notification.component';
import { ContentNotificationComponent } from './content/content-notification.component';

const routes: Routes = [
	{
		path: '',
		component: NotificationsComponent,
	},
	{
		path: 'view/:id',
		component: ViewNotificationComponent,
	},
	{
		path: 'content/:id',
		component: ContentNotificationComponent,
	},
	{
		path: 'edit/:id',
		component: NotificationsComponent,
	},
	{
		path: 'new',
		component: NotificationsComponent,
	},
];

@NgModule({
	imports: [RouterModule.forChild(routes)],
	exports: [RouterModule],
})
export class NotificationsRoutingModule {

}
