import { NgModule } from '@angular/core';
import { NgxEchartsModule } from 'ngx-echarts';

import { ThemeModule } from '../../@theme/theme.module';
import { NotificationsComponent } from './notifications.component';
import { NotificationsRoutingModule } from './notifications-routing.module';
import { DirectivesModule } from '../../@core/directives/directives.module';

import { Ng2SmartTableModule } from 'ng2-smart-table';
import {ViewNotificationComponent} from './view/view-notification.component';
import { ContentNotificationComponent } from './content/content-notification.component';
import { CKEditorModule } from 'ng2-ckeditor';
@NgModule({
    imports: [
        ThemeModule,
        NgxEchartsModule,
        NotificationsRoutingModule,
        DirectivesModule,
      Ng2SmartTableModule,
      CKEditorModule
    ],
    declarations: [
        NotificationsComponent,
        ViewNotificationComponent,
        ContentNotificationComponent
    ],
})
export class NotificationsModule { }
