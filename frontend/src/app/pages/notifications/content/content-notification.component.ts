import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

import { SubscriptionComponent } from '../../../@core/abstract_components/subscription.component';
import { FlashService } from '../../../@core/utils/flash.service';
import * as moment from 'moment';
import { NotificationContentService } from '../../../@core/data/notification-content.service';
import { NavigationEnd } from '@angular/router';
@Component({
	selector: 'ngx-content-notification',
	styleUrls: ['./content-notification.component.scss'],
	templateUrl: './content-notification.component.html',
})
export class ContentNotificationComponent extends SubscriptionComponent implements OnInit {
	public content: any = {};
	public submitted: boolean = false;

	constructor(private notificationContentService: NotificationContentService,
		private flashService: FlashService,
		private router: Router,
		private activatedRoute: ActivatedRoute ) {
		super();
		this.content.id = this.activatedRoute.params['value'].id;


	}

	ngOnInit() {
		if (!this.content.id) {
			this.flashService.push('error', 'Precisamos saber qual notificacao você quer ver');
			this.router.navigate(['admin', 'dashboard']);
		} else {
			this.getNotification();
		}
	}
	back() {
		this.router.navigate(['admin', 'notifications']);
	}
	printDate(date): string {
		if (date) {
			return moment(date).format('DD/MM/YYYY HH:MM:SS');
		} else {
			return '';
		}
	}

	getNotification(): void {
		this.pushSub(
			this.notificationContentService.getSingleEntry(this.content.id, {}).subscribe(
				result => {
					this.content = result;
				}
			),
		);
	}
}
