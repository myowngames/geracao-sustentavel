import { Component, OnDestroy, OnInit } from '@angular/core';
import { SubscriptionComponent } from '../../@core/abstract_components/subscription.component';
import { GangService } from '../../@core/data/gang.service';
import { GangPhaseService } from '../../@core/data/gang-phase.service';
import { PhaseService } from '../../@core/data/phase.service';
import * as moment from 'moment';
import { UserService } from '../../@core/data/user.service';
import { SmartTableRemoteDataSource } from '../../@core/data/smart-table_remote-data-source';
import { NotificationContentService } from '../../@core/data/notification-content.service';
import { NotificationService } from '../../@core/data/notification.service';
import { DateRenderComponent } from '../../@theme/components/smart-table-render';
import { Router } from '@angular/router';

@Component({
  selector: 'ngx-dashboard',
  styleUrls: ['./dashboard.component.scss'],
  templateUrl: './dashboard.component.html',
})
export class DashboardComponent extends SubscriptionComponent implements OnDestroy, OnInit {
	public total_groups: number = 0;
	public active_groups: number = 0;
	public phases: any = [];
	public current_phase: any = {};
	public days_remaining: string = '';
	public total_deliverables: number = 0;
	public user: any = {};
	public groupId: number = 0;
	settings = {
		noDataMessage: 'Nenhuma mensagem',
		editable: false,
		mode: 'external',
		pager: {
			perPage: 20,
			display: true,
		},
		actions: false,
		add: '',
		edit: {
			editButtonContent: '<i class="fa fa-edit"></i>',
			saveButtonContent: '<i class="nb-checkmark"></i>',
			cancelButtonContent: '<i class="nb-close"></i>',
		},
		delete: false,
		columns: {
			content: {
			title: 'Título de mensagem',
			type: 'html',
			valuePrepareFunction: function(cell, row) {
				return cell.subject;
			},
			},
			created: {
			title: 'Data do envio',
			type: 'custom',
			renderComponent: DateRenderComponent,
			},
		},
	};
	source: SmartTableRemoteDataSource = new SmartTableRemoteDataSource(
		this.notificationService,
	);

	settings2 = {
		noDataMessage: 'Nenhuma mensagem',
		editable: false,
		mode: 'external',
		pager: {
			perPage: 20,
			display: true,
		},
		actions: false,
		add: '',
		edit: {
			editButtonContent: '<i class="fa fa-edit"></i>',
			saveButtonContent: '<i class="nb-checkmark"></i>',
			cancelButtonContent: '<i class="nb-close"></i>',
		},
		delete: false,
		columns: {
			subject: {
			title: 'Título de mensagem',
			type: 'string',
			},
			created: {
			title: 'Data do envio',
			type: 'string',
			},
		},
	};
	source2: SmartTableRemoteDataSource = new SmartTableRemoteDataSource(
		this.notificationContentService,
	);

	constructor(
		private gangService: GangService,
		private phaseService: PhaseService,
		private gangPhaseService: GangPhaseService,
		private userService: UserService,
		private notificationService: NotificationService,
		private notificationContentService: NotificationContentService,
		private router: Router,
	) {
		super();
	}

	ngOnDestroy() {}

	ngOnInit() {
		this.momentLocation();
		this.getTotalGangs();
		this.getPhases();
		this.getUser();
	}

	getUser() {
		this.pushSub(
			this.userService.getCurrentUser().subscribe(result => {
			this.user = result;
			if (this.user.groups[0]) {
				this.groupId = this.user.groups[0];
			}
			}),
		);
	}

	onUserRowSelect(event): void {
		this.router.navigate(['admin', 'notifications', 'view', event.data.id]);
	}

	momentLocation() {
		moment.updateLocale('en', {
			relativeTime: {
			future: 'Faltam <strong>%s</strong>',
			past: '%s Atras',
			s: 'alguns segundos',
			ss: '%d segundos',
			m: 'um minuto',
			mm: '%d mintos',
			h: 'uma hora',
			hh: '%d horas',
			d: 'um dia',
			dd: '%d dias',
			M: 'um mês',
			MM: '%d meses',
			y: 'um ano',
			yy: '%d anos',
			},
		});
	}

	getPhases() {
		this.pushSub(
			this.phaseService.getAllEntries().subscribe(result => {
			this.phases = result;
			for (let i = this.phases.length - 1; i >= 0; i--) {
				this.phases[i].start_date = moment(
				this.phases[i].start_at,
				'YYYY-MM-DD',
				).format('DD/MM');
				if (this.phases[i].status === 'Ativo') {
				this.current_phase = this.phases[i];
				this.days_remaining = moment(
					this.current_phase.end_at,
					'YYYY-MM-DD',
				).fromNow();
				break;
				}
			}
			this.getTotalDeliverables();
			}),
		);
	}

	getTotalGangs() {
		this.pushSub(
			this.gangService
			.getAllEntries({ is_focal_point: false })
			.subscribe(result => {
				for (let i = 0; i < result.length; i++) {
				const group = result[i];
				if (group.is_active && !group.is_focal_point) {
					this.active_groups++;
				}
				}
			}),
		);
		this.pushSub(
			this.gangService
			.getCount({ params: { is_focal_point: false } })
			.subscribe(result => {
				this.total_groups = result.count;
			}),
		);
	}

	getTotalDeliverables() {
		if (this.current_phase.id) {
			this.pushSub(
			this.gangPhaseService
				.completeCount(this.current_phase.id)
				.subscribe(result => {
				this.total_deliverables = result.count;
				}),
			);
		} else {
			this.total_deliverables = -1;
		}
	}
}
