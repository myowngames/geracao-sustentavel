import { NgModule } from '@angular/core';
import { NgxEchartsModule } from 'ngx-echarts';

import { ThemeModule } from '../../@theme/theme.module';
import { DashboardComponent } from './dashboard.component';
import { Ng2SmartTableModule } from 'ng2-smart-table';
import { VideosModule } from '../videos/videos.module';

@NgModule({
	imports: [
		ThemeModule,
		NgxEchartsModule,
		Ng2SmartTableModule,
		VideosModule,
	],
	declarations: [
		DashboardComponent,

	],
})
export class DashboardModule { }
