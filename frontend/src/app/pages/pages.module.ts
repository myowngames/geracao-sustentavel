import { NgModule } from '@angular/core';
import { ToasterModule } from 'angular2-toaster';

import { PagesRoutingModule } from './pages-routing.module';
import { ThemeModule } from '../@theme/theme.module';

import { PagesComponent } from './pages.component';
import { DashboardModule } from './dashboard/dashboard.module';
import { CitiesModule } from './cities/cities.module';
import { GangsModule } from './gangs/gangs.module';
import { NotificationsModule } from './notifications/notifications.module';
import { PhasesModule } from './phases/phases.module';
import { PhaseViewModule } from './phaseview/phaseview.module';
import { VideosModule } from './videos/videos.module';
import { FrontendFilesModule } from './frontend-files/frontend-files.module';
import { GroupModule } from './group/group.module';
import { UserModule } from './user/user.module';

const PAGES_COMPONENTS = [
  PagesComponent,
];

@NgModule({
  imports: [
    ToasterModule,
    PagesRoutingModule,
    ThemeModule,
    DashboardModule,
    CitiesModule,
    GangsModule,
    NotificationsModule,
    PhasesModule,
    VideosModule,
    FrontendFilesModule,
    GroupModule,
    UserModule,
    PhaseViewModule
  ],
  declarations: [
    ...PAGES_COMPONENTS,
  ]
})
export class PagesModule {
}
