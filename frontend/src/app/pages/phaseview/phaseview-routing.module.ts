import { RouterModule, Routes, } from '@angular/router';
import { NgModule } from '@angular/core';

import { PhaseViewComponent } from './phaseview.component';
import { InstructionsComponent } from './instructions/instructions.component';

const routes: Routes = [
    {
        path: '',
        component: PhaseViewComponent,
    },
    {
        path: 'instructions/:id',
        component: InstructionsComponent,
    },
    //{
    //    path: 'edit/:id',
    //    component: NewPhaseComponent,
    //},
    //{
    //    path: 'new',
    //    component: NewPhaseComponent,
    //},
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule],
})
export class PhaseViewRoutingModule {

}
