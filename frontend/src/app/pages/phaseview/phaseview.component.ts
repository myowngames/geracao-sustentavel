import { Component, OnDestroy, OnInit } from '@angular/core';
import { NgbDateStruct } from '@ng-bootstrap/ng-bootstrap';

import { CanActivate, Router, ActivatedRoute } from '@angular/router';

import { LocalDataSource } from 'ng2-smart-table';
import { SmartTableRemoteDataSource } from '../../@core/data/smart-table_remote-data-source';

import { SubscriptionComponent } from '../../@core/abstract_components/subscription.component';
import { PhaseService } from '../../@core/data/phase.service';
import { GangPhaseService } from '../../@core/data/gang-phase.service';
import { GangPhaseDeliverableService } from '../../@core/data/gang-phase-deliverable.service';
import { FlashService } from '../../@core/utils/flash.service';
import { DomSanitizer } from '@angular/platform-browser';
import { Moment } from 'moment';
import * as moment from 'moment';

@Component({
	selector: 'ngx-phaseview',
	styleUrls: ['./phaseview.component.scss'],
	templateUrl: './phaseview.component.html',
})

export class PhaseViewComponent extends SubscriptionComponent implements OnInit {

	gangPhases = [];
	allowedExtensions = {
		'images': ['png', 'bmp', 'jpg', 'jpeg', 'gif', 'tiff'],
		'zip': ['zip'],
		'documents': ['pdf', 'doc', 'docx'],
	}
	baseIframeLink = 'https://www.youtube.com/embed/{{video_identifier}}/?rel=0';

	constructor(private phaseService: PhaseService,
		private gangPhaseService: GangPhaseService,
		private gangPhaseDeliverableService: GangPhaseDeliverableService,
		private router: Router,
		private activeRoute: ActivatedRoute,
		private flashService: FlashService,
		private sanitizer: DomSanitizer) {
		super();
	}

	ngOnInit() {
		this.getAllPhases();
	}


	getAllPhases() {
		this.pushSub(this.gangPhaseService.getAllEntries().subscribe(
			(result) => {
				this.gangPhases = [];
				this.gangPhases = result;
				for (let i = 0; i < this.gangPhases.length; i++) {
					for (let j = i + 1; j < this.gangPhases.length; j++) {
						const starAtMomentOfI = moment(this.gangPhases[i].phase.start_at, 'YYYY-MM-DD', true);
						const starAtMomentOfJ = moment(this.gangPhases[j].phase.start_at, 'YYYY-MM-DD', true);

						if (starAtMomentOfI > starAtMomentOfJ) {
							const temp = this.gangPhases[i];
							this.gangPhases[i] = this.gangPhases[j];
							this.gangPhases[j] = temp;
						}
					}
				}
				this.checkIfCanStartSending();
				this.setVideoLink();
			},
		));
	}
	getGangPhaseById(id) {
		for (let i = 0; i < this.gangPhases.length; i++) {
			if (this.gangPhases[i].phase.id === id) {
				return this.gangPhases[i];
			}
		}
	}
	checkIfCanStartSending() {
		// Atualização pedida pela UNO/Cliente Checamos se o usuairo já pode começar a enviar o material
		for (let i = 0; i < this.gangPhases.length; i++) {
			const element = this.gangPhases[i];
			this.gangPhases[i].cant_start = false;
			if (this.gangPhases[i].phase.phase_depends) {
				const gangPhaseDepends: any = this.getGangPhaseById(this.gangPhases[i].phase.phase_depends);
				this.gangPhases[i].cant_start = gangPhaseDepends.completed;
			} else {
				this.gangPhases[i].cant_start = true;
			}
		}
	}
	youtube_parser(url) {
		var regExp = /^.*((youtu.be\/)|(v\/)|(\/u\/\w\/)|(embed\/)|(watch\?))\??v?=?([^#\&\?]*).*/;
		var match = url.match(regExp);
		return (match && match[7].length == 11) ? match[7] : false;
	}
	getIframeVideoLink(video_link: string) {
		if (video_link != null) {
			const video_identifier = this.youtube_parser(video_link);
			if (video_identifier) {
				const link = this.baseIframeLink.replace('{{video_identifier}}', video_identifier);
				return this.sanitizer.bypassSecurityTrustResourceUrl(link);
			} else {
				return null;
			}
		} else {
			return null;
		}
	}
	setVideoLink() {
		for (let i = this.gangPhases.length - 1; i >= 0; i--) {
			for (let j = this.gangPhases[i].gang_phase_deliverable.length - 1; j >= 0; j--) {
				if (this.gangPhases[i]) {
					if (this.gangPhases[i].gang_phase_deliverable[j].video_link) {
						this.gangPhases[i].gang_phase_deliverable[j].video_iframe_link = this.getIframeVideoLink(this.gangPhases[i].gang_phase_deliverable[j].video_link);
					}
				}
			}
		}
	}

	doesPhaseHasDeliverableFile(gangPhase, file) {
		for (let i = 0; i < gangPhase.gang_phase_deliverable.length; i++) {
			if (gangPhase.gang_phase_deliverable[i].deliverable_file === file.id) {
				return true;
			}
		}
		return false;
	}

	getFromGangPhaseDeliverableFile(gangPhase, referenceDeliverableFile, index) {
		for (let i = 0; i < gangPhase.gang_phase_deliverable.length; i++) {
			if (gangPhase.gang_phase_deliverable[i].deliverable_file === referenceDeliverableFile.id) {
				return gangPhase.gang_phase_deliverable[i][index];
			}
		}
	}

	uploadGangPhaseDeliverable($event, gangPhase, referenceDeliverableFile) {
		const data = {
			'deliverable_file': referenceDeliverableFile.id,
			'gang_phase': gangPhase.id,
		};

		if ($event === 'video') {
			data['video_link'] = document.getElementById('video_link-' + referenceDeliverableFile.id)['value'];
			data['name'] = 'Video';
		} else {
			const file = $event.target.files[0];
			data['name'] = file.name;
			data['file'] = file;

			//check filetype
			const fileExtension = data['file'].name.split('.');
			if (fileExtension.length > 1) {
				if (this.allowedExtensions[referenceDeliverableFile.allowed_content_types].indexOf(fileExtension[fileExtension.length - 1].toLowerCase()) === -1) {
					this.flashService.push('error', 'O arquivo enviado não é um arquivo válido. Favor envie um arquivo do tipo ' + String(this.allowedExtensions[referenceDeliverableFile.allowed_content_types].join(', ')));
					return;
				}
			} else {
				this.flashService.push('error', 'Este arquivo não possui uma extensão, favor envie um arquivo válido.');
			}
		}

		this.pushSub(this.gangPhaseDeliverableService.createEntryWithFile(data).subscribe(
			(result) => {
				if (result.video_link) {
					result.video_iframe_link = this.getIframeVideoLink(result.video_link);
				}
				gangPhase.gang_phase_deliverable.push(result);
				this.flashService.push('success', 'Arquivo enviado com sucesso');
				this.getAllPhases();
			},
		));
	}

	printDate(date): string {
		return moment(date).format('DD/MM/YYYY');
	}


	removeGangPhaseDeliverable(gangPhaseDeliverableFileId = null) {
		if (gangPhaseDeliverableFileId != null) {
			this.pushSub(this.gangPhaseDeliverableService.deleteEntry(gangPhaseDeliverableFileId).subscribe(
				(result) => {
					this.flashService.push('success', 'Arquivo removido com sucesso.');
					this.getAllPhases();
				},
			));
		}

	}

}
