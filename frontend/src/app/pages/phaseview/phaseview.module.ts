import { NgModule } from '@angular/core';
import { NgxEchartsModule } from 'ngx-echarts';
import { DirectivesModule } from '../../@core/directives/directives.module';
import { ThemeModule } from '../../@theme/theme.module';
import { PhaseViewRoutingModule } from './phaseview-routing.module';
import { Ng2SmartTableModule } from 'ng2-smart-table';
import { FormsModule } from '@angular/forms';
import { TextMaskModule } from 'angular2-text-mask';

import { PhaseViewComponent } from './phaseview.component';
import { InstructionsComponent } from './instructions/instructions.component';

@NgModule({
    imports: [
        ThemeModule,
        NgxEchartsModule,
        PhaseViewRoutingModule,
        Ng2SmartTableModule,
        DirectivesModule,
        FormsModule,
        TextMaskModule
    ],
    declarations: [
        PhaseViewComponent,
        InstructionsComponent
    ],
})

export class PhaseViewModule { }