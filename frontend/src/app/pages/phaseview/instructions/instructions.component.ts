import { Component, OnDestroy, OnInit } from '@angular/core';
import { NgbDateStruct } from '@ng-bootstrap/ng-bootstrap';

import { CanActivate, Router, ActivatedRoute  } from '@angular/router';

import { SubscriptionComponent } from '../../../@core/abstract_components/subscription.component';
import { GangPhaseService } from '../../../@core/data/gang-phase.service';
import { FlashService } from '../../../@core/utils/flash.service';

import { Moment } from 'moment';
import * as moment from 'moment';

@Component({
    selector: 'ngx-phaseview-instructions',
    styleUrls: ['./instructions.component.scss'],
    templateUrl: './instructions.component.html',
})

export class InstructionsComponent extends SubscriptionComponent{

    public gangPhase: any = {};

    constructor(
        private gangPhaseService:GangPhaseService,
		private router: Router,
		private activatedRoute: ActivatedRoute,
		private flashService: FlashService,
   	){
        super();
        this.gangPhase.id = this.activatedRoute.params['value'].id;
  	}

	ngOnInit(){
		this.pushSub(this.gangPhaseService.getSingleEntry(this.gangPhase.id).subscribe(
            (result) => {
                this.gangPhase = result;
            }
        ));
	}

}
