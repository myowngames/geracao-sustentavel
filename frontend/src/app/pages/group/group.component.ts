import { Component, OnDestroy, OnInit } from '@angular/core';
import { CanActivate, Router, ActivatedRoute } from '@angular/router';

import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { LocalDataSource } from 'ng2-smart-table';
import { SmartTableRemoteDataSource } from '../../@core/data/smart-table_remote-data-source';

import { SubscriptionComponent } from '../../@core/abstract_components/subscription.component';
import { GroupService } from '../../@core/data/group.service';
import { FlashService } from '../../@core/utils/flash.service';

@Component({
	selector: 'ngx-group',
	styleUrls: ['./group.component.scss'],
	templateUrl: './group.component.html',
})

export class GroupComponent extends SubscriptionComponent {
	settings = {
		noDataMessage: 'Nenhuma Informação Encontrada',
		editable: false,
		mode: 'external',
		pager: {
			perPage: 20,
			display: true,
		},
		actions: {
			columnTitle: 'Ações',
		},
		add: {
			addButtonContent: '<i class="nb-plus"></i>',
			createButtonContent: '<i class="nb-checkmark"></i>',
			cancelButtonContent: '<i class="nb-close"></i>',
		},
		edit: {
			editButtonContent: '<i class="fa fa-edit"></i>',
			saveButtonContent: '<i class="nb-checkmark"></i>',
			cancelButtonContent: '<i class="nb-close"></i>',
		},
		delete: {
			deleteButtonContent: '<i class="nb-trash"></i>',
			confirmDelete: true,
		},
		columns: {
			id: {
				title: 'ID',
				type: 'number',
			},
			name: {
				title: 'Nome',
				type: 'string',
			},
		},
	};
	source: SmartTableRemoteDataSource = new SmartTableRemoteDataSource(this.groupService);
	constructor(
		private groupService: GroupService,
		private router: Router,
		private activeRoute: ActivatedRoute,
		private flashService: FlashService,
	) {
		super();
	}


	createNewEntryButtonClickListener(): void {
		this.router.navigate(['admin', 'group', 'new']);
	}

	editEntryButtonClickListener(event): void {
		this.router.navigate(['admin', 'group', 'edit', event.data.id]);
	}

	deleteEntryButtonListener(event): void {
		this.groupService.deleteEntry(event.data.id).subscribe(
			(result) => {
				if (result != false) {
					this.source.remove(event.data);
					this.flashService.push('success', 'Grupo removido com sucesso!');
				}
			}
		);
	}
}
