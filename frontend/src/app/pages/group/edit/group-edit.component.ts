import { Component } from '@angular/core';
import { CanActivate, Router, ActivatedRoute  } from '@angular/router';
import { FormBuilder, FormGroup, Validators, FormControl, FormArray } from '@angular/forms';

import { SubscriptionComponent } from '../../../@core/abstract_components/subscription.component';
import { PermissionService } from '../../../@core/data/permission.service';
import { GroupService } from '../../../@core/data/group.service';
import { FlashService } from '../../../@core/utils/flash.service';

@Component({
	selector: 'group-edit',
	styleUrls: ['./group-edit.component.scss'],
	templateUrl: './group-edit.component.html',
})

export class GroupEditComponent extends SubscriptionComponent{
	public form;
	public group: any = {};
	public allPermissions: any = [];


    constructor(
    	private groupsService: GroupService,
        private flashService: FlashService,
        private permissionsService: PermissionService,
    	private fb: FormBuilder,
    	private router: Router,
    	private activatedRoute: ActivatedRoute
    ){
        super();
    	this.group.id = this.activatedRoute.params['value'].id;
    }

    ngOnInit(){
        this.form = this.fb.group({
            id: [
                this.group.id,
                Validators.compose([])
            ],
            name: [
                this.group.name,
                Validators.compose ([Validators.required, Validators.minLength(4)])
            ],
            permissions: this.fb.array([]),
        });


        if (typeof this.group.id != 'undefined'){
            this.groupsService.getSingleEntry(this.group.id).subscribe(
                (result) => {
                    this.group = result;

                    this.form.patchValue(this.group);
                }
            );
        }

        this.getAllPermissions();
    }


    getAllPermissions(): void{
        this.permissionsService.getAllEntries().subscribe(
            (result) => {
                this.allPermissions = result;
                this.markFormPermissions();
            }
        );
    }


    markFormPermissions(): void{
        let groupPermissions = this.group.permissions;
        let permissions = this.form.get('permissions') as FormArray;

        while(permissions.length != 0){
            permissions.removeAt(permissions.length - 1);
        }

        for(let i=0; i < this.allPermissions.length; i++){
            let userBelongsToGroup = false;

            if (typeof groupPermissions != 'undefined'){
                for(let j=0; j < groupPermissions.length; j++){
                    if (groupPermissions[j].id == this.allPermissions[i].id){
                        userBelongsToGroup = true;
                        break;
                    }
                }
            }

            permissions.push(new FormControl(userBelongsToGroup));
        }
    }


    onSubmit(){
        let data = this.form.value;
        let permissions = data.permissions;
        let permissionIds = [];

        delete data.permissions;

        //get the groups ids
        for(let i=0; i < permissions.length; i++){
            if (permissions[i] == true){
                permissionIds.push(this.allPermissions[i].id);
            }
        }

        if (this.group.id != null && typeof this.group.id != 'undefined'){
            this.groupsService.updateEntry(this.group.id, data).subscribe(
                (result) => {
                    this.group = result;
                    this.saveGroupPermissions(permissionIds);
                    this.flashService.push('success', 'Group updated successfully');
                    this.router.navigate(['admin', 'group']);
                }
            );
        }
        else{
            this.groupsService.createEntry(data).subscribe(
                (result) => {
                    this.group = result;
                    this.saveGroupPermissions(permissionIds);
                    this.flashService.push('success', 'Group saved successfully');
                    this.router.navigate(['admin', 'group']);
                }
            );

        }
    }

    checkAll(){
        let permissions = this.form.get('permissions') as FormArray;

        for(let i=0; i < permissions.controls.length; i++){
            permissions.controls[i].patchValue(true);
        }
    }

    saveGroupPermissions(permissionIds: Number[] = []): void{
        if (permissionIds.length == 0 || this.group.id == null){
            return;
        }

        this.groupsService.setPermissions(this.group.id, permissionIds).subscribe(
            (result) => {
            }
        );
    }

}
