import { NgModule } from '@angular/core';
import { NgxEchartsModule } from 'ngx-echarts';

import { ThemeModule } from '../../@theme/theme.module';
import { GroupComponent } from './group.component';
import { GroupEditComponent } from './edit/group-edit.component';
import { GroupRoutingModule} from './group-routing.module';

import { Ng2SmartTableModule } from 'ng2-smart-table';

@NgModule({
    imports: [
        ThemeModule,
        NgxEchartsModule,
        Ng2SmartTableModule,
        GroupRoutingModule
    ],
    declarations: [
        GroupComponent,
        GroupEditComponent
    ],
})
export class GroupModule { }