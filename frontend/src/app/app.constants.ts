export var CONSTANTS = {
	link_facebook : "https://www.facebook.com/InterCementBrasil/",
	link_github : "https://github.com/shortsave/",
	link_youtube : "https://www.youtube.com/channel/UCZbIz68r_LFGlgP_DKrcZNQ",
	link_twitter : "https://twitter.com/BotDocIt",
	link_linkedin : "https://www.linkedin.com/company/intercement",
	botdoc_blog : "https://botdoc.io/",
	botdoc_terms : "https://botdoc.io/terms/",
	botdoc_security : "https://botdoc.io/security-center",
	botdoc_privacy_policy : "https://botdoc.io/privacy-policy",  
}