import { Component, Inject, HostListener, Input, Output, EventEmitter, ViewChild} from '@angular/core';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';

@Component({
    selector: 'materialize-colorpicker',
    templateUrl: './materialize-colorpicker.component.html',
    styleUrls: ['./materialize-colorpicker.component.scss']
})

export class MaterializeColorpickerComponent {

	@Output() modelToOutput = new EventEmitter<String>();
	@Input() initialValue: String = "";

	public modelToStore: String;
	public selectedColorIndex: String;
	public dropdownColor: String = "btn-outline";
	public colorClass: String = "";

	public colors = [
		{
			'label': 'Red',
			'colors': [
				{'label': 'red lighten-5', 'value': '#ffebee'},
				{'label': 'red lighten-4', 'value': '#ffcdd2'},
				{'label': 'red lighten-3', 'value': '#ef9a9a'},
				{'label': 'red lighten-2', 'value': '#e57373'},
				{'label': 'red lighten-1', 'value': '#ef5350'},
				{'label': 'red', 'value': '#f44336'},
				{'label': 'red darken-1', 'value': '#e53935'},
				{'label': 'red darken-2', 'value': '#d32f2f'},
				{'label': 'red darken-3', 'value': '#c62828'},
				{'label': 'red darken-4', 'value': '#b71c1c'},
				{'label': 'red accent-1', 'value': '#ff8a80'},
				{'label': 'red accent-2', 'value': '#ff5252'},
				{'label': 'red accent-3', 'value': '#ff1744'},
				{'label': 'red accent-4', 'value': '#d50000'},
			]
		},
		{
			'label': 'Pink',
			'colors': [
				{'label': 'pink lighten-5', 'value': '#fce4ec'},
				{'label': 'pink lighten-4', 'value': '#f8bbd0'},
				{'label': 'pink lighten-3', 'value': '#f48fb1'},
				{'label': 'pink lighten-2', 'value': '#f06292'},
				{'label': 'pink lighten-1', 'value': '#ec407a'},
				{'label': 'pink', 'value': '#e91e63'},
				{'label': 'pink darken-1', 'value': '#d81b60'},
				{'label': 'pink darken-2', 'value': '#c2185b'},
				{'label': 'pink darken-3', 'value': '#ad1457'},
				{'label': 'pink darken-4', 'value': '#880e4f'},
				{'label': 'pink accent-1', 'value': '#ff80ab'},
				{'label': 'pink accent-2', 'value': '#ff4081'},
				{'label': 'pink accent-3', 'value': '#f50057'},
				{'label': 'pink accent-4', 'value': '#c51162'},
			]
		},
		{
			'label': 'Purple',
			'colors': [
				{'label': 'purple lighten-5', 'value': '#f3e5f5'},
				{'label': 'purple lighten-4', 'value': '#e1bee7'},
				{'label': 'purple lighten-3', 'value': '#ce93d8'},
				{'label': 'purple lighten-2', 'value': '#ba68c8'},
				{'label': 'purple lighten-1', 'value': '#ab47bc'},
				{'label': 'purple', 'value': '#9c27b0'},
				{'label': 'purple darken-1', 'value': '#8e24aa'},
				{'label': 'purple darken-2', 'value': '#7b1fa2'},
				{'label': 'purple darken-3', 'value': '#6a1b9a'},
				{'label': 'purple darken-4', 'value': '#4a148c'},
				{'label': 'purple accent-1', 'value': '#ea80fc'},
				{'label': 'purple accent-2', 'value': '#e040fb'},
				{'label': 'purple accent-3', 'value': '#d500f9'},
				{'label': 'purple accent-4', 'value': '#aa00ff'},
			]
		},
		{
			'label': 'Deep Purple',
			'colors': [
				{'label': 'deep-purple lighten-5', 'value': '#ede7f6'},
				{'label': 'deep-purple lighten-4', 'value': '#d1c4e9'},
				{'label': 'deep-purple lighten-3', 'value': '#b39ddb'},
				{'label': 'deep-purple lighten-2', 'value': '#9575cd'},
				{'label': 'deep-purple lighten-1', 'value': '#7e57c2'},
				{'label': 'deep-purple', 'value': '#673ab7'},
				{'label': 'deep-purple darken-1', 'value': '#5e35b1'},
				{'label': 'deep-purple darken-2', 'value': '#512da8'},
				{'label': 'deep-purple darken-3', 'value': '#4527a0'},
				{'label': 'deep-purple darken-4', 'value': '#311b92'},
				{'label': 'deep-purple accent-1', 'value': '#b388ff'},
				{'label': 'deep-purple accent-2', 'value': '#7c4dff'},
				{'label': 'deep-purple accent-3', 'value': '#651fff'},
				{'label': 'deep-purple accent-4', 'value': '#6200ea'},
			]
		},
		{
			'label': 'Indigo',
			'colors': [
				{'label': 'indigo lighten-5', 'value': '#e8eaf6'},
				{'label': 'indigo lighten-4', 'value': '#c5cae9'},
				{'label': 'indigo lighten-3', 'value': '#9fa8da'},
				{'label': 'indigo lighten-2', 'value': '#7986cb'},
				{'label': 'indigo lighten-1', 'value': '#5c6bc0'},
				{'label': 'indigo', 'value': '#3f51b5'},
				{'label': 'indigo darken-1', 'value': '#3949ab'},
				{'label': 'indigo darken-2', 'value': '#303f9f'},
				{'label': 'indigo darken-3', 'value': '#283593'},
				{'label': 'indigo darken-4', 'value': '#1a237e'},
				{'label': 'indigo accent-1', 'value': '#8c9eff'},
				{'label': 'indigo accent-2', 'value': '#536dfe'},
				{'label': 'indigo accent-3', 'value': '#3d5afe'},
				{'label': 'indigo accent-4', 'value': '#304ffe'},
			]
		},
		{
			'label': 'Blue',
			'colors': [
				{'label': 'blue lighten-5', 'value': '#e3f2fd'},
				{'label': 'blue lighten-4', 'value': '#bbdefb'},
				{'label': 'blue lighten-3', 'value': '#90caf9'},
				{'label': 'blue lighten-2', 'value': '#64b5f6'},
				{'label': 'blue lighten-1', 'value': '#42a5f5'},
				{'label': 'blue', 'value': '#2196f3'},
				{'label': 'blue darken-1', 'value': '#1e88e5'},
				{'label': 'blue darken-2', 'value': '#1976d2'},
				{'label': 'blue darken-3', 'value': '#1565c0'},
				{'label': 'blue darken-4', 'value': '#0d47a1'},
				{'label': 'blue accent-1', 'value': '#82b1ff'},
				{'label': 'blue accent-2', 'value': '#448aff'},
				{'label': 'blue accent-3', 'value': '#2979ff'},
				{'label': 'blue accent-4', 'value': '#2962ff'},
			]
		},
		{
			'label': 'Light Blue',
			'colors': [
				{'label': 'light-blue lighten-5', 'value': '#e1f5fe'},
				{'label': 'light-blue lighten-4', 'value': '#b3e5fc'},
				{'label': 'light-blue lighten-3', 'value': '#81d4fa'},
				{'label': 'light-blue lighten-2', 'value': '#4fc3f7'},
				{'label': 'light-blue lighten-1', 'value': '#29b6f6'},
				{'label': 'light-blue', 'value': '#03a9f4'},
				{'label': 'light-blue darken-1', 'value': '#039be5'},
				{'label': 'light-blue darken-2', 'value': '#0288d1'},
				{'label': 'light-blue darken-3', 'value': '#0277bd'},
				{'label': 'light-blue darken-4', 'value': '#01579b'},
				{'label': 'light-blue accent-1', 'value': '#80d8ff'},
				{'label': 'light-blue accent-2', 'value': '#40c4ff'},
				{'label': 'light-blue accent-3', 'value': '#00b0ff'},
				{'label': 'light-blue accent-4', 'value': '#0091ea'},
			]
		},
		{
			'label': 'Cyan',
			'colors': [
				{'label': 'cyan lighten-5', 'value': '#e0f7fa'},
				{'label': 'cyan lighten-4', 'value': '#b2ebf2'},
				{'label': 'cyan lighten-3', 'value': '#80deea'},
				{'label': 'cyan lighten-2', 'value': '#4dd0e1'},
				{'label': 'cyan lighten-1', 'value': '#26c6da'},
				{'label': 'cyan', 'value': '#00bcd4'},
				{'label': 'cyan darken-1', 'value': '#00acc1'},
				{'label': 'cyan darken-2', 'value': '#0097a7'},
				{'label': 'cyan darken-3', 'value': '#00838f'},
				{'label': 'cyan darken-4', 'value': '#006064'},
				{'label': 'cyan accent-1', 'value': '#84ffff'},
				{'label': 'cyan accent-2', 'value': '#18ffff'},
				{'label': 'cyan accent-3', 'value': '#00e5ff'},
				{'label': 'cyan accent-4', 'value': '#00b8d4'},
			]
		},
		{
			'label': 'Teal',
			'colors': [
				{'label': 'teal lighten-5', 'value': '#e0f2f1'},
				{'label': 'teal lighten-4', 'value': '#b2dfdb'},
				{'label': 'teal lighten-3', 'value': '#80cbc4'},
				{'label': 'teal lighten-2', 'value': '#4db6ac'},
				{'label': 'teal lighten-1', 'value': '#26a69a'},
				{'label': 'teal', 'value': '#009688'},
				{'label': 'teal darken-1', 'value': '#00897b'},
				{'label': 'teal darken-2', 'value': '#00796b'},
				{'label': 'teal darken-3', 'value': '#00695c'},
				{'label': 'teal darken-4', 'value': '#004d40'},
				{'label': 'teal accent-1', 'value': '#a7ffeb'},
				{'label': 'teal accent-2', 'value': '#64ffda'},
				{'label': 'teal accent-3', 'value': '#1de9b6'},
				{'label': 'teal accent-4', 'value': '#00bfa5'},
			]
		},
		{
			'label': 'Green',
			'colors': [
				{'label': 'green lighten-5', 'value': '#e8f5e9'},
				{'label': 'green lighten-4', 'value': '#c8e6c9'},
				{'label': 'green lighten-3', 'value': '#a5d6a7'},
				{'label': 'green lighten-2', 'value': '#81c784'},
				{'label': 'green lighten-1', 'value': '#66bb6a'},
				{'label': 'green', 'value': '#4caf50'},
				{'label': 'green darken-1', 'value': '#43a047'},
				{'label': 'green darken-2', 'value': '#388e3c'},
				{'label': 'green darken-3', 'value': '#2e7d32'},
				{'label': 'green darken-4', 'value': '#1b5e20'},
				{'label': 'green accent-1', 'value': '#b9f6ca'},
				{'label': 'green accent-2', 'value': '#69f0ae'},
				{'label': 'green accent-3', 'value': '#00e676'},
				{'label': 'green accent-4', 'value': '#00c853'},
			]
		},
		{
			'label': 'Light Green',
			'colors': [
				{'label': 'light-green lighten-5', 'value': '#f1f8e9'},
				{'label': 'light-green lighten-4', 'value': '#dcedc8'},
				{'label': 'light-green lighten-3', 'value': '#c5e1a5'},
				{'label': 'light-green lighten-2', 'value': '#aed581'},
				{'label': 'light-green lighten-1', 'value': '#9ccc65'},
				{'label': 'light-green', 'value': '#8bc34a'},
				{'label': 'light-green darken-1', 'value': '#7cb342'},
				{'label': 'light-green darken-2', 'value': '#689f38'},
				{'label': 'light-green darken-3', 'value': '#558b2f'},
				{'label': 'light-green darken-4', 'value': '#33691e'},
				{'label': 'light-green accent-1', 'value': '#ccff90'},
				{'label': 'light-green accent-2', 'value': '#b2ff59'},
				{'label': 'light-green accent-3', 'value': '#76ff03'},
				{'label': 'light-green accent-4', 'value': '#64dd17'},
			]
		},
		{
			'label': 'Lime',
			'colors': [
				{'label': 'lime lighten-5', 'value': '#f9fbe7'},
				{'label': 'lime lighten-4', 'value': '#f0f4c3'},
				{'label': 'lime lighten-3', 'value': '#e6ee9c'},
				{'label': 'lime lighten-2', 'value': '#dce775'},
				{'label': 'lime lighten-1', 'value': '#d4e157'},
				{'label': 'lime', 'value': '#cddc39'},
				{'label': 'lime darken-1', 'value': '#c0ca33'},
				{'label': 'lime darken-2', 'value': '#afb42b'},
				{'label': 'lime darken-3', 'value': '#9e9d24'},
				{'label': 'lime darken-4', 'value': '#827717'},
				{'label': 'lime accent-1', 'value': '#f4ff81'},
				{'label': 'lime accent-2', 'value': '#eeff41'},
				{'label': 'lime accent-3', 'value': '#c6ff00'},
				{'label': 'lime accent-4', 'value': '#aeea00'},
			]
		},
		{
			'label': 'Yellow',
			'colors': [
				{'label': 'yellow lighten-5', 'value': '#fffde7'},
				{'label': 'yellow lighten-4', 'value': '#fff9c4'},
				{'label': 'yellow lighten-3', 'value': '#fff59d'},
				{'label': 'yellow lighten-2', 'value': '#fff176'},
				{'label': 'yellow lighten-1', 'value': '#ffee58'},
				{'label': 'yellow', 'value': '#ffeb3b'},
				{'label': 'yellow darken-1', 'value': '#fdd835'},
				{'label': 'yellow darken-2', 'value': '#fbc02d'},
				{'label': 'yellow darken-3', 'value': '#f9a825'},
				{'label': 'yellow darken-4', 'value': '#f57f17'},
				{'label': 'yellow accent-1', 'value': '#ffff8d'},
				{'label': 'yellow accent-2', 'value': '#ffff00'},
				{'label': 'yellow accent-3', 'value': '#ffea00'},
				{'label': 'yellow accent-4', 'value': '#ffd600'},
			]
		},
		{
			'label': 'Amber',
			'colors': [
				{'label': 'amber lighten-5', 'value': '#fff8e1'},
				{'label': 'amber lighten-4', 'value': '#ffecb3'},
				{'label': 'amber lighten-3', 'value': '#ffe082'},
				{'label': 'amber lighten-2', 'value': '#ffd54f'},
				{'label': 'amber lighten-1', 'value': '#ffca28'},
				{'label': 'amber', 'value': '#ffc107'},
				{'label': 'amber darken-1', 'value': '#ffb300'},
				{'label': 'amber darken-2', 'value': '#ffa000'},
				{'label': 'amber darken-3', 'value': '#ff8f00'},
				{'label': 'amber darken-4', 'value': '#ff6f00'},
				{'label': 'amber accent-1', 'value': '#ffe57f'},
				{'label': 'amber accent-2', 'value': '#ffd740'},
				{'label': 'amber accent-3', 'value': '#ffc400'},
				{'label': 'amber accent-4', 'value': '#ffab00'},
			]
		},
		{
			'label': 'Orange',
			'colors': [
				{'label': 'orange lighten-5', 'value': '#fff3e0'},
				{'label': 'orange lighten-4', 'value': '#ffe0b2'},
				{'label': 'orange lighten-3', 'value': '#ffcc80'},
				{'label': 'orange lighten-2', 'value': '#ffb74d'},
				{'label': 'orange lighten-1', 'value': '#ffa726'},
				{'label': 'orange', 'value': '#ff9800'},
				{'label': 'orange darken-1', 'value': '#fb8c00'},
				{'label': 'orange darken-2', 'value': '#f57c00'},
				{'label': 'orange darken-3', 'value': '#ef6c00'},
				{'label': 'orange darken-4', 'value': '#e65100'},
				{'label': 'orange accent-1', 'value': '#ffd180'},
				{'label': 'orange accent-2', 'value': '#ffab40'},
				{'label': 'orange accent-3', 'value': '#ff9100'},
				{'label': 'orange accent-4', 'value': '#ff6d00'},
			]
		},
		{
			'label': 'Deep Orange',
			'colors': [
				{'label': 'deep-orange lighten-5', 'value': '#fbe9e7'},
				{'label': 'deep-orange lighten-4', 'value': '#ffccbc'},
				{'label': 'deep-orange lighten-3', 'value': '#ffab91'},
				{'label': 'deep-orange lighten-2', 'value': '#ff8a65'},
				{'label': 'deep-orange lighten-1', 'value': '#ff7043'},
				{'label': 'deep-orange', 'value': '#ff5722'},
				{'label': 'deep-orange darken-1', 'value': '#f4511e'},
				{'label': 'deep-orange darken-2', 'value': '#e64a19'},
				{'label': 'deep-orange darken-3', 'value': '#d84315'},
				{'label': 'deep-orange darken-4', 'value': '#bf360c'},
				{'label': 'deep-orange accent-1', 'value': '#ff9e80'},
				{'label': 'deep-orange accent-2', 'value': '#ff6e40'},
				{'label': 'deep-orange accent-3', 'value': '#ff3d00'},
				{'label': 'deep-orange accent-4', 'value': '#dd2c00'},
			]
		},
		{
			'label': 'Brown',
			'colors': [
				{'label': 'brown lighten-5', 'value': '#efebe9'},
				{'label': 'brown lighten-4', 'value': '#d7ccc8'},
				{'label': 'brown lighten-3', 'value': '#bcaaa4'},
				{'label': 'brown lighten-2', 'value': '#a1887f'},
				{'label': 'brown lighten-1', 'value': '#8d6e63'},
				{'label': 'brown', 'value': '#795548'},
				{'label': 'brown darken-1', 'value': '#6d4c41'},
				{'label': 'brown darken-2', 'value': '#5d4037'},
				{'label': 'brown darken-3', 'value': '#4e342e'},
				{'label': 'brown darken-4', 'value': '#3e2723'},
			]
		},
		{
			'label': 'Grey',
			'colors': [
				{'label': 'grey lighten-5', 'value': '#fafafa'},
				{'label': 'grey lighten-4', 'value': '#f5f5f5'},
				{'label': 'grey lighten-3', 'value': '#eeeeee'},
				{'label': 'grey lighten-2', 'value': '#e0e0e0'},
				{'label': 'grey lighten-1', 'value': '#bdbdbd'},
				{'label': 'grey', 'value': '#9e9e9e'},
				{'label': 'grey darken-1', 'value': '#757575'},
				{'label': 'grey darken-2', 'value': '#616161'},
				{'label': 'grey darken-3', 'value': '#424242'},
				{'label': 'grey darken-4', 'value': '#212121'},
			]
		},
		{
			'label': 'Blue',
			'colors': [
				{'label': 'blue-grey lighten-5', 'value': '#eceff1'},
				{'label': 'blue-grey lighten-4', 'value': '#cfd8dc'},
				{'label': 'blue-grey lighten-3', 'value': '#b0bec5'},
				{'label': 'blue-grey lighten-2', 'value': '#90a4ae'},
				{'label': 'blue-grey lighten-1', 'value': '#78909c'},
				{'label': 'blue-grey', 'value': '#607d8b'},
				{'label': 'blue-grey darken-1', 'value': '#546e7a'},
				{'label': 'blue-grey darken-2', 'value': '#455a64'},
				{'label': 'blue-grey darken-3', 'value': '#37474f'},
				{'label': 'blue-grey darken-4', 'value': '#263238'},
			]
		},
		{
			'label': 'Black',
			'colors': [
				{'label': 'black', 'value': '#000000'},
			]
		},
		{
			'label': 'White',
			'colors': [
				{'label': 'white', 'value': '#ffffff'},
			]
		},
	];

	
	constructor(private modalService: NgbModal) {
	}

    ngOnInit(){
    	if (this.initialValue != 'undefined'){
    		this.setModelValue(this.initialValue)
    	}
    }

    setModelValue(colorLabel){
    	this.colorClass = "";
    	this.dropdownColor = colorLabel;
    	this.modelToStore = colorLabel;
    	this.modelToOutput.emit(this.modelToStore);
    }

    dropdownClick(){
    	this.colorClass = "";
    	this.selectedColorIndex = null;
    }

    changeColor(colorsIndex = 0){
    	this.colorClass = "show";
    	event.stopPropagation();
    	this.selectedColorIndex = colorsIndex.toString();
    }


    //closes the colors if got a click anywhere other than the selected color
	@HostListener("document:click", ['$event'])
	onDocumentClick(event: Event): void {
		this.dropdownClick();
	}

}
