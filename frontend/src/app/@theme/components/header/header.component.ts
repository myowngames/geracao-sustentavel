import { Component, Input, OnInit } from '@angular/core';

import { NbMenuService, NbSidebarService } from '@nebular/theme';
import { UserService } from '../../../@core/data/user.service';
import { NotificationService } from '../../../@core/data/notification.service';
import { AnalyticsService } from '../../../@core/utils/analytics.service';
import { NbTokenService  } from '@nebular/auth';
import { Router } from '@angular/router';
import { Location } from '@angular/common';
import { Observable } from 'rxjs/Observable';

import { SubscriptionComponent } from '../../../@core/abstract_components/subscription.component';

import { SmartTableRemoteDataSource} from "../../../@core/data/smart-table_remote-data-source";

import {DateRenderComponent} from "../smart-table-render";
import {NotificationContentService} from "../../../@core/data/notification-content.service";


@Component({
    selector: 'ngx-header',
    styleUrls: ['./header.component.scss'],
    templateUrl: './header.component.html',
})

export class HeaderComponent extends SubscriptionComponent implements OnInit {
    @Input() position = 'normal';

    user: any;
    
    pastRoute: string;
    
    userMenu = [
        //{ title: 'Meu Perfil', link: '/profile'},
        { title: 'Sair', link: '/auth/logout', icon: 'fa fa-times-circle' }
    ];

    public groupId: number = 0;

    notifications: any = [];
    new_notification_count: number = 0;

    settingsNotifications = {
      noDataMessage: 'Você ainda não recebeu nenhuma mensagem',
      editable: false,
      mode: 'external',
      pager: {
        perPage: 20,
        display: true,
      },
      actions: false,
      add: '',
      edit: false,
      delete: false,
      columns: {
        content: {
          title: '',
          type: 'html',
          valuePrepareFunction: function (cell, row) {
            return cell.subject
          }
 
        }
      },
    };
  sourceNotifications: SmartTableRemoteDataSource = new SmartTableRemoteDataSource(this.notificationService);

    constructor(private sidebarService: NbSidebarService,
              private menuService: NbMenuService,
              private userService: UserService,
              private notificationService: NotificationService,
              private notificationContentService: NotificationContentService,
              private analyticsService: AnalyticsService,
              private tokenService: NbTokenService,
              private router: Router,
              private location: Location) {
      super();
      
    }

    ngOnInit() {
        this.userService.getCurrentUser().subscribe(
            (result) => {
                this.user = result;
            }
        );
        this.getNotificationsCount();
        this.subscribeToCountNotifications();
        this.getUser();
    }
    getUser() {
      this.pushSub(
        this.userService.getCurrentUser().subscribe(
          (result) => {
            this.user = result
            if (typeof this.user.groups[0] === 'number') {
              this.groupId = this.user.groups[0]
            }
          }
        )
      )
    }
    getNotificationsCount() {
      
      this.pushSub(
        this.notificationService.getNewNotificationCount().subscribe(
          (result) => {
            this.new_notification_count = result.count;
          },
          (error) => {
            console.log(error)
          }
        )
      )
    }

    subscribeToCountNotifications() {
      // Reload notification every 5 minutes
      this.pushSub(
        Observable.timer(10000).subscribe((next) => this.getNotificationsCount())
      )
    }

    toggleSidebar(): boolean {
        this.sidebarService.toggle(false, 'menu-sidebar');
        return false;
    }

    toggleSettings(): boolean {
        this.sidebarService.toggle(false, 'settings-sidebar');
        return false;
    }

    goToHome() {
       this.menuService.navigateHome();
    }

    startSearch() {
        this.analyticsService.trackEvent('startSearch');
    }

    logout(){
        // TODO: Change this function to a Helper
        this.tokenService.clear();
        this.router.navigate(['auth/login']);
    }
    onUserRowSelect(event): void{
      if (this.groupId === 2) {
        this.router.navigate(['admin', 'notifications', 'view', event.data.id]);
        this.new_notification_count--;
      } else {
        this.router.navigate(['admin', 'notifications', 'content', event.data.id]);
      }
      
      
    }

    menuClickListener(arg){
        if (typeof arg['functionToCall'] !== 'undefined'){
            if (arg['functionToCall'] === 'logout'){
                this.logout();
            }
        }
    }
}
