import { Component, Input, OnInit } from '@angular/core';

import { ViewCell } from 'ng2-smart-table';
import { Moment } from 'moment';
import * as moment from 'moment';


@Component({
	template: `
    {{renderValue}}
  `,
})
export class DateRenderComponent implements ViewCell, OnInit {

	renderValue: string;

	@Input() value: string | number;
	@Input() rowData: any;

	ngOnInit() {
		const date = new Date(this.value.toString());
		this.renderValue = moment(date).format('DD/MM/YYYY HH:mm:ss')
	}

}
