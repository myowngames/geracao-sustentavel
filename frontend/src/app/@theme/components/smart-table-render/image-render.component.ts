import { Component, Input, OnInit } from '@angular/core';

import { ViewCell } from 'ng2-smart-table';

@Component({
  template: `
  	<div class="image-container">
	  	<a [attr.href]="value" target="_new" *ngIf="!isImage"><i class="fa fa-file fa-4x"></i></a>
	  	<div class="cover" [style.backgroundImage]="'url('+ value +')'" *ngIf="isImage"></div>
  	</div>
  `,
  styleUrls: ['./image-render.component.scss'],
})
export class ImageRenderComponent implements ViewCell, OnInit {

	renderValue: string;
	isImage:boolean = false;
	@Input() value: string;
	@Input() rowData: any;

	ngOnInit() {
		let extension = this.value.split('.').pop().toLowerCase();
		if(extension == 'jpg' || extension == "png" || extension == 'gif'){
			this.isImage = true;
		}
	}

}