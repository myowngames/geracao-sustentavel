import { Component, Input, OnInit } from '@angular/core';

import { ViewCell } from 'ng2-smart-table';

@Component({
	template: `
		<div class="text-center alert alert-success" *ngIf="this.value"><i class="color-success fa fa-thumbs-up" aria-hidden="true"></i></div>
		<div class="text-center alert alert-warning" *ngIf="!this.value"><i class="color-primary fa fa-thumbs-down" aria-hidden="true"></i></div>
	`,
})
export class BooleanRenderComponent implements ViewCell, OnInit {


	@Input() value: string | number;
	@Input() rowData: any;
	

	ngOnInit() {
		
	}

}