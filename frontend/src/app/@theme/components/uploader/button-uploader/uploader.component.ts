import { Component, Inject, HostListener, Input, Output, EventEmitter, ViewChild} from '@angular/core';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';

import { NbAuthService, NbAuthJWTToken } from '@nebular/auth';

@Component({
    selector: 'button-uploader',
    templateUrl: './uploader.component.html',
    styleUrls: ['./uploader.component.scss']
})

export class ButtonUploaderComponent{

	//inputs comming from the user when initializing the component
	@Input() url: string;
	@Input() label: string = "Choose a file";
	@Input() buttonClass: string = "btn btn-info";
	@Input() requestMethod: string = "POST";

	//events dispatched, we may listen for then in the place where we implement the component
	@Output() progressEvent = new EventEmitter();
	@Output() onLoadEvent = new EventEmitter();
	@Output() onErrorEvent = new EventEmitter(); 
	@Output() onAbortEvent = new EventEmitter(); 

	//references to the inputs elements in the HTML
	@ViewChild('buttonInput') buttonInput;
	@ViewChild('fileInput') fileInput;


	private ajaxObject: XMLHttpRequest;
	private formData: FormData;
	public labelToShow: string = this.label; //we update this value in the upload, that is why we got 2 in this comp.


	constructor(
		private nbAuthService: NbAuthService
	) {

	}

	ngOnInit(){
    	this.initObjects();
    }

	inputFileChangeEventListener(event){
		this.formData.delete('file');
		this.formData.append('file', event.target.files[0]);
		this.send();
	}

	send(){
		this.ajaxObject.open(this.requestMethod.toUpperCase(), this.url);
		this.nbAuthService.getToken().subscribe(
			(result) => {
				this.ajaxObject.setRequestHeader('Authorization', 'JWT ' + result['token']);
			    this.setupCallbacks();
			    this.ajaxObject.send(this.formData);
			}
		);
	}

	ajaxObjectListenerProgress(e){

		let total = Math.floor(e.total);
		let sent = Math.floor(e.loaded);
		let percentage = (sent * 100) / total;
		this.labelToShow = String(Math.floor(percentage)) + '%';	
		this.progressEvent.emit(e);
	}

	ajaxObjectListenerOnLoad(e,data){
		this.labelToShow = this.label;
		this.onLoadEvent.emit(e);
	}

	ajaxObjectListenerOnError(e){
		this.onErrorEvent.emit(e);
	}

	ajaxObjectListenerOnAbort(e){
		this.onAbortEvent.emit(e);
	}

    setupCallbacks(){
    	this.ajaxObject.upload.onprogress = (event: any) => {
			let progress = Math.round(event.lengthComputable ? event.loaded * 100 / event.total : 0);
			this.labelToShow = progress+"%";
			this.progressEvent.emit(event);
		};

	    //this.ajaxObject.upload.addEventListener("progress", this.ajaxObjectListenerProgress.bind(this), false);
	    this.ajaxObject.addEventListener("load", this.ajaxObjectListenerOnLoad.bind(this), false);
	    this.ajaxObject.addEventListener("error", this.ajaxObjectListenerOnError.bind(this), false);
	    this.ajaxObject.addEventListener("abort", this.ajaxObjectListenerOnAbort.bind(this), false);
    }

   	initObjects(){
   		this.ajaxObject = new XMLHttpRequest();
   		this.formData 	= new FormData();
   	}
}


