import { Component, Input, OnInit, OnChanges } from '@angular/core';
import { ToasterService, ToasterConfig, Toast, BodyOutputType } from 'angular2-toaster';

import 'style-loader!angular2-toaster/toaster.css';

@Component({
	selector: 'flash-message',
	styleUrls: ['./flash.component.scss'],
	templateUrl: './flash.component.html',
})

export class FlashComponent implements OnChanges {
	@Input() messages = [];

	config: ToasterConfig = new ToasterConfig({
		positionClass: 'toast-bottom-right',
		timeout: 20000,
		newestOnTop: true,
		tapToDismiss: true,
		preventDuplicates: false,
		animation: 'slideDown',
	});

	constructor(private toasterService: ToasterService) {

	}


	ngOnChanges(changes) {
		changes['messages'].currentValue.subscribe(
			(messages) => {
				for (let i = 0; i < messages.length; i++) {
					if (messages[i].read === false) {
						this.popMessage(messages[i].type, messages[i].message);
						messages[i].read = true;
					}
				}
			},
		);
	}

	popMessage(type = null, body = null, title = null) {
		const toast: Toast = {
			type: type == null ? 'info' : type, // 'default', 'info', 'success', 'warning', 'error';
			title: title == null ? '' : title,
			body: body == null ? '' : body,
			bodyOutputType: BodyOutputType.TrustedHtml,
			showCloseButton: true,
		};

		this.toasterService.popAsync(toast);
	}

}
