import { Component } from '@angular/core';
import { CONSTANTS } from '../../../app.constants';
@Component({
  selector: 'ngx-footer',
  styleUrls: ['./footer.component.scss'],
  template: `
    <span class="created-by">
      Created with ♥ by <a href="https://www.unomaisbrand.com/" target="_blank">UNO+BRAND</a> | Powered by <a href="http://myog.io/" target="_blank">MyOG</a>.
    </span>
    <div class="socials">
      <a href="`+ CONSTANTS.link_youtube +`" target="_blank" class="ion ion-social-youtube"></a>
      <a href="`+ CONSTANTS.link_facebook +`" target="_blank" class="ion ion-social-facebook"></a>
      <a href="`+ CONSTANTS.link_linkedin +`" target="_blank" class="ion ion-social-linkedin"></a>
    </div>
  `,
})
export class FooterComponent {
}
