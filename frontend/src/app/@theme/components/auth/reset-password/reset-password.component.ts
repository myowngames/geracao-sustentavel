/**
 * @license
 * Copyright Akveo. All Rights Reserved.
 * Licensed under the MIT License. See License.txt in the project root for license information.
 */
import { Component, Inject } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { NB_AUTH_OPTIONS_TOKEN } from '@nebular/auth';
import { getDeepFromObject } from '../../../../@core/helpers';

import { NbAuthResult, NbAuthService } from '@nebular/auth';
import { FlashService } from '../../../../@core/utils/flash.service';

@Component({
  selector: 'nb-reset-password-page',
  styleUrls: ['./reset-password.component.scss'],
  template: `
    <nb-auth-block>
      <h2 class="title">Mudar Senha</h2>
      <small class="form-text sub-title">Coloque uma nova senha</small>
      <form (ngSubmit)="resetPass()" #resetPassForm="ngForm">

        <div *ngIf="errors && errors.length > 0 && !submitted" class="alert alert-danger" role="alert">
          <div><strong>Ops!</strong></div>
          <div *ngFor="let error of errors">{{ error }}</div>
        </div>
        <div *ngIf="messages && messages.length > 0 && !submitted" class="alert alert-success" role="alert">
          <div><strong>Oba!</strong></div>
          <div *ngFor="let message of messages">{{ message }}</div>
        </div>

        <div class="form-group">
          <label for="input-password" class="sr-only">Nova Senha</label>
          <input name="password" [(ngModel)]="user.password" type="password" id="input-password"
                 class="form-control form-control-lg first" placeholder="Nova Senha" #password="ngModel"
                 [class.form-control-danger]="password.invalid && password.touched"
                 [required]="getConfigValue('forms.validation.password.required')"
                 [minlength]="getConfigValue('forms.validation.password.minLength')"
                 [maxlength]="getConfigValue('forms.validation.password.maxLength')"
                 autofocus>
          <small class="form-text error" *ngIf="password.invalid && password.touched && password.errors?.required">
            Senha é Obrigatoria
          </small>
          <small
            class="form-text error"
            *ngIf="password.invalid && password.touched && (password.errors?.minlength || password.errors?.maxlength)">
            Senha deve ter
            de {{getConfigValue('forms.validation.password.minLength')}}
            até {{getConfigValue('forms.validation.password.maxLength')}}
            caracteres
          </small>
        </div>

        <div class="form-group">
          <label for="input-re-password" class="sr-only">Confirmação de Senha</label>
          <input
            name="rePass" [(ngModel)]="user.confirmPassword" type="password" id="input-re-password"
            class="form-control form-control-lg last" placeholder="Confirmação de Senha" #rePass="ngModel"
            [class.form-control-danger]="(rePass.invalid || password.value != rePass.value) && rePass.touched"
            [required]="getConfigValue('forms.validation.password.required')">
          <small class="form-text error"
                 *ngIf="rePass.invalid && rePass.touched && rePass.errors?.required">
            Confirmação de Senha Requerida
          </small>
          <small
            class="form-text error"
            *ngIf="rePass.touched && password.value != rePass.value && !rePass.errors?.required">
            Senha e Confirmação não são a mesma.
          </small>
        </div>

        <button [disabled]="submitted || !resetPassForm.form.valid" class="btn btn-hero-success btn-block"
                [class.btn-pulse]="submitted">
          
          <span *ngIf="!submitted">Trocar Senha</span><span *ngIf="submitted">Carregando... <i class="fa fa-spinner fa-pulse"></i></span>
        </button>
      </form>

      <div class="links col-sm-12">
        <small class="form-text">
          Já tem uma conta? <a routerLink="../login"><strong>Entrar</strong></a>
        </small>
        <small class="form-text">
          <a routerLink="/inscricoes"><strong>Registrar</strong></a>
        </small>
      </div>
    </nb-auth-block>
  `,
})
export class NbResetPasswordComponent {

    redirectDelay: number = 0;
    showMessages: any = {};
    provider: string = '';

    submitted = false;
    errors: string[] = [];
    messages: string[] = [];
    user: any = {};

    token: string = '';

    constructor(
        protected service: NbAuthService,
        @Inject(NB_AUTH_OPTIONS_TOKEN) protected config = {},
        protected router: Router,
        private activatedRoute: ActivatedRoute,
        private flashService: FlashService
    ){
        this.token = this.user.id = this.activatedRoute.params['value'].token;
        this.redirectDelay = this.getConfigValue('forms.resetPassword.redirectDelay');
        this.showMessages = this.getConfigValue('forms.resetPassword.showMessages');
        this.provider = this.getConfigValue('forms.resetPassword.provider');
  }

    resetPass(): void {
        this.errors = this.messages = [];
        this.submitted = true;

        this.service.resetPassword(this.provider, this.user).subscribe((result: NbAuthResult) => {
            this.submitted = false;
            if (result.isSuccess()) {
                this.messages = result.getMessages();
                this.flashService.push('success',"Your password has been reset successfully, You can now Log in with your username and password.");
            } else {
                this.errors = result.getErrors();
            }

            const redirect = result.getRedirect();

            if (redirect) {
                setTimeout(() => {
                    return this.router.navigateByUrl(redirect);
                }, this.redirectDelay);
            }
        });
    }

    getConfigValue(key: string): any {
        return getDeepFromObject(this.config, key, null);
    }
}
