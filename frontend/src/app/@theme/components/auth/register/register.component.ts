import { Component, Inject } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators, FormControl, FormArray } from '@angular/forms';
import { Observable } from 'rxjs/Observable';
import { UserService } from '../../../../@core/data/user.service';

import { NbAuthService, NbAuthResult } from '@nebular/auth';

import { CONSTANTS } from '../../../../app.constants';
import { getDeepFromObject } from '../../../../@core/helpers';
import { NB_AUTH_OPTIONS_TOKEN } from '@nebular/auth';
@Component({
    selector: 'nb-register',
    styleUrls: ['./register.component.scss'],
    templateUrl: './register.component.html',
})

export class RegisterComponent {
    public form;
    redirectDelay: number = 0;
    error: string = '';
    privacy_link: string = '';
    submitted: boolean = false;
    constructor(
        private router: Router,
        private fb: FormBuilder,
        private userService: UserService,
        private authService: NbAuthService,
        @Inject(NB_AUTH_OPTIONS_TOKEN) protected config = {},
    ) {
        this.redirectDelay = this.getConfigValue('forms.logout.redirectDelay');
    }

    ngOnInit(){
        this.privacy_link = CONSTANTS.botdoc_privacy_policy;
        this.form = this.fb.group({
            first_name: [
                '',
                Validators.compose([
                  Validators.required, 
                  Validators.minLength(4),
                ])
            ],
            last_name: [
                '',
                Validators.compose([
                  Validators.required, 
                  Validators.minLength(4),
                 ])
            ],
            email: [
                '',
                Validators.compose([
                  Validators.required,
                ])
            ],
            password: [
                '',
                Validators.compose([
                  Validators.required,
                  Validators.minLength(8),
                ])
            ],
            confirm_password: [
               '',
                Validators.compose([
                  Validators.required,
                  Validators.minLength(8)
                ])
            ],
            terms: [
                false,
                Validators.compose([
                  Validators.requiredTrue,
                ])
            ],
        });
    }

    onSubmit(): void {
        let data = this.form.value;
        this.submitted = true;
        //this.userService.signupUser(data).subscribe(
        //    (response) => {
        //        let user = {
        //            email: data.email,
        //            password: data.password
        //        }
//
        //        this.authService.authenticate('email', user).subscribe((result: NbAuthResult) => {
        //            this.submitted = false;
        //            if (result.isSuccess()) {
        //                setTimeout(() => {
        //                  return this.router.navigate(['/dashboard']);
        //                }, this.redirectDelay);
        //            }
        //            else {
        //                console.log(result.getErrors());
        //            }
        //        });
        //    },
        //    (err) => {
        //        this.submitted = false;
        //        this.error = '';
        //        for(let indexName in err.error){
        //            this.error += err.error[indexName];
        //        }
        //        return false;
        //    }
        //);
    }
    getConfigValue(key: string): any {
        return getDeepFromObject(this.config, key, null);
    }

}