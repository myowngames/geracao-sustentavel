/**
 * @license
 * Copyright Akveo. All Rights Reserved.
 * Licensed under the MIT License. See License.txt in the project root for license information.
 */
import { Component, Inject } from '@angular/core';
import { Router } from '@angular/router';
import { NB_AUTH_OPTIONS_TOKEN } from '@nebular/auth';
import { getDeepFromObject } from '../../../../@core/helpers';

import { NbAuthResult, NbAuthService } from '@nebular/auth';
import { FlashService } from '../../../../@core/utils/flash.service';

@Component({
  selector: 'nb-request-password-page',
  styleUrls: ['./request-password.component.scss'],
  template: `
    <nb-auth-block>
      <h2 class="title">Esqueci a Senha</h2>
      <small class="form-text sub-title">Coloque seu e-mail que enviaremos um link para trocar a senha!</small>
      <form (ngSubmit)="requestPass()" #requestPassForm="ngForm">

        <div *ngIf="showMessages.error && errors && errors.length > 0 && !submitted"
             class="alert alert-danger" role="alert">
          <div><strong>Ops!</strong></div>
          <div *ngFor="let error of errors">{{ error }}</div>
        </div>
        <div *ngIf="showMessages.success && messages && messages.length > 0 && !submitted"
             class="alert alert-success" role="alert">
          <div><strong>Oba!</strong></div>
          <div *ngFor="let message of messages">{{ message }}</div>
        </div>

        <div class="form-group">
          <label for="input-email" class="sr-only">Endereço de Email</label>
          <input name="email" [(ngModel)]="user.email" id="input-email" #email="ngModel"
                 class="form-control" placeholder="Endereço de Email" pattern=".+@.+\..+"
                 [class.form-control-danger]="email.invalid && email.touched"
                 [required]="getConfigValue('forms.validation.email.required')"
                 autofocus>
          <small class="form-text error" *ngIf="email.invalid && email.touched && email.errors?.required">
            Email Invalido
          </small>
          <small class="form-text error"
                 *ngIf="email.invalid && email.touched && email.errors?.pattern">
            Email Invalido
          </small>
        </div>

        <button [disabled]="submitted || !requestPassForm.form.valid" class="btn btn-hero-success btn-block"
                [class.btn-pulse]="submitted">
          
          <span *ngIf="!submitted">Enviar</span><span *ngIf="submitted">Carregando... <i class="fa fa-spinner fa-pulse"></i></span>
        </button>
      </form>

      <div class="links col-sm-12">
        <small class="form-text">
          Já tem uma conta? <a routerLink="../login"><strong>Entrar</strong></a>
        </small>
        <small class="form-text">
          <a routerLink="/inscricoes"><strong>Registrar</strong></a>
        </small>
      </div>
    </nb-auth-block>
  `,
})
export class NbRequestPasswordComponent {

    redirectDelay: number = 0;
    showMessages: any = {};
    provider: string = '';

    submitted = false;
    errors: string[] = [];
    messages: string[] = [];
    user: any = {};

    constructor(
        protected service: NbAuthService,
        @Inject(NB_AUTH_OPTIONS_TOKEN) protected config = {},
        protected router: Router,
        protected flashService: FlashService
    ) {
        this.redirectDelay = this.getConfigValue('forms.requestPassword.redirectDelay');
        this.showMessages = this.getConfigValue('forms.requestPassword.showMessages');
        this.provider = this.getConfigValue('forms.requestPassword.provider');
    }

    requestPass(): void {
        this.errors = this.messages = [];
        this.submitted = true;

        this.service.requestPassword(this.provider, this.user).subscribe((result: NbAuthResult) => {
            this.submitted = false;
            if (result.isSuccess()) {
                this.messages = result.getMessages();
                this.flashService.push('warning',this.messages);

                const redirect = result.getRedirect();
                if (redirect) {
                    setTimeout(() => {
                        return this.router.navigateByUrl(redirect);
                    }, this.redirectDelay);
                }
            }
            else {
                this.errors = result.getErrors();
                const redirect = result.getRedirect();
                return this.router.navigateByUrl('/auth/request-password');
                
            }
        });
    }

    getConfigValue(key: string): any {
        return getDeepFromObject(this.config, key, null);
    }
}
