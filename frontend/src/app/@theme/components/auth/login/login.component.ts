/**
 * @license
 * Copyright Akveo. All Rights Reserved.
 * Licensed under the MIT License. See License.txt in the project root for license information.
 */
import { Component, Inject } from '@angular/core';
import { Router } from '@angular/router';
import { NB_AUTH_OPTIONS_TOKEN } from '@nebular/auth';
import { getDeepFromObject } from '../../../../@core/helpers';

import { NbAuthResult, NbAuthService } from '@nebular/auth';

import { CONSTANTS } from '../../../../app.constants';

@Component({
  selector: 'nb-login',
  template: `
    <nb-auth-block>
      <h2 class="title">Entrar</h2>
      <small class="form-text sub-title">Olá! Entre com seu usuario e senha</small>

      <form (ngSubmit)="login()" #form="ngForm" autocomplete="nope">

        <div *ngIf="showMessages.error && errors && errors.length > 0 && !submitted"
             class="alert alert-danger" role="alert">
          <div><strong>Ops!</strong></div>
          <div *ngFor="let error of errors">{{ error }}</div>
        </div>

        <div *ngIf="showMessages.success && messages && messages.length > 0 && !submitted"
             class="alert alert-success" role="alert">
          <div><strong>Oba!</strong></div>
          <div *ngFor="let message of messages">{{ message }}</div>
        </div>

        <div class="form-group">
          <label for="input-username" class="sr-only">Log in</label>
          <input name="username" [(ngModel)]="user.username" id="input-username"
                 class="form-control" placeholder="Usuario ou Email" #username="ngModel"
                 [class.form-control-danger]="username.invalid && username.touched" autofocus
                 [required]="getConfigValue('forms.validation.username.required')">
             
          <small class="form-text error" *ngIf="username.invalid && username.touched && username.errors?.required">
            Usuario é obrigatório
          </small>
        </div>

        <div class="form-group">
          <label for="input-password" class="sr-only">Senha</label>
          <input name="password" [(ngModel)]="user.password" type="password" id="input-password"
                 class="form-control" placeholder="Senha" #password="ngModel"
                 [class.form-control-danger]="password.invalid && password.touched"
                 [required]="getConfigValue('forms.validation.password.required')"
                 [minlength]="getConfigValue('forms.validation.password.minLength')"
                 [maxlength]="getConfigValue('forms.validation.password.maxLength')">
          <small class="form-text error" *ngIf="password.invalid && password.touched && password.errors?.required">
            Senha é Obrigatoria
          </small>
          <small
            class="form-text error"
            *ngIf="password.invalid && password.touched && (password.errors?.minlength || password.errors?.maxlength)">
            Password should contains
            from {{ getConfigValue('forms.validation.password.minLength') }}
            to {{ getConfigValue('forms.validation.password.maxLength') }}
            characters
          </small>
        </div>

        <div class="form-group accept-group col-sm-12">
          <nb-checkbox name="rememberMe" [(ngModel)]="user.rememberMe">Lembrar-me</nb-checkbox>
          <a class="forgot-password" routerLink="../request-password">Esqueceu a Senha?</a>
        </div>

        <button [disabled]="submitted || !form.valid" class="btn btn-block btn-hero-success"
                [class.btn-pulse]="submitted">
          <span *ngIf="!submitted">Entrar</span><span *ngIf="submitted">Carregando... <i class="fa fa-spinner fa-pulse"></i></span>
        </button>
      </form>

      <div class="links">

        <small class="form-text">
          Não tem uma conta? <a routerLink="/inscricoes"><strong>Registrar</strong></a>
        </small>

        
        <div class="socials">
          <a href="`+ CONSTANTS.link_youtube +`" target="_blank" class="ion ion-social-youtube"></a>
          <a href="`+ CONSTANTS.link_facebook +`" target="_blank" class="ion ion-social-facebook"></a>
          <a href="`+ CONSTANTS.link_linkedin +`" target="_blank" class="ion ion-social-linkedin"></a>
        </div>

        
      </div>
    </nb-auth-block>
  `,
})
export class NbLoginComponent {

  redirectDelay: number = 0;
  showMessages: any = {};
  provider: string = '';

  errors: string[] = [];
  messages: string[] = [];
  user: any = {};
  submitted: boolean = false;

  constructor(protected service: NbAuthService,
              @Inject(NB_AUTH_OPTIONS_TOKEN) protected config = {},
              protected router: Router) {

    this.redirectDelay = this.getConfigValue('forms.login.redirectDelay');
    this.showMessages = this.getConfigValue('forms.login.showMessages');
    this.provider = this.getConfigValue('forms.login.provider');
  }

  login(): void {
    this.errors = this.messages = [];
    this.submitted = true;

    this.service.authenticate(this.provider, this.user).subscribe((result: NbAuthResult) => {
      this.submitted = false;

      if (result.isSuccess()) {
        this.messages = result.getMessages();
      } else {
        this.errors = result.getErrors();
      }

      const redirect = result.getRedirect();
      if (redirect) {
        setTimeout(() => {
          return this.router.navigateByUrl(redirect);
        }, this.redirectDelay);
      }
    });
  }

  getConfigValue(key: string): any {
    return getDeepFromObject(this.config, key, null);
  }
}
