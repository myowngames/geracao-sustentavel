import { ModuleWithProviders, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

import { RouterModule, Routes } from '@angular/router';

import { NbAuthComponent } from './components/auth/auth.component';
import { NbAuthBlockComponent } from './components/auth/auth-block/auth-block.component';
import { NbLoginComponent } from './components/auth/login/login.component';
import { RegisterComponent } from './components/auth/register/register.component';
import { NbLogoutComponent } from './components/auth/logout/logout.component';
import { NbRequestPasswordComponent } from './components/auth/request-password/request-password.component';
import { NbResetPasswordComponent } from './components/auth/reset-password/reset-password.component';
import { Ng2SmartTableModule } from 'ng2-smart-table';


import {
  NbActionsModule,
  NbCardModule,
  NbLayoutModule,
  NbMenuModule,
  NbRouteTabsetModule,
  NbSearchModule,
  NbSidebarModule,
  NbTabsetModule,
  NbThemeModule,
  NbUserModule,
  NbCheckboxModule,
} from '@nebular/theme';

import {
  FooterComponent,
  HeaderComponent,
  SearchInputComponent,
  ThemeSettingsComponent,
  TinyMCEComponent,
} from './components';
import { CapitalizePipe, PluralPipe, RoundPipe, TimingPipe } from './pipes';
import {
  OneColumnLayoutComponent,
  SampleLayoutComponent,
  ThreeColumnsLayoutComponent,
  TwoColumnsLayoutComponent,
} from './layouts';
import { DEFAULT_THEME } from './styles/theme.default';
import { COSMIC_THEME } from './styles/theme.cosmic';

import { ButtonUploaderComponent } from './components/uploader/button-uploader/uploader.component';
import { MaterializeColorpickerComponent } from'./components/materialize-colorpicker/materialize-colorpicker.component';
import { BooleanRenderComponent, DateRenderComponent, ImageRenderComponent } from './components/smart-table-render'
import { FlashComponent } from './components/flash/flash.component';
import { ToasterModule } from 'angular2-toaster';

const BASE_MODULES = [CommonModule, FormsModule, ReactiveFormsModule];

const NB_MODULES = [
    NbCardModule,
    NbLayoutModule,
    NbTabsetModule,
    NbRouteTabsetModule,
    NbMenuModule,
    NbUserModule,
    NbActionsModule,
    NbSearchModule,
    NbSidebarModule,
    NbCheckboxModule,
    NgbModule,
    RouterModule,
    ToasterModule,
    Ng2SmartTableModule
];

const COMPONENTS = [
    HeaderComponent,
    FooterComponent,
    SearchInputComponent,
    ThemeSettingsComponent,
    TinyMCEComponent,
    OneColumnLayoutComponent,
    SampleLayoutComponent,
    ThreeColumnsLayoutComponent,
    TwoColumnsLayoutComponent,
    ButtonUploaderComponent,
    MaterializeColorpickerComponent,
    NbAuthComponent,
    NbAuthBlockComponent,
    NbLoginComponent,
    RegisterComponent,
    NbLogoutComponent,
    NbRequestPasswordComponent,
    NbResetPasswordComponent,
    FlashComponent,
    // Smart Table render components, also have to go to ENTRY_COMPONENTS
    BooleanRenderComponent,
    DateRenderComponent,
    ImageRenderComponent
];
const ENTRY_COMPONENTS = [
    BooleanRenderComponent,
    DateRenderComponent,
    ImageRenderComponent
];
const PIPES = [
  CapitalizePipe,
  PluralPipe,
  RoundPipe,
  TimingPipe,
];

const NB_THEME_PROVIDERS = [
  ...NbThemeModule.forRoot(
    {
      name: 'default',
    },
    [ DEFAULT_THEME, COSMIC_THEME ],
  ).providers,
  ...NbSidebarModule.forRoot().providers,
  ...NbMenuModule.forRoot().providers,
];

@NgModule({
  imports: [...BASE_MODULES, ...NB_MODULES],
  exports: [...BASE_MODULES, ...NB_MODULES, ...COMPONENTS, ...PIPES],
  declarations: [...COMPONENTS, ...PIPES],
  entryComponents: [ENTRY_COMPONENTS ]
})
export class ThemeModule {
  static forRoot(): ModuleWithProviders {
    return <ModuleWithProviders>{
      ngModule: ThemeModule,
      providers: [...NB_THEME_PROVIDERS],
    };
  }
}
