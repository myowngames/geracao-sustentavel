import { Directive, ElementRef, Renderer2, Input, OnInit, HostListener, SimpleChanges } from '@angular/core';


@Directive({
	selector: '[formValidation]'
})

export class FormValidationCheckerDirective implements OnInit {

	@Input() formValidation; //form group
	@Input() inputErrorClass = "form-control-danger"; //class which we attach to inputs in case of errors

	validationMessages = [];
	submitBtn;

	constructor(private el: ElementRef, private renderer: Renderer2) {

	}

	ngOnInit() {
		for (let i = 0; i < this.el.nativeElement.querySelectorAll('*[validationMessages]').length; i++) {
			let item = {};

			item['messagesContainer'] = this.el.nativeElement.querySelectorAll('[validationMessages]')[i];
			item['input'] = this.el.nativeElement.querySelector('*[name="' + item['messagesContainer'].attributes.validationMessages.nodeValue + '"]');
			item['messages'] = item['messagesContainer'].querySelectorAll('[validationType]');

			item['input'].addEventListener('blur', (evt) => {
				this.updateStatusMessageItem(item);
			});

			this.renderer.setStyle(item['messagesContainer'], "display", "none");
			this.validationMessages.push(item);
		}

		this.submitBtn = this.el.nativeElement.querySelector('[type="submit"]');

		if (this.submitBtn == null) {
			//if submit is not there we grab the first button in the page
			this.el.nativeElement.querySelector('button');
		}

		this.subscribeToFormControlStatusChanges();


		this.formValidation.statusChanges.subscribe(
			(result) => {
				if (this.submitBtn) {
					if (result.toLowerCase() == 'invalid') {
						this.submitBtn.disabled = true;
					}
					else {
						this.submitBtn.disabled = false;
					}
				}

			}
		);
	}


	subscribeToFormControlStatusChanges() {
		for (let i = 0; i < this.validationMessages.length; i++) {
			this.formValidation.get(this.validationMessages[i]['input'].name).statusChanges.subscribe(
				(result) => {
					this.updateStatusMessageItem(this.validationMessages[i]);
				}
			);
		}
	}


	updateStatusMessageItem(item) {
		if (this.formValidation.get(item['input'].name).invalid && this.formValidation.get(item['input'].name).touched) {
			this.renderer.addClass(item['input'], this.inputErrorClass);
			this.renderer.setStyle(item['messagesContainer'], "display", "initial");

			for (let i = 0; i < item['messages'].length; i++) {
				if (this.formValidation.get(item['input'].name).hasError(item['messages'][i].attributes.validationType.nodeValue)) {
					this.renderer.setStyle(item['messages'][i], "display", "initial");
				}
				else {
					this.renderer.setStyle(item['messages'][i], "display", "none");
				}
			}
		}
		else {
			this.renderer.removeClass(item['input'], this.inputErrorClass);
			this.renderer.setStyle(item['messagesContainer'], "display", "none");
		}
	}

}
