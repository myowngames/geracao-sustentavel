
import { FormBuilder, FormGroup, Validators, FormControl, FormArray } from '@angular/forms';
import * as moment from 'moment';
export function ValidateDate(input: FormControl) {
	if (input.value) {
		let datetext: string = input.value;
		datetext = datetext.replace(/_/g, '');
		if (datetext.length < 10) {
			return { 'minlength': true  };
		} else {
			const dateExploded = input.value.split('/');
			const date = moment(new Date(dateExploded[2], dateExploded[1], dateExploded[0]));
			return (date.isValid() && moment().isAfter(date)) ? null : { 'minlength': true };
		}
	} else {
		return { 'minlength': true  };
	}
}
