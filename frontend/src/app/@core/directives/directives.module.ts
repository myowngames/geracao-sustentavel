import { NgModule } from '@angular/core';
import { FormValidationCheckerDirective } from './FormValidationChecker.directive';

@NgModule({
  imports: [],
  declarations: [FormValidationCheckerDirective],
  exports: [FormValidationCheckerDirective]
})
export class DirectivesModule { }