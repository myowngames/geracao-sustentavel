import { Component, OnDestroy } from '@angular/core';
import { ISubscription } from "rxjs/Subscription";

export class SubscriptionComponent implements OnDestroy{
    private subscriptions: ISubscription[] = [];


    pushSub(subscription: ISubscription){
        this.subscriptions.push(subscription);
    }

    ngOnDestroy(){
        for(let i=0; i < this.subscriptions.length; i++){
            if (this.subscriptions[i]){
                this.subscriptions[i].unsubscribe();
            }
        }

        this.subscriptions = [];
    }
}