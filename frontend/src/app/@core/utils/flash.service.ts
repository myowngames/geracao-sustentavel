import { Injectable } from '@angular/core';

import { Observable } from 'rxjs/Observable';

@Injectable()
export class FlashService  {

	private messageStack = [];

	getAll(): Observable<any> {
		this.cleanReadMessagesArr();
		return Observable.of(this.messageStack);
	}

	push(type, message): void {
		this.messageStack.push({
			message: message,
			type: type,
			read: false,
		});
	}

	cleanReadMessagesArr() {
		const newStack = [];
		for (let i = 0; i < this.messageStack.length; i++) {
			if (this.messageStack[i].read === false) {
				newStack.push(this.messageStack[i]);
			}
		}
		this.messageStack = newStack;
	}
}
