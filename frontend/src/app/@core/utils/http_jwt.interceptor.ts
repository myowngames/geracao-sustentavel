import { Injectable, Injector } from '@angular/core';
import { HttpEvent, HttpHandler, HttpInterceptor, HttpRequest, HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';

import { environment } from '../../../environments/environment';

import { UserService } from '../../@core/data/user.service';
import { FlashService } from '../../@core/utils/flash.service';

import 'rxjs/add/operator/switchMap';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';
import { switchMap } from 'rxjs/operators/switchMap';

import { NbAuthService, NbAuthJWTToken, NbTokenService } from '@nebular/auth';
import { Router } from '@angular/router';


@Injectable()
export class HttpJwtInterceptor implements HttpInterceptor {
	private public_links = [
		{ url: '/auth/token_auth/', method: 'POST' },
		{ url: '/auth/token_refresh/', method: 'POST' },
		{ url: '/city/', method: 'GET' },
		{ url: '/signup/create_gang/', method: 'POST' },
		{ url: '/signup/create_user/', method: 'POST' },
		{ url: '/frontendfile/', method: 'GET' },
		{ url: '/signup/register/', method: 'POST'},
	];

	private checkIfRequestIsPublic(req: HttpRequest<any>) {
		const url = req.url.split('?')[0];
		const method = req.method;
		for (let i = 0; i < this.public_links.length; i++) {
			if (url === this.public_links[i].url && method === this.public_links[i].method) {
				return true;
			}
		}
		return false;
	}
	constructor(private injector: Injector) {
	}

	intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
		return this.authService.getToken().pipe(switchMap((token: NbAuthJWTToken) => {
			if (req.url === '/auth/logout/') {
				window.location.href = '/';
				req = req.clone({
					url: '/',
				});
				this.logout();
				return next.handle(req);
			}
			if (!this.checkIfRequestIsPublic(req)) {
				if (token.getValue() != null) {
					// Check if token
					// Compare the Token, if the Rember me is checked renew, otherwise log-out
					const now = new Date();
					const exp = token.getTokenExpDate();

					const dif_seconds = (exp.getTime() - now.getTime()) / 1000;
					if (now >= exp) {
						// Token has expired
						// TODO:If Remember is is Checked we update the token
						this.logout();
					} else if (dif_seconds <= 3600) {
						let request;
						// Refresh Token
						this.getUserService.refreshToken(token.getValue()).subscribe(
							(result) => {
								// Talvez de problema por que tem que ser promessa e o token não sabemos se vai atualizar na aplicacao toda
								token.setValue(result.token);
								request = this.cloneAndSend(req, next, token);
							},
							(err) => {
								// Request got an error, logout user
								this.logout();
							},
						);
						return Observable.of(request);
					} else {
						return this.cloneAndSend(req, next, token);
					}
				} else {
					this.logout();
				}
			}

			return this.cloneAndSend(req, next, null);
		}),
		);
	}

	protected cloneAndSend(req: HttpRequest<any>, next: HttpHandler, token: NbAuthJWTToken, opts = {}): Observable<HttpEvent<any>> {
		const headers = {
			'Content-Type': 'application/json',
		};
		if (req.headers.get('Content-Type')) {
			headers['Content-Type'] = undefined;
		}
		if (token != null) {
			headers['Authorization'] = `JWT ${token.getValue()}`;
		}

		// define url
		let url = environment.api_endpoint;
		const suffix = req.url;

		if (suffix.search('http://') === -1 && suffix.search('https://') === -1) {
			const parts = suffix.split('');
			if (parts[0] === '/') {
				parts.shift();
			}

			const imploded = parts.join('');

			url += imploded;
		}

		req = req.clone({
			setHeaders: headers,
			url: url,
		});

		return next.handle(req)
			.do(
				(ev: HttpEvent<any>) => {
					/*
                        Don't have anything to do here, we just want to track
                        the errors, in order to do that we need to call the
                        .catch in the response. This block has the response
                        of the http request if everything went well
                    */
				},
			)
			.catch(
				(response) => {

					if (response['error']) {
						if (response['statusText'] === 'Unknown Error') {
							console.error('Unkown ERROR', response);
						} else if (typeof response['error'] === 'string') {
							this.flashService.push('error', response['error']);
						} else if (typeof response['error'] === 'object') {
							const errorSttring = this.getRecursiveErrors(response['error']);
							this.flashService.push('error', errorSttring);
						} else {
							this.flashService.push('error', response['message']);
						}
					}
					return Observable.throw(response);
				},
			);
	}

	public getRecursiveErrors(error): string {
		let errorString = '';
		if (typeof error === 'string') {
			errorString += error;
		} else {
			for (const i in error) {
				if (error[i]) {
					if (error[i].value) {
						errorString += error[i].value + ' ';
					} else {
						if (!isNaN(parseInt(i))) {
							errorString += this.getRecursiveErrors(error[i]);
						} else {
							errorString += this.getRecursiveErrors(error[i]) + '<br/>';
						}
					}
				}
			}
		}
		return errorString;
	}

	protected get getRouter(): Router {
		return this.injector.get(Router);
	}

	protected get getUserService(): UserService {
		return this.injector.get(UserService);
	}

	protected get getTokenService(): NbTokenService {
		return this.injector.get(NbTokenService);
	}

	protected get authService(): NbAuthService {
		return this.injector.get(NbAuthService);
	}

	protected get flashService(): FlashService {
		return this.injector.get(FlashService);
	}

	public logout() {
		this.getTokenService.clear();
		this.getRouter.navigate(['auth/login']);
	}
}
