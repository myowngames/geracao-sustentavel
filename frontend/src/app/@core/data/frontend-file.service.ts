import { Injectable, Injector } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { HttpClient } from '@angular/common/http';
import { NbAuthJWTToken, NbTokenService } from '@nebular/auth';

import 'rxjs/add/observable/of';

import { AbstractService } from './abstract.service';

@Injectable()
export class FrontEndFileService extends AbstractService{

    public links = {
        paginate: '/frontendfile/',
        listEntries: '/frontendfile/',
        retrieveSingleEntry: '/frontendfile/#id#/',
        updateEntry: '/frontendfile/#id#/',
        createEntry: '/frontendfile/',
        deleteEntry: '/frontendfile/#id#/',
        counter: '/frontendfile/counter/',
    }

}
