import { NgModule, ModuleWithProviders } from '@angular/core';
import { CommonModule } from '@angular/common';

import { StateService } from './state.service';
import { UserService } from './user.service';
import { CityService } from './city.service';
import { DeliverableService } from './deliverable.service';
import { FrontEndFileService } from './frontend-file.service';
import { GangPhaseDeliverableVoteService } from './gang-phase-deliverable-vote.service';
import { GangPhaseDeliverableService } from './gang-phase-deliverable.service';
import { GangPhaseService } from './gang-phase.service';
import { GangService } from './gang.service';
import { NotificationService } from './notification.service';
import { NotificationContentService } from './notification-content.service';
import { PhaseService } from './phase.service';
import { GroupService } from './group.service';
import { PermissionService } from './permission.service';
import { DeliverableFileService } from './deliverable-file.service';
import { GangPhaseFilteredService } from './gang-phase-filtered.service';

const SERVICES = [
	UserService,
	StateService,
	CityService,
	DeliverableService,
	FrontEndFileService,
	GangPhaseDeliverableVoteService,
	GangPhaseDeliverableService,
	GangPhaseService,
	GangService,
	NotificationService,
	NotificationContentService,
	PhaseService,
	GroupService,
	PermissionService,
	DeliverableFileService,
	GangPhaseFilteredService,
];

@NgModule({
	imports: [
		CommonModule,
	],
	providers: [
		...SERVICES,
	],
})
export class DataModule {
	static forRoot(): ModuleWithProviders {
		return <ModuleWithProviders>{
			ngModule: DataModule,
			providers: [
				...SERVICES,
			],
		};
	}
}
