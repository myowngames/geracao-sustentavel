import { Injectable, Injector } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { HttpClient } from '@angular/common/http';
import { NbAuthJWTToken, NbTokenService } from '@nebular/auth';

import 'rxjs/add/observable/of';

import { AbstractService } from './abstract.service';

@Injectable()
export class DeliverableService extends AbstractService{

    public links = {
        paginate: '/deliverable/',
        listEntries: null,
        retrieveSingleEntry: '/deliverable/#id#/',
        updateEntry: '/deliverable/#id#/',
        createEntry: '/deliverable/',
        deleteEntry: '/deliverable/#id#/',
        counter: '/deliverable/counter/',
    }


    getAllowedContentTypes(): Observable<any>{
    	return this.get('/deliverable/get_allowed_content_types/');
    }
}
