import { Injectable, Injector } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { HttpClient } from '@angular/common/http';
import { NbAuthJWTToken, NbTokenService } from '@nebular/auth';

import 'rxjs/add/observable/of';

import { AbstractService } from './abstract.service';

@Injectable()
export class GangPhaseDeliverableService extends AbstractService{

    public links = {
        paginate: '/gangphasedeliverable/',
        listEntries: null,
        retrieveSingleEntry: '/gangphasedeliverable/#id#/',
        updateEntry: '/gangphasedeliverable/#id#/',
        createEntry: '/gangphasedeliverable/',
        deleteEntry: '/gangphasedeliverable/#id#/',
        counter: '/gangphasedeliverable/counter/',
    }


    getAllVideos(): Observable<any>{
    	return this.get('/gangphasedeliverable/get_all_videos/');
    }

}
