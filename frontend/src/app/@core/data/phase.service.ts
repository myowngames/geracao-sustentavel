import { Injectable, Injector } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { HttpClient } from '@angular/common/http';
import { NbAuthJWTToken, NbTokenService } from '@nebular/auth';

import 'rxjs/add/observable/of';

import { AbstractService } from './abstract.service';
import * as moment from 'moment';
@Injectable()
export class PhaseService extends AbstractService{
    public links = {
        paginate: '/phase/',
        listEntries: '/phase/',
        retrieveSingleEntry: '/phase/#id#/',
        updateEntry: '/phase/#id#/',
        createEntry: '/phase/',
        deleteEntry: '/phase/#id#/',
        counter: '/phase/counter/',
    }
    public getStatus(start:number,end:number):string{
      let today = new Date(Date.now()).getTime();
    	if(today <= end && today >= start){
            return "Ativo";
		}else if(today > end){
            return "Expirado";
		}else{
            return "Aguardando"
		}
    }

    public getDeliverable(id){
        return this.get('/phase/' + id + '/get_deliverable/');
    }
}
