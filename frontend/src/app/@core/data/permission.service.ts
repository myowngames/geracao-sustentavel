import { Injectable, Injector } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { HttpClient } from '@angular/common/http';
import { NbAuthJWTToken, NbTokenService } from '@nebular/auth';

import 'rxjs/add/observable/of';

import { AbstractService } from './abstract.service';

@Injectable()
export class PermissionService extends AbstractService{

    public links = {
        paginate: '/permission/',
        listEntries: null,
        retrieveSingleEntry: '/permission/#id#/',
        updateEntry: '/permission/#id#/',
        createEntry: '/permission/',
        deleteEntry: '/permission/#id#/',
        counter: '/permission/counter/',
    }
}
