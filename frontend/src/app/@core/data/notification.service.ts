import { Injectable, Injector } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { HttpClient } from '@angular/common/http';
import { NbAuthJWTToken, NbTokenService } from '@nebular/auth';

import { AbstractService } from './abstract.service';

@Injectable()
export class NotificationService extends AbstractService{

    public links = {
      paginate: '/notification/',
      listEntries: null,
      retrieveSingleEntry: '/notification/#id#/',
      updateEntry: '/notification/#id#/',
      createEntry: '/notification/',
      deleteEntry: '/notification/#id#/',
      counter: '/notification/counter/',
      getUserNotifications: '/notification/get_user_notification/',
      getNewNotificationCount: '/notification/get_user_new_notification_count/'
    }

    public getNewNotificationCount(options = {}): Observable<any> {
      return this.get(this.links['getNewNotificationCount'],options);
    }
    public getUserNotification(options = {}): Observable<any> {
      return this.get(this.links['getUserNotifications'], options);
    }
    protected get getTokenService(): NbTokenService {
        return this.injector.get(NbTokenService);
    }
}
