import { Injectable, Injector } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { HttpClient } from '@angular/common/http';
import { NbAuthJWTToken, NbTokenService } from '@nebular/auth';

import 'rxjs/add/observable/of';

import { AbstractService } from './abstract.service';

@Injectable()
export class GroupService extends AbstractService{

    public links = {
        paginate: '/group/',
        listEntries: null,
        retrieveSingleEntry: '/group/#id#/',
        updateEntry: '/group/#id#/',
        createEntry: '/group/#id#/',
        deleteEntry: '/group/#id#/',
        counter: '/group/counter/',
        register: '/group/register/',
    }

    public register(data:any = []): Observable<any>{
      return this.post(this.links.register,data).map(
        (response) => {
          return response;
        }
      )
    }
    public setPermissions(groupId=null, permissionIds=[]): Observable<any>{
      return this.put('/group/' + groupId + '/set_permissions/', {permissions: permissionIds}).map(
        (response) => {
          return response;
        }
      );
    }
}
