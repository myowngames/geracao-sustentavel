import { Injectable } from '@angular/core';
import { LocalDataSource } from 'ng2-smart-table';

import { RequestOptionsArgs } from '@angular/http/src/interfaces';
import { URLSearchParams } from '@angular/http';
import { Observable } from 'rxjs';

import 'rxjs/add/operator/toPromise';

export class SmartTableRemoteDataSource extends LocalDataSource {
	protected serviceToUse;
	protected lastRequestCount: number = 0;
	protected conf = {
		dataKey: 'results',
		totalKey: 'count',
		pagerPageKey: 'page',
		pagerLimitKey: 'page_size',
		sortFieldKey: 'ordering',
		sortDirKey: 'sort',
		filterFieldKey: '#field#__icontains',
	}

	constructor(serviceToUse = null) {
		super();
		this.serviceToUse = serviceToUse;
	}

	count(): number {
		return this.lastRequestCount;
	}

	getElements(): Promise<any> {
		return this.requestElements().map(res => {
			this.lastRequestCount = this.extractTotalFromResponse(res);
			this.data = this.extractDataFromResponse(res);
			return this.data;
		}).toPromise();
	}

	protected extractDataFromResponse(res: any): Array<any> {
		const data = res[this.conf.dataKey];
    if (data instanceof Array) {
			return data;
		}
		
		throw new Error(`Data must be an array. Please check that data extracted from the server response by the key '${this.conf.dataKey}' exists and is array.`);
	}

	protected extractTotalFromResponse(res: any): number {
		return res[this.conf.totalKey];
	}

	protected requestElements(): Observable<any> {
		this.addPagerRequestOptions();
		return this.serviceToUse.getListPaginated(this.createRequestOptions());
	}

	protected createRequestOptions(): any {
		let requestOptions: any = [];

		requestOptions = this.addSortRequestOptions(requestOptions);
		requestOptions = this.addFilterRequestOptions(requestOptions);

		return requestOptions;
	}

	protected addSortRequestOptions(requestOptions: any): any {
		this.sortConf.forEach((fieldConf) => {
			if(fieldConf.direction.toUpperCase() == "DESC"){
				requestOptions[this.conf.sortFieldKey] 	= "-"+fieldConf.field;
			}else{
				requestOptions[this.conf.sortFieldKey] 	= fieldConf.field;
			}
			
		});

		return requestOptions;
	}

	protected addFilterRequestOptions(requestOptions: any): any {
		if (this.filterConf.filters) {
			this.filterConf.filters.forEach((fieldConf: any) => {
				if (fieldConf['search']) {
					if (fieldConf['field'] === 'search') {
						requestOptions[fieldConf['field']] = fieldConf['search'];
					}else {
						requestOptions[this.conf.filterFieldKey.replace('#field#', fieldConf['field'])] = fieldConf['search'];
					}
				}
			});
		}

		return requestOptions;
	}
	
	protected addPagerRequestOptions(): void{
		this.serviceToUse.setPage(this.pagingConf['page']);
		this.serviceToUse.setPageSize(this.pagingConf['perPage']);
	}
	
}
