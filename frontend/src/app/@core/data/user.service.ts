import {Injectable, Injector} from '@angular/core';
import {Observable} from 'rxjs/Observable';
import {HttpClient} from '@angular/common/http';
import {NbAuthJWTToken, NbTokenService} from '@nebular/auth';

import 'rxjs/add/observable/of';

import {AbstractService} from './abstract.service';

@Injectable()
export class UserService extends AbstractService {
  
  public links = {
    paginate: '/user/',
    listEntries: null,
    retrieveSingleEntry: '/user/#id#/',
    updateEntry: '/user/#id#/',
    createEntry: '/user/',
    deleteEntry: '/user/#id#/',
    counter: '/user/counter/',
  }
  
  
  getCurrentUser(): Observable<any> {
    return Observable.fromPromise(new Promise((resolve, reject) => {
      this.getTokenService.get().subscribe(
        (result: NbAuthJWTToken) => {
          this.getSingleEntry(result.getPayload().user_id).subscribe(
            (result2) => {
              resolve(result2);
            }
          );
        }
      )
    }));
  }
  
  refreshToken(token): Observable<any> {
    return this.post('/token_refresh/', {token: token}).map((response: Response) => {
      return response;
    });
  }
  
  
  protected get getTokenService(): NbTokenService {
    return this.injector.get(NbTokenService);
  }
}
