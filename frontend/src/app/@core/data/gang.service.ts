import { Injectable, Injector, Optional } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { HttpClient } from '@angular/common/http';
import { NbAuthJWTToken, NbTokenService } from '@nebular/auth';

import 'rxjs/add/observable/of';

import { AbstractService } from './abstract.service';

@Injectable()
export class GangService extends AbstractService {

	public links = {
		paginate: '/gang/',
		listEntries: null,
		retrieveSingleEntry: '/gang/#id#/',
		updateEntry: '/gang/#id#/',
		createEntry: '/gang/',
		deleteEntry: '/gang/#id#/',
		counter: '/gang/counter/',
		getGangUsers: '/gang/#id#/get_gang_users/',
		counterByCities: '/gang/count_by_cities/',
		createUser: '/signup/create_user/',
		getGangPhases: '/gang/#id#/get_gang_phases/',
		register: '/signup/register/',
		downloadReport: '/report/generate/',
	};

	public getGangUsers(id = null, options = {}): Observable<any> {
		const args = Object.assign({}, this.paginatorOptions, options);
		const link = this.links['getGangUsers'].replace(/#id#/g, id);
		return this.get(link, args);
	}

	public getGangPhases(id = null, options = {}): Observable<any> {
		const args = Object.assign({}, this.paginatorOptions, options);
		const link = this.links['getGangPhases'].replace(/#id#/g, id);
		return this.get(link, args);
	}

	public getCountByCities(data: any = {}, options: any = {}): Observable<any> {
		return this.post(this.links.counterByCities, data, options);
	}

	public publicRegister(data: any = {}, options: any = {}): Observable<any> {
		return this.post(this.links.register, data, options);
	}

	public downloadReport(options: any = {}): Observable<any> {
		return this.get(this.links.downloadReport, options);
	}

}
