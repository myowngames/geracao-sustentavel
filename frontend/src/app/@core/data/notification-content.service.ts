import { Injectable, Injector } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { HttpClient } from '@angular/common/http';
import { NbAuthJWTToken, NbTokenService } from '@nebular/auth';

import 'rxjs/add/observable/of';

import { AbstractService } from './abstract.service';

@Injectable()
export class NotificationContentService extends AbstractService{

    public links = {
        paginate: '/notificationcontent/',
        listEntries: null,
        retrieveSingleEntry: '/notificationcontent/#id#/',
        updateEntry: '/notificationcontent/#id#/',
        createEntry: '/notificationcontent/',
        deleteEntry: '/notificationcontent/#id#/',
        counter: '/notificationcontent/counter/',
    }

}
