import { Injectable, Injector } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { HttpClient } from '@angular/common/http';
import { NbAuthJWTToken, NbTokenService } from '@nebular/auth';

import 'rxjs/add/observable/of';

import { AbstractService } from './abstract.service';

@Injectable()
export class GangPhaseDeliverableVoteService extends AbstractService{

    public links = {
        paginate: '/gangphasedeliverablevote/',
        listEntries: null,
        retrieveSingleEntry: '/gangphasedeliverablevote/#id#/',
        updateEntry: '/gangphasedeliverablevote/#id#/',
        createEntry: '/gangphasedeliverablevote/',
        deleteEntry: '/gangphasedeliverablevote/#id#/',
        counter: '/gangphasedeliverablevote/counter/',
    }


    saveCityAdminVote(data = {}): Observable<any>{
    	return this.post('/gangphasedeliverablevote/save_city_admin_vote/', data);
    }


    getPhaseDeliverableVotesAdminCity(data): Observable<any>{
    	return this.post('/gangphasedeliverablevote/get_phase_deliverable_votes_admin_city/', data);
    }

    getVideoTotalVotes(data): Observable<any>{
    	return this.post('/gangphasedeliverablevote/get_video_total_votes/', data);
    }

    getVoterUserVote(): Observable<any>{
    	return this.get('/gangphasedeliverablevote/get_voter_user_vote/');
    }
}
