import { Injectable, Injector } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { HttpClient } from '@angular/common/http';
import { NbAuthJWTToken, NbTokenService } from '@nebular/auth';

import 'rxjs/add/observable/of';

import { AbstractService } from './abstract.service';

@Injectable()
export class DeliverableFileService extends AbstractService{
  
  public links = {
    paginate: '/deliverablefile/',
    listEntries: null,
    retrieveSingleEntry: '/deliverablefile/#id#/',
    updateEntry: '/deliverablefile/#id#/',
    createEntry: '/deliverablefile/',
    deleteEntry: '/deliverablefile/#id#/',
    counter: '/deliverablefile/counter/',
  }
  
  
  getAllowedContentTypes(): Observable<any>{
    return this.get('/deliverablefile/get_allowed_content_types/');
  }
}
