import { Injectable, Injector } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { HttpClient } from '@angular/common/http';
import { NbAuthJWTToken, NbTokenService } from '@nebular/auth';

import 'rxjs/add/observable/of';

import { AbstractService } from './abstract.service';

@Injectable()
export class CityService extends AbstractService{

    public links = {
        paginate: '/city/',
        listEntries: null,
        retrieveSingleEntry: '/city/#id#/',
        updateEntry: '/city/#id#/',
        createEntry: '/city/',
        deleteEntry: '/city/#id#/',
        counter: '/city/counter/',
    }

	public states = {
		'AC':'Acre',
		'AL':'Alagoas',
		'AP':'Amapá',
		'AM':'Amazonas',
		'BA':'Bahia',
		'CE':'Ceará',
		'DF':'Distrito Federal',
		'ES':'Espírito Santo',
		'GO':'Goiás',
		'MA':'Maranhão',
		'MT':'Mato Grosso',
		'MS':'Mato Grosso do Sul',
		'MG':'Minas Gerais',
		'PA':'Pará',
		'PB':'Paraíba',
		'PR':'Paraná',
		'PE':'Pernambuco',
		'PI':'Piauí',
		'RJ':'Rio de Janeiro',
		'RN':'Rio Grande do Norte',
		'RS':'Rio Grande do Sul',
		'RO':'Rondônia',
		'RR':'Roraima',
		'SC':'Santa Catarina',
		'SP':'São Paulo',
		'SE':'Sergipe',
		'TO':'Tocantins'
	};

}
