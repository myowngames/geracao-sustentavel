import { Injectable, Injector } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { HttpClient } from '@angular/common/http';
import { NbAuthJWTToken, NbTokenService } from '@nebular/auth';

import 'rxjs/add/observable/of';

import { AbstractService } from './abstract.service';

@Injectable()
export class GangPhaseService extends AbstractService{

    public links = {
        paginate: '/gangphase/',
        listEntries: null,
        retrieveSingleEntry: '/gangphase/#id#/',
        updateEntry: '/gangphase/#id#/',
        createEntry: '/gangphase/',
        deleteEntry: '/gangphase/#id#/',
        counter: '/gangphase/counter/',
        completeCount: '/gangphase/#id#/get_complete_count/'
    }
  
  
  public completeCount(id =null,options= {}): Observable<any>{
    const args = Object.assign({}, this.paginatorOptions, options);
    const link = this.links['completeCount'].replace(/#id#/g, id);
    return this.get(link, args);
  }
}
