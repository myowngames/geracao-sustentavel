import { Injectable, Injector } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { NbMenuItem } from '@nebular/theme';
import { HttpClient } from '@angular/common/http';
import { URLSearchParams } from '@angular/http';
import { NbAuthService, NbAuthJWTToken } from '@nebular/auth';
import { environment } from '../../../environments/environment';
import 'rxjs/add/observable/of';
import 'rxjs/add/observable/fromPromise';


@Injectable()
export class AbstractService {
	public xhr: any;
	constructor(
		protected httpClient: HttpClient,
		protected authService: NbAuthService,
		protected injector: Injector,

	) {
		this.links = Object.assign({}, this.linksDefault, this.links);
	}

	// those options are rewriten by the called function
	// they are just default values
	public paginatorOptions = {
		page: 1,
		page_size: 15,
	};

	public linksDefault = {
		paginate: null,
		listEntries: null,
		retrieveSingleEntry: null,
		updateEntry: null,
		createEntry: null,
		deleteEntry: null,
		counter: null,
	};

	public links = {};

	getPaginatedDataPromise: Promise<any>;
	getPaginatedDataResolve: Function | null = null;


	public getCount(options = {}): Observable<any> {

		return this.get(this.links['counter'], options);
	}
	public setPage(page) {
		this.paginatorOptions.page = page;
	}
	public getPage() {
		return this.paginatorOptions.page;
	}
	public setPageSize(page_size = 15) {
		this.paginatorOptions.page_size = page_size;
	}
	public getPageSize() {
		return this.paginatorOptions.page_size;
	}

	public getSingleEntry(id = null, options = {}): Observable<any> {
		const args = Object.assign({}, this.paginatorOptions, options);
		const link = this.links['retrieveSingleEntry'].replace(/#id#/g, id);

		return this.get(link, args);
	}

	public getAllEntries(options = {}): Observable<any> {
		if (this.links['listEntries'] != null) {
			return this.get(this.links['listEntries'], options);
		} else {
			this.getPaginatedDataPromise = new Promise((resolve, reject) => {
				this.getPaginatedDataResolve = resolve;
			});

			// we solve the promise inside the function
			this.getAllFromListPaginated(1, [], options);

			return Observable.fromPromise(this.getPaginatedDataPromise);
		}
	}

	protected getAllFromListPaginated(page = 1, arrayToPush = [], options = {}): void {
		options['page'] = page;

		this.getListPaginated(options).subscribe(
			(result) => {
				for (let i = 0; i < result.results.length; i++) {
					arrayToPush.push(result.results[i]);
				}

				if (result.links.next != null) {
					this.getAllFromListPaginated(page + 1, arrayToPush);
				} else {
					this.getPaginatedDataResolve(arrayToPush);
				}
			},
		);
	}

	public getListPaginated(options = {}): Observable<any> {
		const args = Object.assign({}, this.paginatorOptions, options);
		return this.get(this.links['paginate'], { data: args });
	}



	public createEntry(data, options = {}): Observable<any> {
		const args = Object.assign({}, this.paginatorOptions, options);
		return this.post(this.links['createEntry'], data, args);
	}
	private objectToFormData = function (obj, form: FormData = null, namespace: string = null) {
		const fd = form || new FormData();
		let formKey;
		for (const property in obj) {
			if (obj.hasOwnProperty(property)) {

				if (namespace) {
					formKey = namespace + '[' + property + ']';
				} else {
					formKey = property;
				}

				// if the property is an object, but not a File,
				// use recursivity.
				if (typeof obj[property] === 'object' && !(obj[property] instanceof File)) {
					if (namespace) {
						this.objectToFormData(obj[property], fd, formKey);
					} else {
						this.objectToFormData(obj[property], fd, property);
					}

				} else {
					// if it's a string or a File object
					fd.append(formKey, obj[property]);
				}
			}
		}
		return fd;
	};
	public createEntryWithFile(data, options = {}): Observable<any> {
		return this.xhrPost(this.links['createEntry'], data);
	}

	public updateEntry(id, data, options = {}): Observable<any> {
		const args = Object.assign({}, this.paginatorOptions, options);
		const link = this.links['updateEntry'].replace(/#id#/g, id);

		return Observable.fromPromise(
			new Promise((resolve, reject) => {
				this.getSingleEntry(id).subscribe(
					(entryBeforeUpdate) => {

						for (const indexName in data) {
							if (typeof entryBeforeUpdate[indexName] !== 'undefined') {
								if (entryBeforeUpdate[indexName] === data[indexName]) {
									delete data[indexName];
								}
							}
						}

						this.patch(link, data, args).subscribe(
							(result) => {
								resolve(result);
							},
						);
					},
				);
			}),
		);
	}
	public UpdateEntryWithFile(id, data, options = {}, method = 'PATCH'): Observable<any> {
		return this.xhrPost(this.links['updateEntry'].replace(/#id#/g, id), data, false, method);
	}
	public deleteEntry(id = null, options = {}): Observable<any> {
		const link = this.links['deleteEntry'].replace(/#id#/g, id);

		if (confirm('Certeza que deseja remover?')) {
			return this.delete(link, options);
		} else {
			return Observable.of(false);
		}
	}


	public setupCallbacks() {
		this.xhr.upload.addEventListener('progress', this.ajaxObjectListenerProgress.bind(this), false);
		this.xhr.addEventListener('load', this.ajaxObjectListenerOnLoad.bind(this), false);
		this.xhr.addEventListener('error', this.ajaxObjectListenerOnError.bind(this), false);
		this.xhr.addEventListener('abort', this.ajaxObjectListenerOnAbort.bind(this), false);
	}
	public ajaxObjectListenerProgress(event) {

	}
	public ajaxObjectListenerOnLoad(event) {

	}
	public ajaxObjectListenerOnError(event) {

	}
	public ajaxObjectListenerOnAbort(event) {

	}
	protected get(url: string, options = {}): Observable<any> {
		if (typeof options['data'] !== 'undefined') {
			let getArgsString = '?';

			for (const argName in options['data']) {
				if (typeof options['data'][argName] === 'boolean') {
					if (options['data'][argName] === true) {
						options['data'][argName] = 1;
					} else {
						options['data'][argName] = 0;
					}
				}
				getArgsString += argName + '=' + options['data'][argName] + '&';
			}

			url += getArgsString;
		}

		return this.httpClient.get(url, options);
	}


	protected post(url: string, data = {}, options = {}): Observable<any> {
		return this.httpClient.post(url, data, options);
	}

	protected put(url: string, data = {}, options = {}): Observable<any> {
		return this.httpClient.put(url, data, options);
	}

	protected patch(url: string, data = {}, options = {}): Observable<any> {
		return this.httpClient.patch(url, data, options);
	}

	protected delete(url: string, options = {}): Observable<any> {
		return this.httpClient.delete(url, options);
	}
	protected xhrPost(url: string, data = {}, ignoreToken: boolean = false, method = 'POST'): Observable<any> {
		data = this.objectToFormData(data);

		const that = this;
		return Observable.fromPromise(new Promise((resolve, reject) => {
			that.xhr = new XMLHttpRequest();

			that.xhr.onreadystatechange = function () {
				if (that.xhr.readyState === 4) {
					if (that.xhr.status === 201 || that.xhr.statusText === 'Created' || that.xhr.statusText === 'OK') {
						resolve(JSON.parse(that.xhr.response));
					} else {
						try {
							reject(JSON.parse(that.xhr.response));
						} catch (err) {
							reject(that.xhr.response);
						}
					}
				}
			};
			that.xhr.open(method, this.getUrl(url), true);
			this.setupCallbacks();
			if (ignoreToken) {
				that.xhr.send(data);
			} else {
				that.authService.getToken().subscribe(
					(result) => {
						that.xhr.setRequestHeader('Authorization', 'JWT ' + result['token']);
						that.xhr.send(data);
					},
				);
			}

		}));
	}

	public getUrl(endpoint_url) {
		// define url
		let url = environment.api_endpoint;
		const suffix = endpoint_url;

		if (suffix.search('http://') === -1 && suffix.search('https://') === -1) {
			const parts = suffix.split('');
			if (parts[0] === '/') {
				parts.shift();
			}

			const imploded = parts.join('');

			url += imploded;
		}
		return url;
	}
}
