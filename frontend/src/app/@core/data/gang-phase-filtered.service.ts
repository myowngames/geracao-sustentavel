import { Injectable, Injector } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { HttpClient } from '@angular/common/http';
import { NbAuthJWTToken, NbTokenService } from '@nebular/auth';

import 'rxjs/add/observable/of';

import { AbstractService } from './abstract.service';

@Injectable()
export class GangPhaseFilteredService extends AbstractService{
  private id:number = 0;
 
  public setLink(id): any {
    this.id = id;
    return this;
  }
  public set links(data){
    // Ignorando
  }
  public get links(): any{
    return {
      paginate: '/phase/' + this.id + '/get_gang_phase/',
      listEntries: '/phase/' + this.id + '/get_gang_phase/',
    }
  }
}
