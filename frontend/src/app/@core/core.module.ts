import { ModuleWithProviders, NgModule, Optional, SkipSelf } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NbAuthModule, NbEmailPassAuthProvider, NB_AUTH_TOKEN_WRAPPER_TOKEN, NbAuthJWTToken } from '@nebular/auth';
import { RouterModule } from '@angular/router';


import { throwIfAlreadyLoaded } from './module-import-guard';
import { DataModule } from './data/data.module';
import { AnalyticsService } from './utils/analytics.service';

import { getDeepFromObject, deepExtend, getTokenFromHttpResponse } from './helpers';

import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { FlashService } from '../@core/utils/flash.service';

const NB_CORE_PROVIDERS = [
    ...DataModule.forRoot().providers,
    ...NbAuthModule.forRoot({
        providers: {
            email: {
                service: NbEmailPassAuthProvider,
                config: {
                    delay: 10,
                    login: {
                        endpoint: '/api-token-auth/',
                        rememberMe: true,
                        redirect: {
                            success: '/admin/dashboard',
                            failure: null,
                        },
                        defaultErrors: ['A combinação de usuario e senha não está correta, tente novamente.'],
                       defaultMessages: ['Você foi logado com sucesso.'],
                    },
                    logout: {
                        alwaysFail: false,
                        endpoint: '/auth/logout/',
                        method: 'get',
                        redirect: {
                            success: '/admin',
                            failure: '/',
                        },
                        defaultErrors: ['Alguma coisa deu errado. Por favor, tente outra vez.'],
                        defaultMessages: ['Você foi desconectado com sucesso.'],
                    },
                    requestPass: {
                        endpoint: '/signup/remember_pass/',
                        method: 'post',
                        redirect: {
                            success: '/',
                            failure: null,
                        },
                        defaultErrors: ['Alguma coisa deu errado. Por favor, tente outra vez.'],
                        defaultMessages: ['As instruções de reiniciar a senha foram enviadas para o seu email.'],
                    },
                    register: {
                        endpoint: '/signup/',
                        method: 'post',
                        redirect: {
                            success: '/',
                            failure: '/auth/register',
                        },
                        defaultErrors: ['Alguma coisa deu errado. Por favor, tente outra vez.'],
                        defaultMessages: ['Você foi registrado com sucesso.'],
                    },
                    resetPass: {
                        endpoint: '/signup/reset_pass/',
                        method: 'post',
                        redirect: {
                            success: '/',
                            failure: '/auth/reset-password/:token',
                        },
                        defaultErrors: ['Alguma coisa deu errado. Por favor, tente outra vez.'],
                        defaultMessages: ['Sua senha foi alterada com sucesso.'],
                    },
                    token: {
                        key: 'data.token',
                        getter: getTokenFromHttpResponse,
                    },

                },
            },
        },
    }).providers,
    AnalyticsService,
    FlashService,
];

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        NbAuthModule,
        RouterModule
    ],
    exports: [
        NbAuthModule,
    ],
})


export class CoreModule {
    constructor(@Optional() @SkipSelf() parentModule: CoreModule) {
        throwIfAlreadyLoaded(parentModule, 'CoreModule');
    }

    static forRoot(): ModuleWithProviders {
        return <ModuleWithProviders>{
        ngModule: CoreModule,
            providers: [
                ...NB_CORE_PROVIDERS,
            ],
        };
    }
}
