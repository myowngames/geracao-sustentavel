import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { CityService } from '../../@core/data/city.service';
import { FlashService } from '../../@core/utils/flash.service';
import { UserService } from '../../@core/data/user.service';
import { forEach } from '@angular/router/src/utils/collection';
import { SubscriptionComponent } from '../../@core/abstract_components/subscription.component';
import { GangService } from '../../@core/data/gang.service';
import * as moment from 'moment';
import { ValidateDate } from '../../@core/directives/DateValidators.directive';
import { letProto } from 'rxjs/operator/let';
import { Observable } from 'rxjs/Observable';
import { NavigationEnd, Router } from '@angular/router';

@Component({
	selector: 'ngx-public-inscricoes',
	styleUrls: ['./inscricoes.component.scss', '../public.component.scss'],
	templateUrl: './inscricoes.component.html',
})
export class InscricoesComponent extends SubscriptionComponent implements OnDestroy, OnInit {
	public form: FormGroup;
	public gang: any;
	public cities: any;
	public files: any = [];
	public group: any = [];
	public userSent: number = 0;
	public submitted: boolean = false;
	public dateMask = [/\d/, /\d/, '/', /\d/, /\d/, '/', /\d/, /\d/, /\d/, /\d/];
	public phoneMask = ['(', /[1-9]/, /\d/, ')', ' ', /\d/, /\d/, /\d/, /\d/, '-', /\d/, /\d/, /\d/, /\d/, /\d/];
	public rgMask = [/\d/, /\d/, '.', /\d/, /\d/, /\d/, '.', /\d/, /\d/, /\d/, '-', /\w/];
	public startDate = moment('02/04/2018 00:00:00', 'DD/MM/YYYY hh:mm:ss');
	public finalDate = moment('11/05/2018 23:59:59', 'DD/MM/YYYY hh:mm:ss');
	public canSubscribe = false;
	public isToEarly = true;
	public isToLate = true;
	constructor(private formbuilder: FormBuilder,
		private city_services: CityService,
		private flashService: FlashService,
		private userService: UserService,
		private gangService: GangService,
		private router: Router,
	) {
		super();
		this.gang = {};
		this.cities = [];
	}
	ngOnInit() {
		this.form = this.formbuilder.group({
			name: [this.gang.name, Validators.compose([Validators.required, Validators.minLength(2)])],
			city: [this.gang.city, Validators.compose([Validators.required])],
			school: [this.gang.school, Validators.compose([Validators.required, Validators.minLength(2)])],
			users: new FormArray([]),
		});
		// Começando com 5 users
		this.formCreateUser();
		this.formCreateUser();
		this.formCreateUser();
		this.formCreateUser();
		this.formCreateUser();
		this.retrieveCities();
		this.checkIfSubscriptionIsOff();
		this.subscribeToCountNotifications();
	}
	checkIfIsToEarly(): void {
		// Resultado negativo esta no passado
		// se tiver no passado é muito cedo
		moment().diff(this.startDate, 'seconds') <= 0 ? this.isToEarly = true : this.isToEarly = false;
	}
	checkIfIsToLate(): void {
		// se tiver no futuro, é tarde d+
		moment().diff(this.finalDate, 'seconds') <= 0 ? this.isToLate = false : this.isToLate = true;
	}
	checkIfSubscriptionIsOff() {
		this.checkIfIsToEarly();
		this.checkIfIsToLate();
		if (!this.isToEarly && !this.isToLate) {
			this.canSubscribe = true;
		}
	}
	subscribeToCountNotifications() {
		this.pushSub(
			Observable.timer(10000).subscribe((next) => this.checkIfSubscriptionIsOff()),
		);
	}
	ngOnDestroy() {

	}
	formCreateUser() {
		let gang_leader_default: Boolean = true;
		const userFormControl = this.form.get('users') as FormArray;
		if (userFormControl.controls.length > 0) {
			gang_leader_default = false;
		} else {
			this.gang.users = [];
		}
		const index = userFormControl.controls.length;
		this.gang.users[index] = {};
		const formGroup: FormGroup = this.formbuilder.group({
			name: [this.gang.users[index].name, Validators.compose([Validators.required, Validators.minLength(2)])],
			email: [this.gang.users[index].email, Validators.compose([Validators.required, Validators.email])],
			phone: [this.gang.users[index].phone, Validators.compose([])],
			password: [this.gang.users[index].password, Validators.compose([])],
			username: [this.gang.users[index].username, Validators.compose([])],
			is_active: [this.gang.users[index].is_active, Validators.compose([])],
			rg: [this.gang.users[index].rg, Validators.compose([Validators.required, Validators.minLength(8), Validators.maxLength(13)])],
			birthday: [this.gang.users[index].birthday, Validators.compose([Validators.required, ValidateDate])],
			term: [this.gang.users[index].term, Validators.compose([Validators.required])],
			gang_leader: [gang_leader_default, Validators.compose([])],
		});
		userFormControl.push(formGroup);
	}
	getCityById(id): any {
		for (let i = 0; i < this.cities.length; i++) {
			if (this.cities[i].id === id) {
				return this.cities[i];
			}
		}
		return [];
	}
	formRemoveUser(i) {
		const user_control: any = this.form.get('users');
		user_control.controls.splice(i, 1);
	}
	slugify(text: string = '') {
		return text.toLowerCase()
			.replace(/\s+/g, '-')           // Replace spaces with -
			.replace(/[^.\w\-]+/g, '')       // Remove all non-word chars
			.replace(/\-\-+/g, '-')         // Replace multiple - with single -
			.replace(/^-+/, '')             // Trim - from start of text
			.replace(/-+$/, '');            // Trim - from end of text
	}
	generatePassword() {
		const length = 8,
			charset = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789';
		let retVal = '';
		for (let i = 0, n = charset.length; i < length; ++i) {
			retVal += charset.charAt(Math.floor(Math.random() * n));
		}
		return retVal;
	}
	onLeaderChange(i) {
		const users_control: any = this.form.get('users');
		for (let j = 0; j < users_control.controls.length; j++) {
			users_control.controls[j].get('gang_leader').setValue(false);
		}
		users_control.controls[i].get('gang_leader').setValue(true);
	}
	public get userControls() {
		const controls: any = this.form.controls['users'];
		return controls.controls;
	}
	resetForm() {
		this.form.reset();
		this.gang = [];
		this.files = [];
	}
	onSubmit() {
		if (!this.form.valid) {
			this.submitted = false;
			this.markFormGroupTouched(this.form);
			this.flashService.push('error', 'Por favor, verifique seus campos de cadastro!');
		} else {
			let data = this.form.value;
			let hasOneLeader = false;
			for (let i = 0; i < data.users.length; i++) {
				data.users[i].term = this.files[i];
				data.users[i].term.name = this.slugify(data.users[i].term.name);
				data.users[i].term.filename = data.users[i].term.name;
				data.users[i].password = this.generatePassword();
				data.users[i].username = this.slugify(data.users[i].name);
				data.users[i].phone = data.users[i].phone.replace(/_/g, '');
				data.users[i].rg = data.users[i].rg.replace(/_/g, '');
				const dateExploded = data.users[i].birthday.split('/');
				if (dateExploded.length > 1 ) {
					const date = moment(new Date(dateExploded[2], (dateExploded[1] - 1), dateExploded[0]));
					data.users[i].birthday = date.format('YYYY-MM-DD');
				}
				if (data.users[i].gang_leader) {
					hasOneLeader = true;
				}
			}
			if (!hasOneLeader) {
				this.flashService.push('error', 'Necessario escolher pelo menos um lider do grupo!');
				return false;
			}
			// const users = data.users;
			// data.city = this.getCityById(data.city);
			// delete data.users;
			this.submitted = true;
			this.pushSub(
				this.gangService.publicRegister(data).subscribe(
					(response) => {
						this.submitted = false;
						this.resetForm();
						this.flashService.push('success', 'Equipe criada com sucesso! Você receberá um e-mail com os dados para login.');
						this.router.navigate(['auth', 'login']);
					},
					(error) => {
						this.submitted = false;
						/*
						if (typeof error === 'object') {
							for (const e in error) {
								if (error.hasOwnProperty(e)) {
									this.flashService.push('error', error[e]);
								}
							}
						} else {
							this.flashService.push('error', 'Erro ao criar a equipe');
						}
						*/
					},
				),
			);
		}
	}
	private markFormGroupTouched(formGroup: any) {
		for (const key in formGroup.controls) {
			if (formGroup.controls.hasOwnProperty(key)) {
				const control = formGroup.get(key);
				control.markAsTouched();
				if (control.controls) {
					control.controls.forEach(c => this.markFormGroupTouched(c));
				}
			}
		}
	}
	/*
	onFileChange(event, index) {
		this.files[index] = event.target.files[0];
		const users: any = this.form.get('users');
		users.controls[index].get('term').setValue(
			this.files[index].name ? this.files[index].name : null,
		); // <-- Set Value for Validation

	}
	*/
	onFileChange(event, index) {
		const reader = new FileReader();
		if (event.target.files && event.target.files.length > 0) {
			const file = event.target.files[0];
			reader.readAsDataURL(file);
			reader.onload = () => {
				this.files[index] = {
					filename: file.name,
					name: file.name,
					filetype: file.type,
					value: reader.result.split(',')[1],
				};
				const users: any = this.form.get('users');
				users.controls[index].get('term').setValue(
					this.files[index].filename ? this.files[index].filename : null,
				);
			};
		}
	}
	retrieveCities() {
		this.city_services.getAllEntries().subscribe(
			(result) => {
				this.cities = result;
			},
		);
	}
}
