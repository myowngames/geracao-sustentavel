import { NgModule } from '@angular/core';
import { NgxEchartsModule } from 'ngx-echarts';

import { ThemeModule } from '../../@theme/theme.module';
import { InscricoesComponent } from './inscricoes.component';
import { FormsModule } from '@angular/forms';
import { DirectivesModule } from '../../@core/directives/directives.module';
import { TextMaskModule } from 'angular2-text-mask';

@NgModule({
	imports: [
		ThemeModule,
		NgxEchartsModule,
		FormsModule,
		DirectivesModule,
		TextMaskModule,
	],
	declarations: [
		InscricoesComponent,
	],
})
export class InscricoesModule { }
