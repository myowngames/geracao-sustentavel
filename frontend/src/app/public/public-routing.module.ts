import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';
import {PublicComponent} from "./public.component";
import {HomeComponent} from "./home/home.component";
import {AgendaComponent} from "./agenda/agenda.component";
import {ApresentacaoComponent} from "./apresentacao/apresentacao.component";
import {ContatoComponent} from "./contato/contato.component";
import {InscricoesComponent} from "./inscricoes/inscricoes.component";
import {PremiacaoComponent} from "./premiacao/premiacao.component";
import {RegulamentoComponent} from "./regulamento/regulamento.component";




const routes: Routes = [
    {
        path: '',
        component: PublicComponent,
        children: [
            {
                path: '',
                component: HomeComponent
            },
            {
              path: 'agenda',
              component: AgendaComponent
            },
            {
              path: 'apresentacao',
              component: ApresentacaoComponent
            },
            {
              path: 'contato',
              component: ContatoComponent
            },
            {
              path: 'inscricoes',
              component: InscricoesComponent
            },
            {
              path: 'premiacao',
              component: PremiacaoComponent
            },
            {
              path: 'regulamento',
              component: RegulamentoComponent
            }
        ],
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule],
})
export class PublicRoutingModule {
}
