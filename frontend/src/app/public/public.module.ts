import { NgModule } from '@angular/core';


import { ThemeModule } from '../@theme/theme.module';
import { PublicComponent } from './public.component';
import { PublicRoutingModule } from './public-routing.module';
import { HomeModule } from './home/home.module';
import { AgendaModule } from './agenda/agenda.module';
import { ApresentacaoModule } from './apresentacao/apresentacao.module';
import { ContatoModule } from './contato/contato.module';
import { InscricoesModule } from './inscricoes/inscricoes.module';
import { PremiacaoModule } from './premiacao/premiacao.module';
import { RegulamentoModule } from './regulamento/regulamento.module';
const PUBLIC_COMPONENTS = [
	PublicComponent,
];

@NgModule({
	imports: [
		PublicRoutingModule,
		ThemeModule,
		HomeModule,
		AgendaModule,
		ApresentacaoModule,
		ContatoModule,
		InscricoesModule,
		PremiacaoModule,
		RegulamentoModule,
	],
	declarations: [
		...PUBLIC_COMPONENTS,
	],
})
export class PublicModule {
}
