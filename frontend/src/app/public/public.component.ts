import { Component } from '@angular/core';
import {Router, NavigationEnd } from "@angular/router";

@Component({
  selector: 'ngx-public',
  styleUrls: ['./public.component.scss'],
  templateUrl: './public.component.html',
})


export class PublicComponent {
  constructor(private router: Router) { }
  public isNavbarCollapsed: boolean = false;
  ngOnInit() {
    this.router.events.subscribe((evt) => {
      if (!(evt instanceof NavigationEnd)) {
        return;
      }
      window.scrollTo(0, 0)
    });
  }

}
