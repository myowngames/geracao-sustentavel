/**
 * @license
 * Copyright Akveo. All Rights Reserved.
 * Licensed under the MIT License. See License.txt in the project root for license information.
 */
import { Component, OnInit } from '@angular/core';
import { AnalyticsService } from './@core/utils/analytics.service';
import { FlashService } from './@core/utils/flash.service';

@Component({
  selector: 'ngx-app',
  template: '<router-outlet><flash-message [messages]="messagesToShow"></flash-message></router-outlet>',
})

export class AppComponent implements OnInit {

    constructor(
        private analytics: AnalyticsService,
        private flashService: FlashService
    ) {
    }

    ngOnInit(): void {
        this.analytics.trackPageViews();
    }

    get messagesToShow(){
        return this.flashService.getAll();
    }

}
