# [BotDoc API FrontEnd](https://api.botdoc.io/)

BotDoc API FrondEnd, is where the developers go to see metricks and manage their API information, including payment and other stuff

## Installation
Requirements
To install ngx-admin on your machine you need to have the following tools installed:
- Git - https://git-scm.com
- Node.js - https://nodejs.org. Please note the version should be >=7
- Npm - Node.js package manager, comes with Node.js. Please make sure npm version is >=5
- You might also need some specific native packages depending on your operating system like <i>build-essential</i> on Ubuntu

Please note that it is not possible to build ngx-admin without these tools and it will not be possible because of the way how Angular is built.

### Download the Code

After you completed tools setup, you need to download the code of ngx-admin application. The easiest way to do that is to clone GitHub repository:

git clone https://github.com/shortsave/botdoc_api_frontend_nebular

After clone is completed, you need to install npm modules:
``` bash
cd botdoc_api_frontend_nebular
npm install
```

You should create proxy.conf.json file in your application root. The file should contain something like below: (You can also use the proxy.conf.sample.json)
Change the target for the running Python Django Application of the BotDoc API https://github.com/shortsave/botdoc_api

``` json
{
  "/api": {
    "target": "http://localhost:8000",
    "secure": false
  }
}

```

To run the server from you terminal, just run, 

``` bash
npm start
```

## Code Deployment (Build)
* In order to build for the production environment run:
``` bash
	npm run build:prod
```
* In order to build for the sandbox environment run:
``` bash
	npm run build:sandbox
```

## Code Workflow
* Master - Deployment Branch
  * hotfix/#*Issue Number* - Fix directly on master based on github issue number
* Sandbox - Development Branch
  * issue/#*Issue Number* - Fix an issue that is non blocking and can be fixed on the development
  * feature/Feature Name#*Issue Number* - Create a new Feature, should be non blocking before sending for testing

## Environments
* Deployment (Master) - https://developer.botdoc.io
* Development/Beta Testing (sandbox) - https://sandboxdev.botdoc.io
* All other Enviroments should be localhost

## Some Links
* [Nebular Documentation](https://akveo.github.io/nebular/#/docs/getting-started/what-is-nebular/) - Nebular is a set of essential modules that we use on our application.

