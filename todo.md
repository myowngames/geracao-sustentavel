
#BUGS
- [x] CSS fica zoado do Public quando desloga da Home
- [X] Arrumar tabelas de listagem de fases e deliverables
- [ ] Filtro de arquivo na hora de enviar deliverables
- [X] Arrumar Criação/update de Phases/PhaseViews
#Melhorias
- [ ] Notificações
    - [X] Badge de notificações
    - [ ] Quando clicar mostrar dropdown de notificações
- [ ] PhaseView
    - [ ] Estilizar botão de Upload
    - [ ] Mudar Botão de download de arquivo para a direita e adicionar icone 
    - [ ] Adicionar Visualização de arquivos ja enviados
 
    



Backend
- [X] post-save gang adicionar gang phases
- [X] post-save phase adicionar gang phases
- [X] pre-deleted phase check se exit gang phases e não deixar
- [X] Ultimo Deliverable de acordo com a fase marca como completed

#FrontEnd
- Geral
	- [X] Implementar Esqueci a Senha

- Visão Admin
	- [X] Pagina de envios de notificação
		- [X] Pagina de visualização de notificação
	- [X] Pagina de criação de deliverables dentro de cada fase
	    - [X] Cada Deliverable pode ter varios arquivos linkados com ele preicamos alterar como é feita essa ligação
	- [X] Pagina de visualização de Videos
	- [X] Visualização de Grupos
	    - [X] Flata Visualizar os Deliverables
	- [X] Visualização de Grupos
	    - [X] Flata Visualizar os Deliverables
	- [X] View Notification Content
	
- Visão Grupo
	- [X] Aterar Dashboard para visão de grupo
		- [X] Deixar visivel apensar a fase atual
		- [X] Adicionar mensagems dos Adminstradores/notificações
		- [X] Adicionar Contato
	- [X] Gerenciamento dos usuarios e grupos
		- [X] Poder altera o Nome do Grupo e Nome da Escola
		- [X] Gerenciar usuarios integrantes
	- [X] Pagina de Fases com a lista de deliverables

- Visão Admin Cidade
	- [x] Altear Dashboard
		- [x] Alterar total de grupos baseado na cidade
		- [x] Alterar o Status de Envio Baseado na cidade
	- [ ] Notificações dos grupos daquela cidade
	- [ ] Visualização dos Grupos mas sem edição
	- [X] Videos

- Visão Votação
	- [X] Dashboard de Votãção Nacional
	
- Public
    - [X] Pagina de Resgitro de User
        - [X] Envia email pro lider com usuario e senha
        - [x] Apenas 1 Lider de grupo (trocar checkbox para radio button ou deixa desabilitado e o primeiro é lider do grupo automaticamente (nem mostra o chcebkox nos outros))

#Layout
## Geral
- [x] Acredito que seja melhor deixar com tamanho maximo igual esta no Ilustrado 1280px
	~dan comment: o container tem 1170px (estava 1040px). Sendo 55px de padding pra cada lado. (estava 70px)
- [ ] As quebras de linhas tem que esta bem parecidas com o layout, então fazer max-width nas colunas de texto (essa podemos esperar se eles vão reclamar)
## Home
- [x] Link do Botão INSCREVA-SE
- [x] Imagem do Mosquito esta cortada as boardas para mim, acho que precisa dar margem nela
## Concurso
- [ ] Na tela grande Texto principal esta sendo cortado em duas linhas, acho melhor deixar o max width igual eles colocaram que eles vão reclamar
- [x] No Mobileo Fundo do "VOCE SABE O QUE PODE FAZER" não esta pegando o texto inteiro então o texto esta se misturando com o branco
## Agenda
- [x] Acho que precisa dar um Jeito nesse mapa ta meio estrnaho no mobile
	~dan comment: tentei dar uma melhorada, pode navegar com o dedo dando scroll
## Inscrições
- [x] O botão do "Upload do Termo de Responsabilidade" não ta centralizado o texto
- [x] Terá apenas 1 lider do grupo


 