from django.apps import AppConfig


class ModSharedConfig(AppConfig):
    name = 'mod_shared'
