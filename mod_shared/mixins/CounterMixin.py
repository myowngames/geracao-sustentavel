from django.http import JsonResponse

from rest_framework.decorators import list_route


class CounterMixin(object):

    @list_route(methods=['get'])
    def counter(self, request, **kwargs):
        queryset = self.filter_queryset(self.get_queryset())
        return JsonResponse({"count": queryset.count()})
