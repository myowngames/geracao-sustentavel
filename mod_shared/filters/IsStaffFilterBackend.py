from rest_framework.filters import BaseFilterBackend;

class IsStaffFilterBackend(BaseFilterBackend):

	def filter_queryset(self, request, queryset, view):
		if request.user == None or str(request.user) == 'AnonymousUser':
			raise PermissionDenied();
			
		if request.user.is_staff == True:
			return queryset;
		else:
			return None;
