from rest_framework.filters import BaseFilterBackend


class EmptyQueryset(BaseFilterBackend):

    def filter_queryset(self, request, queryset, view):
        return None
