from rest_framework.filters import BaseFilterBackend
from rest_framework.exceptions import PermissionDenied


class BelongsToUserOnlyFilterBackend(BaseFilterBackend):

    def filter_queryset(self, request, queryset, view):
        if not request.user or str(request.user) == 'AnonymousUser':
            raise PermissionDenied()

        return queryset.filter(user=request.user)
