import json

from django.db import models

from json.decoder import JSONDecodeError

from lib.Tools.SecurityTools import SecurityTools

class JsonField(models.TextField):
	description = "An object capable of converting text back and forth to json to/from the database"

	def __init__(self, encryptData=False, *args, **kwargs):
		self.encryptData = encryptData;
		super(JsonField, self).__init__(*args, **kwargs);


	def deconstruct(self):
		name, path, args, kwargs = super(JsonField, self).deconstruct();
		kwargs['encryptData'] = self.encryptData;
		return name, path, args, kwargs;

	'''
	If present for the field subclass, from_db_value() will be called in all circumstances when the data is loaded from the database, including in aggregates and values() calls.
	'''
	def from_db_value(self, value, expression, connection, context):
		if value == None or value == "":
			value = {};
		else:
			if self.encryptData == True:
				try:
					value = SecurityTools.decrypt(value);
				except:
					value = '{}';

			try:
				value = json.loads(value);
			except:
				value = {};
		
		return value;

	#this value if the value that goes to the database
	def get_prep_value(self, value):
		stringToReturn = '';

		if 'dict' in str(type(value)):
			stringToReturn = json.dumps(value);
		elif value == None:
			stringToReturn = "{}";
		elif type(value).__name__ == 'str':
			#check if that is a valid json string
			try:
				json.loads(value);
				stringToReturn = value;
			except JSONDecodeError:
				stringToReturn = "{}";

		if self.encryptData == True:
			stringToReturn = SecurityTools.encrypt(stringToReturn);

		return stringToReturn;
		