VARS = {
    'DEBUG': True,
    'TEMPLATE_DEBUG': True,
    'BASE_URL': 'http://localhost:8000/',
    'ANGULAR_FRONTEND_BASE_URL': 'http://localhost:4200/',
    'DATABASES': {
        'default': {
                'ENGINE': 'django.db.backends.sqlite3',
                'NAME': 'mydatabase.sqlite3',
        }

        'mysql': {
                'ENGINE': 'django.db.backends.mysql',
                'NAME': 'database_name',
                'USER': 'database_user',
                'PASSWORD': 'database_pass',
                'HOST': 'database_host',
                'PORT': '3306',
        }
    },
}
