"""DjangoApp URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.conf.urls import include, url

from rest_framework import routers
from rest_framework_jwt.views import obtain_jwt_token, refresh_jwt_token, verify_jwt_token


from mod_application.views.CityViewSet import CityViewSet
from mod_application.views.DeliverableViewSet import DeliverableViewSet
from mod_application.views.DeliverableFileViewSet import DeliverableFileViewSet
from mod_application.views.FrontendFileViewSet import FrontendFileViewSet
from mod_application.views.GangPhaseDeliverableViewSet import GangPhaseDeliverableViewSet
from mod_application.views.GangPhaseDeliverableVoteViewSet import GangPhaseDeliverableVoteViewSet
from mod_application.views.GangPhaseViewSet import GangPhaseViewSet
from mod_application.views.GangViewSet import GangViewSet
from mod_application.views.NotificationViewSet import NotificationViewSet
from mod_application.views.NotificationContentViewSet import NotificationContentViewSet
from mod_application.views.PhaseViewSet import PhaseViewSet
from mod_application.views.ReportViewSet import ReportViewSet

from django.conf.urls.static import static

from mod_user.views.UserViewSet import UserViewSet
from mod_user.views.SignupViewSet import SignupViewSet
from DjangoApp import settings
from mod_user.views.GroupViewSet import GroupViewSet
from mod_user.views.PermissionViewSet import PermissionViewSet

router = routers.DefaultRouter()
router.register(r'user', UserViewSet)
router.register(r'group', GroupViewSet)
router.register(r'permission', PermissionViewSet)
router.register(r'city', CityViewSet)
router.register(r'deliverable', DeliverableViewSet)
router.register(r'deliverablefile',DeliverableFileViewSet)
router.register(r'frontendfile', FrontendFileViewSet)
router.register(r'gangphasedeliverable', GangPhaseDeliverableViewSet)
router.register(r'gangphasedeliverablevote', GangPhaseDeliverableVoteViewSet)
router.register(r'gangphase', GangPhaseViewSet)
router.register(r'gang', GangViewSet)
router.register(r'notification', NotificationViewSet)
router.register(r'notificationcontent', NotificationContentViewSet)
router.register(r'report', ReportViewSet)
router.register(r'phase', PhaseViewSet)
router.register(r'signup', SignupViewSet)


urlpatterns = [
    url(r'^api-token-auth/', obtain_jwt_token),
    url(r'^api-token-refresh/', refresh_jwt_token),
    url(r'^api-token-verify/', verify_jwt_token),
    url(r'^', include(router.urls)),
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
