from django.db import models
import sys

def sample_directory_path(instance, filename):
    # file will be uploaded to MEDIA_ROOT/user_<id>/<filename>
    return 'deliverable_samples/{0}_{1}'.format(instance.id, filename.encode(sys.getdefaultencoding()))

from mod_application.models.Deliverable import Deliverable


class DeliverableFile(models.Model):
	ALLOWED_CONTENT_TYPES_CHOICES = (
		('documents', 'Documentos'),
		('images', 'Imagens'),
		('videos', 'Videos'),
		('zip', 'Arquivos ZIP'),
	);


	id = models.AutoField(primary_key=True, max_length=10)
	deliverable = models.ForeignKey(Deliverable, on_delete=models.CASCADE, related_name='files')
	name = models.CharField(max_length=255, null=False)
	sample = models.FileField(upload_to=sample_directory_path, null=True, blank=True)
	allowed_content_types = models.TextField(null=True, choices=ALLOWED_CONTENT_TYPES_CHOICES, default='images')
	votable = models.BooleanField(default=False)
	created = models.DateTimeField(auto_now_add=True)
	updated = models.DateTimeField(auto_now=True)

	def sample_directory_path(self, filename):
		sample_directory_path(self, filename)