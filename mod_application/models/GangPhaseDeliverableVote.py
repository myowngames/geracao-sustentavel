from django.db import models

from mod_application.models.GangPhaseDeliverable import GangPhaseDeliverable
from mod_user.models.User import User

class GangPhaseDeliverableVote(models.Model):
    id = models.AutoField(primary_key=True, max_length=10)
    gang_phase_deliverable = models.ForeignKey(GangPhaseDeliverable, on_delete=models.CASCADE, related_name='gang_phase_deliverable_vote', null=True,default=None)
    user = models.ForeignKey(User, on_delete=models.CASCADE, related_name='gang_phase_deliverable_vote', null=True,default=None)
    source_ip_address = models.CharField(max_length=255, null=True, default=None)
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)