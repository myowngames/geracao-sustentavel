from django.db import models

from mod_application.models.Gang import Gang
from mod_application.models.Phase import Phase


class GangPhase(models.Model):
    id = models.AutoField(primary_key=True, max_length=10)
    gang = models.ForeignKey(Gang, on_delete=models.CASCADE, related_name='gang_phase')
    phase = models.ForeignKey(Phase, on_delete=models.CASCADE, related_name='gang_phase')
    completed = models.BooleanField(default=False)
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)
