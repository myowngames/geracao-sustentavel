from django.db import models
import datetime


class Phase(models.Model):
    id = models.AutoField(primary_key=True, max_length=10)
    name = models.CharField(max_length=255, null=False)
    start_at = models.DateField(null=False)
    end_at = models.DateField(null=False)
    instructions = models.TextField(null=True)
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)
    phase_depends = models.ForeignKey("self", on_delete=models.SET_NULL, related_name='phase_phase', null=True)

    @property
    def status(self):
        start = self.start_at
        end = self.end_at

        today = datetime.datetime.now().date()
        if today <= end and today >= start:
            return "Ativo"
        elif today > end:
            return "Expirado"

        return "Aguardando"
