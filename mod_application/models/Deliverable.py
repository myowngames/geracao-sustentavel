from django.db import models

from mod_application.models.Phase import Phase


class Deliverable(models.Model):
	id = models.AutoField(primary_key=True, max_length=10)
	phase = models.ForeignKey(Phase, on_delete=models.CASCADE, related_name='deliverable')
	name = models.CharField(max_length=255, null=False)
	description = models.TextField(null=True)
	created = models.DateTimeField(auto_now_add=True)
	updated = models.DateTimeField(auto_now=True)