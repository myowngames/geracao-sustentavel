from django.db import models

from mod_application.models.City import City

class Gang(models.Model):
    id = models.AutoField(primary_key=True, max_length=10)
    name = models.CharField(
        max_length=255,
        null=False,
        unique=True,
        error_messages={
            'invalid':"Nome do grupo é invalido",
            'unique':"Nome do grupo já existe ",
        }
    )
    school = models.CharField(max_length=255, null=False)
    city = models.ForeignKey(City, on_delete=models.CASCADE, related_name='city', null=True)
    is_focal_point = models.BooleanField(default=False)
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)

    @property
    def is_active(self):
        users = self.user.all()
        for user in users:
            if user.gang_leader and not user.is_active:
                return False
        return True
    