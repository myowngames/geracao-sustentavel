from django.db import models
import sys

def file_directory_path(instance, filename):
    # file will be uploaded to MEDIA_ROOT/user_<id>/<filename>
    return 'frontend_files/{0}_{1}'.format(instance.id, filename.encode(sys.getdefaultencoding()))

class FrontendFile(models.Model):
    id = models.AutoField(primary_key=True, max_length=10)
    name = models.CharField(max_length=255)
    file = models.FileField(upload_to=file_directory_path)
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)

    def file_directory_path(self, filename):
        file_directory_path(self, filename)
