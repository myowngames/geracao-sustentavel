from django.db import models

from mod_application.models.NotificationContent import NotificationContent
from mod_application.models.Gang import Gang

from django.core.mail import send_mail


class Notification(models.Model):
    NOTIFICATION_STATES = (
        ('read', 'Read'),
        ('unread', 'Unread'),
    )

    id = models.AutoField(primary_key=True, max_length=10)
    gang = models.ForeignKey(Gang, on_delete=models.CASCADE, related_name='gang', null=True, default=None)
    content = models.ForeignKey(NotificationContent, on_delete=models.SET_NULL, null=True, related_name='notification')
    email_notification = models.BooleanField(default=False)
    internal_notification = models.BooleanField(default=True)
    state = models.CharField(max_length=64, choices=NOTIFICATION_STATES, default='unread')
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)

    def send(self):
        if self.email_notification:
            gangUsers = self.gang.user.all()

            for user in gangUsers:
                if not user.email:
                    print("Usuario " + user.name + " Não tem email")
                else:
                    send_mail(
                        subject=self.content.subject,
                        message=self.content.content,
                        from_email='naoresponda@geracaosustentavel.org',
                        recipient_list=[user.email],
			html_message=self.content.content
                    )
