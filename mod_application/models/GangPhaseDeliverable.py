from django.db import models

from mod_application.models.GangPhase import GangPhase
from mod_application.models.DeliverableFile import DeliverableFile
import sys

def gang_phase_deliverables_directory_path(instance, filename):
    # file will be uploaded to MEDIA_ROOT/user_<id>/<filename>
    return 'gang_phase_deliverables/{0}_{1}'.format(instance.id, filename.encode(sys.getdefaultencoding()))

from mod_application.models.Deliverable import Deliverable

class GangPhaseDeliverable(models.Model):
    id = models.AutoField(primary_key=True, max_length=10)
    gang_phase = models.ForeignKey(GangPhase, on_delete=models.CASCADE, related_name='gang_phase_deliverable')
    name = models.CharField(max_length=255, null=True, default=None)
    file = models.FileField(upload_to=gang_phase_deliverables_directory_path, null=True)
    video_link = models.TextField(null=True, blank=True)
    deliverable_file = models.ForeignKey(DeliverableFile, on_delete=models.CASCADE, related_name='gang_phase_deliverable', null=True)
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)

    def gang_phase_deliverables_directory_path(self, filename):
        gang_phase_deliverables_directory_path(self, filename)