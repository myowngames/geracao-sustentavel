from django.db import models


class NotificationContent(models.Model):

    id = models.AutoField(primary_key=True, max_length=10)
    subject = models.CharField(max_length=255, null=True)
    content = models.TextField(null=True)
    owner_id = models.IntegerField(null=True, default=None)
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)
