from django.http import JsonResponse

from url_filter.integrations.drf import DjangoFilterBackend

from rest_framework.authentication import SessionAuthentication, BasicAuthentication
from rest_framework.permissions import IsAuthenticated, AllowAny
from rest_framework import viewsets
from rest_framework.decorators import detail_route, list_route
from rest_framework.filters import OrderingFilter

from rest_framework_jwt.authentication import JSONWebTokenAuthentication

from mod_application.models.DeliverableFile import DeliverableFile
from mod_application.serializers.DeliverableFileSerializer import DeliverableFileSerializer

from mod_shared.paginations.DefaultPagination import DefaultPagination
from mod_shared.mixins.CounterMixin import CounterMixin


class DeliverableFileViewSet(viewsets.ModelViewSet, CounterMixin):
    queryset = DeliverableFile.objects.all()
    serializer_class = DeliverableFileSerializer
    authentication_classes = (SessionAuthentication, BasicAuthentication, JSONWebTokenAuthentication)
    permission_classes = (IsAuthenticated,)
    filter_backends = [DjangoFilterBackend, OrderingFilter]
    ordering = ('-id')
    filter_fields = DeliverableFileSerializer().fields

    @list_route(methods=['get'])
    def get_allowed_content_types(self, request, *args, **kwargs):
        return JsonResponse(DeliverableFile.ALLOWED_CONTENT_TYPES_CHOICES, safe=False);