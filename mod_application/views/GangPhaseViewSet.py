from django.http import JsonResponse

from url_filter.integrations.drf import DjangoFilterBackend

from rest_framework.authentication import SessionAuthentication, BasicAuthentication
from rest_framework.permissions import IsAuthenticated
from rest_framework import viewsets
from rest_framework.filters import OrderingFilter
from rest_framework.decorators import detail_route, list_route

from rest_framework_jwt.authentication import JSONWebTokenAuthentication

from mod_application.models.GangPhase import GangPhase
from mod_application.serializers.GangPhaseSerializer import GangPhaseSerializer
from mod_application.serializers.GangPhaseDeliverableSerializer import GangPhaseDeliverableSerializer
from rest_framework.response import Response

from mod_shared.paginations.DefaultPagination import DefaultPagination
from mod_shared.mixins.CounterMixin import CounterMixin


class GangPhaseViewSet(viewsets.ModelViewSet, CounterMixin):
    queryset = GangPhase.objects.all()
    serializer_class = GangPhaseSerializer
    authentication_classes = (SessionAuthentication, BasicAuthentication, JSONWebTokenAuthentication)
    permission_classes = (IsAuthenticated, )
    pagination_class = DefaultPagination
    filter_backends = [DjangoFilterBackend, OrderingFilter]
    ordering = ('-created')
    filter_fields = ('id', 'completed', 'created', 'updated')

    def get_queryset(self):
        if self.request.user.is_staff or self.request.user.groups.filter(id__in=[1,3]):
            return GangPhase.objects.all()
        else:
            return GangPhase.objects.filter(gang=self.request.user.gang)

    @detail_route(methods=['get'])
    def get_complete_count(self, request, pk=None):
        count = GangPhase.objects.filter(phase=pk).filter(completed=True).count()
        return Response({'count':count})