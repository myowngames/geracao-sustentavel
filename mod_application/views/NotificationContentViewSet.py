from url_filter.integrations.drf import DjangoFilterBackend

from rest_framework.authentication import SessionAuthentication, BasicAuthentication
from rest_framework.permissions import IsAuthenticated
from rest_framework import viewsets
from rest_framework.filters import OrderingFilter

from rest_framework_jwt.authentication import JSONWebTokenAuthentication

from mod_application.models.Notification import NotificationContent
from mod_application.serializers.NotificationContentSerializer import NotificationContentSerializer
from mod_shared.paginations.DefaultPagination import DefaultPagination
from mod_shared.mixins.CounterMixin import CounterMixin


class NotificationContentViewSet(viewsets.ModelViewSet, CounterMixin):
    queryset = NotificationContent.objects.all()
    serializer_class = NotificationContentSerializer
    authentication_classes = (SessionAuthentication, BasicAuthentication, JSONWebTokenAuthentication)
    permission_classes = (IsAuthenticated, )
    pagination_class = DefaultPagination
    filter_backends = [DjangoFilterBackend, OrderingFilter]
    ordering = ('-id')
    filter_fields = ('id', 'subject', 'content', 'owner_id', 'created', 'updated')
