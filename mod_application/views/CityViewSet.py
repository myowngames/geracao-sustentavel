from url_filter.integrations.drf import DjangoFilterBackend

from rest_framework.authentication import SessionAuthentication, BasicAuthentication
from rest_framework.permissions import IsAuthenticated, AllowAny
from rest_framework import viewsets
from rest_framework.filters import OrderingFilter

from rest_framework_jwt.authentication import JSONWebTokenAuthentication

from mod_application.models.City import City
from mod_application.serializers.CitySerializer import CitySerializer

from mod_shared.paginations.DefaultPagination import DefaultPagination
from mod_shared.mixins.CounterMixin import CounterMixin


class CityViewSet(viewsets.ModelViewSet, CounterMixin):
	queryset = City.objects.all()
	serializer_class = CitySerializer
	authentication_classes = (SessionAuthentication, BasicAuthentication, JSONWebTokenAuthentication)
	permission_classes = (IsAuthenticated, )
	pagination_class = DefaultPagination
	filter_backends = [DjangoFilterBackend, OrderingFilter]
	ordering = ('name')
	filter_fields = CitySerializer().fields

	def get_permissions(self):
		if self.action == 'list':
			permission_classes = [AllowAny]
		else:
			permission_classes = [IsAuthenticated]
		return [permission() for permission in permission_classes]
   