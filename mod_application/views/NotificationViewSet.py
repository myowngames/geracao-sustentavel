from url_filter.integrations.drf import DjangoFilterBackend

from rest_framework.authentication import SessionAuthentication, BasicAuthentication
from rest_framework.permissions import IsAuthenticated
from rest_framework import viewsets
from rest_framework.filters import OrderingFilter
from rest_framework.serializers import CurrentUserDefault
from rest_framework_jwt.authentication import JSONWebTokenAuthentication

from mod_application.models.Notification import Notification
from mod_application.models.Gang import Gang
from mod_application.serializers.NotificationSerializer import NotificationSerializer

from mod_shared.paginations.DefaultPagination import DefaultPagination
from mod_shared.mixins.CounterMixin import CounterMixin
from rest_framework.decorators import list_route
from rest_framework.response import Response

from pprint import pprint


class NotificationViewSet(viewsets.ModelViewSet, CounterMixin):
    queryset = Notification.objects.all()
    serializer_class = NotificationSerializer
    authentication_classes = (SessionAuthentication, BasicAuthentication, JSONWebTokenAuthentication)
    permission_classes = (IsAuthenticated, )
    pagination_class = DefaultPagination
    filter_backends = [DjangoFilterBackend, OrderingFilter]
    ordering = ('-id')
    filter_fields = ('id', 'email_notification',
                     'internal_notification', 'state', 'created', 'updated')

    def get_queryset(self):
        gp = set([1, 3])
        gps = set(self.request.user.groups.all())
        if self.request.user.is_staff or gp.intersection(gps):
            return Notification.objects.all()
        else:
            return Notification.objects.filter(gang=self.request.user.gang)

    @list_route(methods=['get'])
    def get_user_new_notification_count(self,request):
        if request.user.gang:
            gang = Gang.objects.get(pk=request.user.gang_id)
            notification = Notification.objects.filter(gang=gang).filter(state="unread").order_by('-created').count()
            return Response({'count': notification})
        else:
            return Response({'count': 0})




    @list_route(methods=['get'])
    def get_user_notification(self, request, **kwargs):

        if request.user.gang:
            gang = Gang.objects.get(pk=request.user.gang_id)
            notification = Notification.objects.filter(gang=gang).order_by('-created')
            page = self.paginate_queryset(notification)

            if page is not None:
                serializer = self.get_serializer(page, many=True)
                return self.get_paginated_response(serializer.data)

            serializer = self.get_serializer(notification, many=True)
            return Response(serializer.data)
        else:
            return Response({'message': 'User does not belong to gang'})
