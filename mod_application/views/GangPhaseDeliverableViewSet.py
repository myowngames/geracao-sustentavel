import functools, operator

from django.contrib.auth.models import Group
from django.db.models import Q
from django.http import JsonResponse

from url_filter.integrations.drf import DjangoFilterBackend

from rest_framework.authentication import SessionAuthentication, BasicAuthentication
from rest_framework.permissions import IsAuthenticated
from rest_framework import viewsets
from rest_framework.filters import OrderingFilter
from rest_framework.decorators import detail_route, list_route

from rest_framework_jwt.authentication import JSONWebTokenAuthentication

from mod_application.models.GangPhaseDeliverable import GangPhaseDeliverable
from mod_application.serializers.GangPhaseDeliverableSerializer import GangPhaseDeliverableSerializer

from mod_application.models.GangPhase import GangPhase
from mod_application.serializers.GangPhaseSerializer import GangPhaseSerializer

from mod_application.models.Gang import Gang
from mod_application.serializers.GangSerializer import GangSerializer

from mod_application.models.GangPhaseDeliverableVote import GangPhaseDeliverableVote

from mod_user.serializers.UserSerializer import UserSerializer

from mod_shared.paginations.DefaultPagination import DefaultPagination
from mod_shared.mixins.CounterMixin import CounterMixin


class GangPhaseDeliverableViewSet(viewsets.ModelViewSet, CounterMixin):
    queryset = GangPhaseDeliverable.objects.all()
    serializer_class = GangPhaseDeliverableSerializer
    authentication_classes = (SessionAuthentication, BasicAuthentication, JSONWebTokenAuthentication)
    permission_classes = (IsAuthenticated, )
    pagination_class = DefaultPagination
    filter_backends = [DjangoFilterBackend, OrderingFilter]
    ordering = ('-id')


    @list_route(methods=['get'])
    def get_all_videos(self, request, *args, **kwargs):
        data = [];
        filters = [];
        filters.append(Q(deliverable_file__allowed_content_types="videos"));
        voterGroup = Group.objects.get(pk=4);

        if request.user.city != None and request.user.city != '':
            filters.append( Q(gang_phase__gang__city=request.user.city) );


        gangPhaseDeliverableSerializer = self.get_serializer(self.get_queryset().filter(functools.reduce(operator.and_, filters)), many=True);
        
        for item in gangPhaseDeliverableSerializer.data:
            gangPhaseSerializer = GangPhaseSerializer(GangPhase.objects.get(pk=item['gang_phase']));
            item['gang_phase'] = gangPhaseSerializer.data;

            gangSerializer = GangSerializer(Gang.objects.get(pk=item['gang_phase']['gang']));
            item['gang_phase']['gang'] = gangSerializer.data;
        
            gangPhaseDeliverableVotes = GangPhaseDeliverableVote.objects.filter(gang_phase_deliverable=GangPhaseDeliverable.objects.get(pk=item['id'])).filter(user__groups__in=[voterGroup]);
            item['voter_user_votes'] = [];

            for vote in gangPhaseDeliverableVotes:
                userSerializer = UserSerializer(vote.user);
                item['voter_user_votes'].append(userSerializer.data);

            data.append(item);

        return JsonResponse(data, safe=False);