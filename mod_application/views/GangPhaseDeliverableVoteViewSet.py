from django.http import JsonResponse
from django.contrib.auth.models import Group

from url_filter.integrations.drf import DjangoFilterBackend

from rest_framework.decorators import detail_route, list_route
from rest_framework.authentication import SessionAuthentication, BasicAuthentication
from rest_framework.permissions import IsAuthenticated
from rest_framework import viewsets
from rest_framework.filters import OrderingFilter
from rest_framework.exceptions import ValidationError

from rest_framework_jwt.authentication import JSONWebTokenAuthentication

from mod_application.models.GangPhaseDeliverableVote import GangPhaseDeliverableVote
from mod_application.serializers.GangPhaseDeliverableVoteSerializer import GangPhaseDeliverableVoteSerializer
from mod_application.models.GangPhaseDeliverable import GangPhaseDeliverable

from mod_shared.paginations.DefaultPagination import DefaultPagination
from mod_shared.mixins.CounterMixin import CounterMixin

from mod_user.models.User import User
from mod_user.serializers.UserSerializer import UserSerializer



class GangPhaseDeliverableVoteViewSet(viewsets.ModelViewSet, CounterMixin):
    queryset 				= GangPhaseDeliverableVote.objects.all();
    serializer_class 		= GangPhaseDeliverableVoteSerializer;
    authentication_classes 	= (SessionAuthentication, BasicAuthentication, JSONWebTokenAuthentication);
    permission_classes 		= (IsAuthenticated, );
    pagination_class 		= DefaultPagination;
    filter_backends 		= [DjangoFilterBackend, OrderingFilter];
    ordering 				= ('-id')
    filter_fields 			= GangPhaseDeliverableVoteSerializer().fields;


    def create(self, request, *args, **kwargs):
        query = self.get_queryset().filter(user=self.request.user);

        if query.count() > 0:
            raise ValidationError("Você já realizou o seu voto");
        else:
            return super(GangPhaseDeliverableVoteViewSet, self).create(request, *args, **kwargs);


    @list_route(methods=['get'])
    def get_voter_user_vote(self, request, *args, **kwargs):
        user = self.request.user;
        query = GangPhaseDeliverableVote.objects.filter(user=user);

        if query.count() == 0:
            return JsonResponse({})
        elif query.count() > 1:
            query.all().delete()
            raise ValidationError("Varios votos já realizados anterioremente! Possivelmente um Admin de Cidade que foi transformado em Votador. Todos os Votos Antigos foram removidos")
        else:
            vote = query.get();
            serializer = self.get_serializer(vote);
            return JsonResponse(serializer.data['gang_phase_deliverable'], safe=False);



    @list_route(methods=['post'])
    def save_city_admin_vote(self, request, pk=None, *args, **kwargs):
        if request.user.city != None:
            try:
                totalVotes = int(request.data['total_votes']);
                gangPhaseDeliverableId = request.data['gang_phase_deliverable'];
            except KeyError:
                totalVotes = 0;

            gangPhaseDeliverableInstance = GangPhaseDeliverable.objects.get(pk=gangPhaseDeliverableId);

            query = self.get_queryset().filter(gang_phase_deliverable=gangPhaseDeliverableInstance).filter(user=request.user);

            if query.count() > 0:
                query.delete();

            data = {
                'user': request.user.id,
                'gang_phase_deliverable': gangPhaseDeliverableId
            }

            for i in range(0, totalVotes):
                serializer = self.get_serializer(data=data);
                serializer.is_valid(raise_exception=True);
                serializer.save();

            return JsonResponse({'votes': totalVotes})

        else:
            raise ValidationError("Você não pode fazer isso");


    @list_route(methods=['post'])
    def get_phase_deliverable_votes_admin_city(self, request, *args, **kwargs):
        if request.user.city != None:
            try:
                gangPhaseDeliverableId = request.data['gang_phase_deliverable'];
            except KeyError:
                raise ValidationError("Você deve fornecer qual material voce deseja o total de votos");

            gangPhaseDeliverable = GangPhaseDeliverable.objects.get(pk=gangPhaseDeliverableId);
            query = self.get_queryset().filter(gang_phase_deliverable=gangPhaseDeliverable).filter(user=request.user);
            return JsonResponse({'votes': query.count()})
        else:
            raise ValidationError("Você não pode fazer isso");


    @list_route(methods=['post'])
    def get_video_total_votes(self, request, *args, **kwargs):
        try:
            gangPhaseDeliverable = GangPhaseDeliverable.objects.get(pk=request.data['gang_phase_deliverable'])
            query = GangPhaseDeliverableVote.objects.filter(gang_phase_deliverable=gangPhaseDeliverable);
            return JsonResponse({'votes': query.count()})
        except KeyError:
            raise ValidationError("Você não pode fazer isso");