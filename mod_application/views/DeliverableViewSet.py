from django.http import JsonResponse

from url_filter.integrations.drf import DjangoFilterBackend

from rest_framework.authentication import SessionAuthentication, BasicAuthentication
from rest_framework.permissions import IsAuthenticated, AllowAny
from rest_framework import viewsets
from rest_framework.decorators import detail_route, list_route
from rest_framework.filters import OrderingFilter

from rest_framework_jwt.authentication import JSONWebTokenAuthentication

from mod_application.models.Deliverable import Deliverable
from mod_application.models.DeliverableFile import DeliverableFile
from mod_application.serializers.DeliverableSerializer import DeliverableSerializer

from mod_shared.paginations.DefaultPagination import DefaultPagination
from mod_shared.mixins.CounterMixin import CounterMixin


class DeliverableViewSet(viewsets.ModelViewSet,CounterMixin):
	queryset 				= Deliverable.objects.all();
	serializer_class 		= DeliverableSerializer;
	authentication_classes 	= (SessionAuthentication, BasicAuthentication, JSONWebTokenAuthentication);
	permission_classes 		= (IsAuthenticated, );
	pagination_class 		= DefaultPagination;
	filter_backends 		= [DjangoFilterBackend, OrderingFilter];
	ordering 				= ('-id')
	filter_fields 			= DeliverableSerializer().fields;

	@list_route(methods=['get'])
	def get_allowed_content_types(self, request, *args, **kwargs):
		return JsonResponse(DeliverableFile.ALLOWED_CONTENT_TYPES_CHOICES, safe=False);