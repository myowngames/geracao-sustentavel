from django.http import JsonResponse

from url_filter.integrations.drf import DjangoFilterBackend

from rest_framework.authentication import SessionAuthentication, BasicAuthentication
from rest_framework.permissions import IsAuthenticated
from rest_framework import viewsets
from rest_framework.filters import OrderingFilter
from rest_framework.decorators import detail_route, list_route

from rest_framework_jwt.authentication import JSONWebTokenAuthentication

from mod_application.models.Phase import Phase
from mod_application.models.GangPhase import GangPhase
from mod_application.models.Gang import Gang
from mod_application.serializers.PhaseSerializer import PhaseSerializer
from mod_application.serializers.DeliverableSerializer import DeliverableSerializer
from mod_application.serializers.GangPhaseSerializer import GangPhaseSerializer

from mod_shared.paginations.DefaultPagination import DefaultPagination
from mod_shared.mixins.CounterMixin import CounterMixin
from rest_framework.pagination import LimitOffsetPagination


class PhaseViewSet(viewsets.ModelViewSet, CounterMixin):
    queryset = Phase.objects.all()
    serializer_class = PhaseSerializer
    authentication_classes = (SessionAuthentication, BasicAuthentication, JSONWebTokenAuthentication)
    permission_classes = (IsAuthenticated,)
    # pagination_class = DefaultPagination
    filter_backends = [DjangoFilterBackend, OrderingFilter]
    ordering = ('start_at')
    filter_fields = ('id', 'name', 'start_at', 'end_at', 'instructions', 'created', 'updated')
    
    @detail_route(methods=['get'])
    def get_deliverable(self, request, pk=None, *args, **kwargs):
        phase = self.get_object()
        serializer = DeliverableSerializer(phase.deliverable.all(), many=True)
        return JsonResponse(serializer.data, safe=False)
    
    @detail_route(methods=['get'], pagination_class=DefaultPagination)
    def get_gang_phase(self, request, pk=None, *args, **kwargs):
        paginator = self.pagination_class()
        phase = Phase.objects.get(pk=pk)
        
        if self.request.user.groups.filter(id=3):
            queryset = GangPhase.objects.filter(
                phase=phase,
                gang__in=Gang.objects.filter(city=self.request.user.city, is_focal_point=False).all()
            )
        else:
            queryset = GangPhase.objects.filter(
                phase=phase,
                gang__in=Gang.objects.filter(is_focal_point=False).all()
            )
        
        if request.GET and request.GET.get('ordering'):
            queryset.order_by(request.GET.get('ordering') )
        else:
            queryset.order_by('created')
        queryset = queryset.all()

        page = paginator.paginate_queryset(queryset=queryset, request=request)
        serializer = GangPhaseSerializer(page, many=True)
        
        return paginator.get_paginated_response(serializer.data)
