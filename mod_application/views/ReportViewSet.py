import csv, os, io

from xlsxwriter.workbook import Workbook


from django.http import JsonResponse, HttpResponse, FileResponse, StreamingHttpResponse, HttpRequest

from rest_framework import viewsets
from rest_framework.authentication import SessionAuthentication, BasicAuthentication
from rest_framework.permissions import AllowAny
from rest_framework.decorators import detail_route, list_route
from rest_framework.pagination import LimitOffsetPagination

from rest_framework_jwt.authentication import JSONWebTokenAuthentication

from mod_shared.paginations.DefaultPagination import DefaultPagination

from mod_application.models.Deliverable import Deliverable
from mod_application.models.GangPhaseDeliverable import GangPhaseDeliverable
from mod_application.models.GangPhase import GangPhase
from mod_application.models.Gang import Gang

from mod_application.serializers.DeliverableSerializer import DeliverableSerializer
from mod_user.models.User import User
from django.contrib.sites.shortcuts import get_current_site
from django.conf import settings

class ReportViewSet(viewsets.GenericViewSet):

    queryset = Deliverable.objects.all();
    serializer_class = DeliverableSerializer;
    authentication_classes = (SessionAuthentication, BasicAuthentication, JSONWebTokenAuthentication);
    permission_classes = (AllowAny,);
   
    @list_route(methods=['get'])
    def user_list(self, request, *args, **kwargs):
        fileLocation = "user_report.xlsx";

        output = io.BytesIO()
        workbook = Workbook(output, {'in_memory': True})
        worksheet = workbook.add_worksheet();

        rows = [];
        rows.append({
            'user_name': 'NOME',
            'user_email': 'EMAIL',
            'group_name': 'GRUPO',
            'school': 'ESCOLA',
            'city': 'CIDADE',
            'state': 'ESTADO',
        });
        gangs = Gang.objects.filter(is_focal_point= False).all();

        for gang in gangs:
            users = gang.user.all()
            for user in users:
                currentRow = {
                    'user_name': user.name,
                    'user_email': user.email,
                    'group_name': user.gang.name,
                    'school': user.gang.school,
                    'city': user.gang.city.name,
                    'state': user.gang.city.state,
                };
                rows.append(currentRow)
        
        rowNumber = 0;
        for row in rows:
            # Adicionando na mão para ficar na ordem correta
            worksheet.write(rowNumber, 0, row['user_name'])
            worksheet.write(rowNumber, 1, row['user_email'])
            worksheet.write(rowNumber, 2, row['group_name'])
            worksheet.write(rowNumber, 3, row['school'])
            worksheet.write(rowNumber, 4, row['city'])
            worksheet.write(rowNumber, 5, row['state'])
            rowNumber += 1

        workbook.close();

        output.seek(0);

        response = HttpResponse(output.read(), content_type="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet")
        response['Content-Disposition'] = "attachment; filename="+fileLocation
        return response;

    @list_route(methods=['get'])
    def generate(self, request, *args, **kwargs):
       
        fileLocation = "report.xlsx";

        output = io.BytesIO()

        workbook = Workbook(output, {'in_memory': True})
        worksheet = workbook.add_worksheet();

        rows = [];
        rows.append({
            'group_name': 'NOME DO GRUPO',
            'city': 'CIDADE',
            'state': 'ESTADO',
            'school': 'ESCOLA',
            'is_active': 'STATUS DO GRUPO',
            'group_leader_name': 'LIDER DO GRUPO',
            'group_leader_email': 'EMAIL DO LIDER',
            'group_leader_phone': 'TELEFONE DO LIDER',
            'phase': 'FASE',
            'status': 'STATUS',
            'deliverable_name': 'NOME DO ESTREGAVEL',
            'deliverable_link': 'LINK DO ENTREGAVEL',
        });

        gangPhases = GangPhase.objects.filter(
                        gang__in=Gang.objects.filter(is_focal_point = False).all()
                    ).all();

        for gangPhase in gangPhases:
            for phaseDeliverable in gangPhase.phase.deliverable.all():
                for phaseDeliverableFile in phaseDeliverable.files.all():
                    currentRow = {
                        'group_name': gangPhase.gang.name,
                        'city': gangPhase.gang.city.name,
                        'state': gangPhase.gang.city.state,
                        'school': gangPhase.gang.school,
                        'is_active':  "ATIVO" if gangPhase.gang.is_active else "BLOQUEADO",
                        'group_leader_name': '',
                        'group_leader_email': '',
                        'group_leader_phone': '',
                        'phase': gangPhase.phase.name,
                        'status': 'Incompleto',
                        'deliverable_name': phaseDeliverable.name + " - " + phaseDeliverableFile.name,
                        'deliverable_link': '',
                    };

                    #get user leader of this group
                    query = User.objects.filter(gang_leader=True).filter(gang=gangPhase.gang);
                    if query.count() > 0:
                        user = query[:1].get();
                        currentRow['group_leader_name'] = user.name;
                        currentRow['group_leader_email'] = user.email;
                        currentRow['group_leader_phone'] = user.phone;

                    for gangPhaseDeliverable in gangPhase.gang_phase_deliverable.all():
                        if phaseDeliverableFile.allowed_content_types == 'videos' and gangPhaseDeliverable.video_link != None and gangPhaseDeliverable.name == phaseDeliverableFile.name:
                            currentRow['status'] = "COMPLETO";
                            currentRow['deliverable_link'] = gangPhaseDeliverable.video_link;
                        else:
                            if phaseDeliverableFile.pk == gangPhaseDeliverable.deliverable_file.pk:
                                currentRow['status'] = "COMPLETO";
                                currentRow['deliverable_link'] = request.build_absolute_uri(gangPhaseDeliverable.file.url);

                    for index in currentRow:
                        currentRow[index] = currentRow[index];
                    rows.append(currentRow);

        rowNumber = 0;
        for row in rows:

            # Adicionando na mão para ficar na ordem correta
            worksheet.write(rowNumber, 0, row['group_name'])
            worksheet.write(rowNumber, 1, row['city'])
            worksheet.write(rowNumber, 2, row['state'])
            worksheet.write(rowNumber, 3, row['school'])
            worksheet.write(rowNumber, 3, row['is_active'])
            worksheet.write(rowNumber, 4, row['group_leader_name'])
            worksheet.write(rowNumber, 5, row['group_leader_email'])
            worksheet.write(rowNumber, 6, row['group_leader_phone'])
            worksheet.write(rowNumber, 7, row['phase'])
            worksheet.write(rowNumber, 8, row['status'])
            worksheet.write(rowNumber, 9, row['deliverable_name'])
            worksheet.write(rowNumber, 10, row['deliverable_link'])
            rowNumber += 1

        workbook.close();

        output.seek(0);

        response = HttpResponse(output.read(), content_type="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet")
        response['Content-Disposition'] = "attachment; filename=report.xlsx"
        return response;