from rest_framework.decorators import detail_route, list_route
from url_filter.integrations.drf import DjangoFilterBackend
from url_filter.filtersets import ModelFilterSet
from url_filter.filters import Filter

from django import forms

from rest_framework.authentication import SessionAuthentication, BasicAuthentication
from rest_framework.permissions import IsAuthenticated, AllowAny
from rest_framework import viewsets

from rest_framework.filters import OrderingFilter
# from django_filters import rest_framework as filters

from rest_framework.exceptions import ValidationError
from rest_framework.response import Response
from rest_framework_jwt.authentication import JSONWebTokenAuthentication

from mod_application.models.Gang import Gang
from mod_application.models.GangPhase import GangPhase
from mod_application.serializers.GangSerializer import GangSerializer
from mod_application.serializers.GangPhaseSerializer import GangPhaseSerializer
from mod_application.models.City import City
from mod_user.serializers.UserSerializer import UserSerializer
from mod_user.models.User import User

from mod_shared.paginations.DefaultPagination import DefaultPagination
from mod_shared.mixins.CounterMixin import CounterMixin


class GangFilter(ModelFilterSet):
    # city__name = filters.CharFilter(lookup_expr='icontains')
    # city = Filter(form_field=forms.CharField(max_length=15))
    # is_active = Filter(form_field=forms.BooleanField());
    class Meta:
        model = Gang
        #allow_related = True
        #fields = ['id', 'name', 'school', 'created','city__name', 'updated', 'is_focal_point', ]
      


class GangViewSet(viewsets.ModelViewSet, CounterMixin):
    queryset = Gang.objects.all().select_related('city')
    serializer_class = GangSerializer
    authentication_classes = (SessionAuthentication, BasicAuthentication, JSONWebTokenAuthentication)
    permission_classes = (IsAuthenticated, AllowAny)
    pagination_class = DefaultPagination
    filter_backends = (DjangoFilterBackend, OrderingFilter)
    ordering = ('-id')
    filter_class = GangFilter
    # filter_fields = ('id', 'name', 'school', 'city', 'created', 'updated')
    # Removing City Filter For know is giving me some kin of error
    # filter_fields = ('id', 'name', 'school', 'created', 'updated', 'is_focal_point')
    
    
    def get_queryset(self):
        if self.request.user.is_staff or self.request.user.groups.filter(id__in=[1, 4]):
            return Gang.objects.all()
        elif self.request.user.groups.filter(id=3):
            return Gang.objects.filter(city=self.request.user.city).all()
        else:
            return Gang.objects.filter(pk=self.request.user.gang.id)
    
    @detail_route(methods=['get'])
    def get_gang_phases(self, request, pk=None):
        if not pk:
            pk = request.user.gang_id

        if pk:
            gangs = GangPhase.objects.all().filter(gang_id=pk).order_by('phase__start_at')
            serializer = GangPhaseSerializer(gangs, many=True)

            return Response(serializer.data)

        else:
            raise ValidationError("Gang esperada")

    @detail_route(methods=['get'])
    def get_gang_users(self, request, pk=None):
        if not pk:
            pk = request.user.gang_id

        if pk:
            gang = Gang.objects.get(pk=pk)
            users = gang.user.all();
            serializer = UserSerializer(users, many=True)

            return Response(serializer.data)
        else:
            raise ValidationError("Gang esperada");

    def get_permissions(self):
        if self.action == 'create':
            permission_classes = [AllowAny]
        else:
            permission_classes = [IsAuthenticated]
        return [permission() for permission in permission_classes]