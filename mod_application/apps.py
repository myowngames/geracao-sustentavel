from django.apps import AppConfig


class ModApplicationConfig(AppConfig):
    name = 'mod_application'

    def ready(self):
        import mod_application.signals.GangSignals
        import mod_application.signals.PhaseSignals
        import mod_application.signals.GangPhaseDeliverableSignals
