# Generated by Django 2.0.1 on 2018-02-14 18:42

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('mod_application', '0006_auto_20180214_1624'),
    ]

    operations = [
        migrations.AlterField(
            model_name='gang',
            name='city',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, related_name='city', to='mod_application.City'),
        ),
    ]
