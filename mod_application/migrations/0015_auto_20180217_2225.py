# Generated by Django 2.0.1 on 2018-02-18 00:25

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('mod_application', '0014_merge_20180217_1149'),
    ]

    operations = [
        migrations.CreateModel(
            name='DeliverableFile',
            fields=[
                ('id', models.AutoField(max_length=10, primary_key=True, serialize=False)),
                ('name', models.CharField(max_length=255)),
                ('sample', models.FileField(blank=True, null=True, upload_to='deliverable_samples/')),
                ('allowed_content_types', models.TextField(choices=[('documents', 'Documentos'), ('images', 'Imagens'), ('videos', 'Videos'), ('zip', 'Arquivos ZIP')], default='images', null=True)),
                ('votable', models.BooleanField(default=False)),
                ('created', models.DateTimeField(auto_now_add=True)),
                ('updated', models.DateTimeField(auto_now=True)),
            ],
        ),
        migrations.CreateModel(
            name='GangPhaseDeliverableFile',
            fields=[
                ('id', models.AutoField(max_length=10, primary_key=True, serialize=False)),
                ('name', models.CharField(default=None, max_length=255, null=True)),
                ('file', models.FileField(upload_to='gang_phase_deliverables/')),
                ('created', models.DateTimeField(auto_now_add=True)),
                ('updated', models.DateTimeField(auto_now=True)),
            ],
        ),
        migrations.RemoveField(
            model_name='deliverable',
            name='allowed_content_types',
        ),
        migrations.RemoveField(
            model_name='deliverable',
            name='sample',
        ),
        migrations.RemoveField(
            model_name='deliverable',
            name='votable',
        ),
        migrations.RemoveField(
            model_name='gangphasedeliverable',
            name='file',
        ),
        migrations.RemoveField(
            model_name='gangphasedeliverablevote',
            name='deliverable',
        ),
        migrations.AlterField(
            model_name='gangphasedeliverable',
            name='gang_phase',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='GangPhaseDeliverable', to='mod_application.GangPhase'),
        ),
        migrations.AlterField(
            model_name='gangphasedeliverable',
            name='name',
            field=models.CharField(default=None, max_length=255, null=True),
        ),
        migrations.AlterField(
            model_name='gangphasedeliverablevote',
            name='source_ip_address',
            field=models.CharField(default=None, max_length=255, null=True),
        ),
        migrations.AddField(
            model_name='gangphasedeliverablefile',
            name='gang_phase_deliverable',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='files', to='mod_application.GangPhaseDeliverable'),
        ),
        migrations.AddField(
            model_name='deliverablefile',
            name='deliverable',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='files', to='mod_application.Deliverable'),
        ),
        migrations.AddField(
            model_name='gangphasedeliverablevote',
            name='deliverable_file',
            field=models.ForeignKey(default=None, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='deliverable', to='mod_application.GangPhaseDeliverableFile'),
        ),
    ]
