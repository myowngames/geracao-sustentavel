# Generated by Django 2.0.1 on 2018-04-27 14:43

from django.db import migrations, models
import mod_application.models.FrontendFile
import mod_application.models.GangPhaseDeliverable


class Migration(migrations.Migration):

    dependencies = [
        ('mod_application', '0025_auto_20180427_1110'),
    ]

    operations = [
        migrations.AlterField(
            model_name='frontendfile',
            name='file',
            field=models.FileField(upload_to=mod_application.models.FrontendFile.file_directory_path),
        ),
        migrations.AlterField(
            model_name='gang',
            name='name',
            field=models.CharField(error_messages={'invalid': 'Nome do grupo é invalido', 'unique': 'Nome do grupo já existe '}, max_length=255, unique=True),
        ),
        migrations.AlterField(
            model_name='gangphasedeliverable',
            name='file',
            field=models.FileField(null=True, upload_to=mod_application.models.GangPhaseDeliverable.gang_phase_deliverables_directory_path),
        ),
    ]
