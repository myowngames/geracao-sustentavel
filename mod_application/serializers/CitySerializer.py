from rest_framework import serializers

from mod_application.models.City import City


class CitySerializer(serializers.ModelSerializer):
    class Meta:
        fields = ('id', 'name', 'state', 'created', 'updated')
        read_only_fields = ('created', 'updated')
        model = City
