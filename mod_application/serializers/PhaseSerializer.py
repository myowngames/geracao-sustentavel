from rest_framework import serializers

from mod_application.models.Phase import Phase
from mod_application.serializers.DeliverableSerializer import DeliverableSerializer


class PhaseSerializer(serializers.ModelSerializer):
    deliverable = DeliverableSerializer(many=True, read_only=True)

    class Meta:
        fields = (
        'id', 'name', 'start_at', 'end_at', 'status', 'deliverable', 'instructions', 'phase_depends', 'created', 'updated')
        read_only_fields = ('created', 'updated','deliverable')
        model = Phase
