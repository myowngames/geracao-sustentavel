from rest_framework import serializers

from mod_application.models.GangPhase import GangPhase
from mod_application.serializers.PhaseSerializer import PhaseSerializer
from mod_application.serializers.GangPhaseDeliverableSerializer import GangPhaseDeliverableSerializer;


class GangPhaseSerializer(serializers.ModelSerializer):
	phase = PhaseSerializer();
	gang_phase_deliverable = GangPhaseDeliverableSerializer(many=True, read_only=True);
	
	class Meta:
		fields = ('id', 'gang', 'phase', 'completed', 'gang_phase_deliverable', 'created', 'updated')
		read_only_fields = ('created', 'updated')
		model = GangPhase