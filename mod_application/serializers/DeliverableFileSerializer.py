from rest_framework import serializers

from mod_application.models.DeliverableFile import DeliverableFile

class DeliverableFileSerializer(serializers.ModelSerializer):

	class Meta:
		fields = ('id','deliverable','name','sample','allowed_content_types','votable','created','updated',)
		read_only_fields = ('created', 'updated')
		model = DeliverableFile