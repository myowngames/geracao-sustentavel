from rest_framework import serializers

from mod_application.models.GangPhaseDeliverableVote import GangPhaseDeliverableVote


class GangPhaseDeliverableVoteSerializer(serializers.ModelSerializer):
	user = serializers.HiddenField(
		default = serializers.CurrentUserDefault()
	);

	class Meta:
		fields = ('id', 'gang_phase_deliverable', 'user','source_ip_address', 'created', 'updated')
		read_only_fields = ('created', 'updated')
		model = GangPhaseDeliverableVote
