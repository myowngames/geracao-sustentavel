from rest_framework import serializers

from mod_application.models.FrontendFile import FrontendFile


class FrontendFileSerializer(serializers.ModelSerializer):
    class Meta:
        fields = ('id', 'name', 'file', 'created', 'updated')
        read_only_fields = ('created', 'updated')
        model = FrontendFile
