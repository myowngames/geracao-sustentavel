from rest_framework import serializers

from mod_application.models.GangPhaseDeliverable import GangPhaseDeliverable
from mod_application.serializers.DeliverableFileSerializer import DeliverableFileSerializer

class GangPhaseDeliverableSerializer(serializers.ModelSerializer):
	class Meta:
		fields = ('id', 'gang_phase', 'name','file', 'deliverable_file', 'video_link' ,'created', 'updated')
		read_only_fields = ('created', 'updated')
		model = GangPhaseDeliverable
