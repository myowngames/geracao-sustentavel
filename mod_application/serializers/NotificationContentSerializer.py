from rest_framework import serializers

from mod_application.models.NotificationContent import NotificationContent


class NotificationContentSerializer(serializers.ModelSerializer):
    owner_id = serializers.ReadOnlyField(
        default=serializers.CurrentUserDefault()
    )

    class Meta:
        fields = ('id', 'subject', 'content', 'owner_id', 'created', 'updated')
        read_only_fields = ('created', 'updated', )
        model = NotificationContent
