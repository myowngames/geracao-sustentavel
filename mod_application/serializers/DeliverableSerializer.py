from rest_framework import serializers

from mod_application.models.Deliverable import Deliverable
from mod_application.serializers.DeliverableFileSerializer import DeliverableFileSerializer


class DeliverableSerializer(serializers.ModelSerializer):
    files = DeliverableFileSerializer(many=True, read_only=True)

    class Meta:
        fields = ('id', 'phase', 'name', 'files', 'description', 'created', 'updated')
        read_only_fields = ('created', 'updated')
        model = Deliverable
