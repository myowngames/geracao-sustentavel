from rest_framework import serializers

from django.contrib.contenttypes.models import ContentType


class ContentTypeSerializer(serializers.ModelSerializer):

    class Meta:
        fields = ('id', 'app_label', 'model')
        model = ContentType
