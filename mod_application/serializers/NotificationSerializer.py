from rest_framework import serializers

from mod_application.models.Notification import Notification
from mod_application.models.Gang import Gang
from mod_application.serializers.NotificationContentSerializer import NotificationContentSerializer
from mod_application.models.NotificationContent import NotificationContent
from rest_framework.exceptions import ValidationError


class NotificationSerializer(serializers.ModelSerializer):
    content = NotificationContentSerializer(required=False)
    gang_list = serializers.ListField(required=False)
    state = serializers.CharField(required=False)

    class Meta:
        fields = ('id', 'gang', 'gang_list', 'content', 'email_notification',
                  'internal_notification', 'state', 'created', 'updated')
        read_only_fields = ('created', 'updated')
        model = Notification

    def create(self, validated_data):
        try:
            content_data = validated_data.pop('content')
        except KeyError:
            raise ValidationError("Faltando Dados do conteudo da notificação")
        try:
            gangs = validated_data.pop('gang_list')
        except KeyError:
            raise ValidationError("Faltando Grupos para enviar a notificação")

        content = {
            'content': None,
            'subject': None,
            'user': None,
            'owner_id': None,
        }

        for k, v in content_data.items():
            content[k] = v

        user = content.pop('user')
        content['owner_id'] = user
        content = NotificationContentSerializer().validate(content)

        content = NotificationContent.objects.create(**content)
        gang_sent = [0]
        validated_data['content'] = content
        notification = None
        for x in gangs:
            if 'id' in x:
                if x['id'] not in gang_sent:
                    validated_data['gang'] = Gang.objects.get(pk=x['id'])
                    notification = Notification.objects.create(**validated_data)
                    notification.send()
                    gang_sent.append(x['id'])

        if notification:
            return notification
        else:
            raise ValidationError("Erro criando notificacoes")
