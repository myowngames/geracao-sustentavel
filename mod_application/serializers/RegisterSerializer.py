from rest_framework import serializers

from mod_user.serializers.UserSerializer import UserSerializer
from mod_application.serializers.GangSerializer import GangSerializer


class RegisterSerializer(serializers.ModelSerializer):
    gang = GangSerializer(many=False,required=True)
    users = UserSerializer(many=True,required=True)

    class Meta:
        fields = ('gang', 'users')
