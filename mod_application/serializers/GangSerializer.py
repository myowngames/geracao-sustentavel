from rest_framework import serializers
from rest_framework.exceptions import ValidationError

from mod_application.models.Gang import Gang
from mod_application.serializers.CitySerializer import CitySerializer
from mod_application.models.City import City

class GangSerializer(serializers.ModelSerializer):
    city = CitySerializer(read_only=True )

    class Meta:
        fields = ('id', 'name', 'school', 'city', 'is_focal_point', 'created', 'updated','is_active')
        read_only_fields = ('created', 'updated')
        model = Gang

    def create(self, validated_data):
        try:
            city = self.initial_data.pop('city')
            city = CitySerializer(many=False).validate(city)
        except KeyError:
            raise ValidationError('Cidade é obrigatorio')

        city = City.objects.get(pk=city)

        validated_data['city'] = city

        gang = Gang.objects.create(**validated_data)
        return gang

    def update(self, instance, validated_data):
        try:
            city = self.initial_data.pop('city')
            city = CitySerializer(many=False).validate(city)

            city = City.objects.get(pk=city)
            validated_data['city'] = city
        except KeyError:
            print("Faltando cidade")

        for attr in validated_data:
            setattr(instance, attr, validated_data[attr])

        instance.save()
        return instance
