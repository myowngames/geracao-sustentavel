from django.db.models.signals import pre_delete, post_save, pre_save
from django.dispatch import receiver

from mod_application.models.GangPhaseDeliverable import GangPhaseDeliverable
from mod_application.models.Deliverable import Deliverable
from mod_application.models.Phase import Phase
from mod_application.models.GangPhase import GangPhase


@receiver(post_save, sender=GangPhaseDeliverable)
def markGangPhaseAsComplete(sender, instance, *args, **kwargs):
    if instance.file or instance.video_link:
        gang_phase = instance.gang_phase
        phase = gang_phase.phase
        if not gang_phase.completed:
            deliverables = phase.deliverable.all()
            for deliverable in deliverables:
                files = deliverable.files.all()
                for file in files:
                    final_deliverables = GangPhaseDeliverable.objects.filter(gang_phase=gang_phase, deliverable_file=file).count()
                    if final_deliverables <= 0:
                        return
            gang_phase.completed = True
            gang_phase.save()
