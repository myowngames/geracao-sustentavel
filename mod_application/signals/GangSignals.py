from django.db.models.signals import pre_delete, post_save, pre_save
from django.dispatch import receiver

from mod_application.models.Gang import Gang
from mod_application.models.Phase import Phase
from mod_application.models.GangPhase import GangPhase

@receiver(post_save, sender=Gang)
def addPhasesToThisGang(sender, instance, *args, **kwargs):
	if kwargs['created'] == True:
		phases = Phase.objects.all();
		for phase in phases:
			GangPhase.objects.create(phase=phase, gang=instance);

