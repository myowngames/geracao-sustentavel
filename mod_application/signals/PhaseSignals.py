from django.db.models.signals import pre_delete, post_save, pre_save
from django.dispatch import receiver

from rest_framework.exceptions import ValidationError

from mod_application.models.Phase import Phase
from mod_application.models.Gang import Gang
from mod_application.models.GangPhase import GangPhase

@receiver(post_save, sender=Phase)
def addNewPhaseToGangs(sender, instance, *args, **kwargs):
	if kwargs['created'] == True:
		gangs = Gang.objects.all();
		for gang in gangs:
			GangPhase.objects.create(phase=instance, gang=gang);


@receiver(pre_delete, sender=Phase)
def avoidDeleteIfGangIsAssociated(sender, instance, *args, **kwargs):
	check = GangPhase.objects.filter(phase=instance).count();

	if check > 0:
		raise ValidationError('Você não pode remover está fase pois existem grupos associadas a ela.');